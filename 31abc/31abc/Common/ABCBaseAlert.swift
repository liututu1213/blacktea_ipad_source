//
//  ABCBaseAlert.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/28.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

class BaseAlert: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var contentSize: CGSize = CGSize(width: 300, height: 300) {
        didSet {
            contentView.snp.updateConstraints { (make) in
                make.width.equalTo(contentSize.width)
                make.height.equalTo(contentSize.height)
            }
        }
    }
    
    func show() {
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.endEditing(true)
            self.rooterView.layoutIfNeeded()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            ABCBaseAlertManager.shared.show(self)
            self.contentView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            self.contentView.alpha = 1
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
                self.backgroundView.alpha = 1.0
                self.contentView.transform = .identity
            }, completion: nil)
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseOut, animations: {
            self.backgroundView.alpha = 0.0
            self.contentView.alpha = 0.0
        }, completion: {(finished) in
            self.rooterView.removeFromSuperview()
            ABCBaseAlertManager.shared.hide(self)
        } )
    }
    
    fileprivate lazy var backgroundView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        return v
    }()
    
    lazy var contentView: UIView = {
        let v = UIView()
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = true
        let effect = UIBlurEffect(style: .light)
        let efftectView = UIVisualEffectView(effect: effect)
        v.addSubview(efftectView)
        efftectView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }
        v.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
        return v
    }()
    
    fileprivate lazy var rooterView: UIView = {
        let v = UIView()
        v.addSubview(backgroundView)
        v.addSubview(contentView)
        
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }
        
        contentView.snp.makeConstraints { (make) in
            make.center.equalTo(v)
        }
        return v
    }()
}

class ABCBaseAlertManager: NSObject {
    var contextWindow: UIWindow?
    var alerts:[BaseAlert] = []
    static let shared = ABCBaseAlertManager()
    
    override init() {
        super.init()
    }
    
    func show(_ alert: BaseAlert?) {
        guard let _alert = alert else { return }
        showContextWindow()
        if !alerts.contains(_alert) {
            alerts.append(_alert)
            if let w = contextWindow {
                w.addSubview(_alert.rooterView)
                _alert.rooterView.snp.makeConstraints { (make) in
                    make.edges.equalTo(w)
                }
            }
        }
        contextWindow?.bringSubviewToFront(_alert.rooterView)
    }
    
    func hide(_ alert: BaseAlert?) {
        if let _alert = alert, alerts.contains(_alert) {
            if let index = alerts.firstIndex(of: _alert) {
                alerts.remove(at: index)
            }
        }
        if alerts.count == 0 {
            hideContextWindow()
        }
    }
    
    func showContextWindow() {
        if  contextWindow == nil {
            let keyWindow = UIApplication.shared.keyWindow
            contextWindow?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            contextWindow = UIWindow(frame: UIScreen.main.bounds)
            contextWindow?.windowLevel = .alert
            
            
            let orientation = UIApplication.shared.statusBarOrientation
            if (orientation == .landscapeLeft) {
                let rotation = CGAffineTransform(rotationAngle: CGFloat.pi*3/2)
                contextWindow?.transform = rotation
            }
            if (orientation == .landscapeRight) {
                let rotation = CGAffineTransform(rotationAngle: CGFloat.pi/2);
                contextWindow?.transform = rotation
            }
            contextWindow?.frame.origin = CGPoint.zero
            
            contextWindow?.makeKeyAndVisible()
            keyWindow?.makeKey()
        }
    }
    
    func hideContextWindow() {
        if contextWindow != nil {
            contextWindow?.removeFromSuperview()
            contextWindow?.resignKey()
            contextWindow = nil
        }
    }
}
