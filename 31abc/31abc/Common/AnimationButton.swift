//
//  ABCAnimationButton.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/27.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

class AnimationButton: UIButton {
    var clickClosure: ((UIButton) -> ())?
    var image: UIImage? {
        didSet {
            setImage(image, for: .normal)
        }
    }

    var title: String? {
        didSet {
            setTitle(title, for: .normal)
        }
    }

    var cornerRadius: CGFloat? {
        didSet {
            layoutIfNeeded()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor("#FCDE93")
        layer.shadowColor = UIColor("#D2BA7A").cgColor
        layer.shadowOpacity = 1
        layer.shadowRadius = 0
        layer.shadowOffset = CGSize(width: 0, height: 4)


        self.imageEdgeInsets = UIEdgeInsets(top: 6, left: 0, bottom: 6, right: 0)
        self.imageView?.contentMode = .scaleAspectFit
        self.isHighlighted = false
        self.adjustsImageWhenHighlighted = false
        
        addTarget(self, action: #selector(touchDown), for: .touchDown)
        addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func touchDown() {
        self.transform = CGAffineTransform(translationX: 0, y: 4)
        UIView.animate(withDuration: 0.5) {
            self.layer.shadowColor = UIColor.clear.cgColor
        }
    }
    
    @objc func touchUpInside(_ button: UIButton) {
        self.transform = self.transform.translatedBy(x: 0, y: -4)
        UIView.animate(withDuration: 0.5) {
            self.layer.shadowColor = UIColor("#D2BA7A").cgColor
        }
        clickClosure?(button)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        if let radius = cornerRadius {
            layer.cornerRadius = radius
        } else {
            layer.cornerRadius = frame.size.height / 2
        }
    }
}

