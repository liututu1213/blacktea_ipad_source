//
//  NavigationTool.swift
//  4iPad
//
//  Created by Cathy on 2019/9/28.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

typealias CompletionClosure = () -> Void

class NavigationTool: NSObject {
    static let shared = NavigationTool()
    
    var presentQueue: [UIViewController] = []
    var animatedDict: [String: Bool] = [:]
    var completionDict: [String: CompletionClosure] = [:]
    var isPresenting: Bool = false
    
    public var visibleNavigationController: UINavigationController? {
        return visibleViewController()?.navigationController
    }
    
    public func pushViewController(_ vc: UIViewController, animated: Bool = false) {
        visibleViewController()?.navigationController?.pushViewController(vc, animated: animated)
    }
    
    public func presentNavigationController(
        _ nav: UINavigationController,
        animated: Bool = false,
        completion: CompletionClosure? = nil) {
        
        var validNav = nav
        if !nav.isKind(of: UINavigationController.self) {
            validNav = UINavigationController(rootViewController: nav)
            validNav.isNavigationBarHidden = true
        }
        presentQueue.append(validNav)
        animatedDict.updateValue(animated, forKey: validNav.description)
        
        if let _completion =  completion {
            completionDict.updateValue(_completion, forKey: validNav.description)
        }
        
        if isPresenting { return }
        
        present()
    }
    
    private func present() {
        if presentQueue.count > 0, let navi = presentQueue.first {
            isPresenting = true
            DispatchQueue.main.async {
                let animated = self.animatedDict[navi.description] ?? false
                
                self.visibleViewController()?.present(navi, animated: animated, completion: {[weak self] in
                    let completion = self?.completionDict[navi.description]
                    completion?()
                    self?.presentQueue.remove(at: 0)
                    self?.animatedDict.removeValue(forKey: navi.description)
                    self?.completionDict.removeValue(forKey: navi.description)
                    
                    if let queue = self?.presentQueue, queue.count > 0 {
                        self?.present()
                    } else {
                        self?.isPresenting = false
                    }
                })
            }
            
        }
    }
    
     func visibleViewController() -> UIViewController? {
        guard let window = UIApplication.shared.delegate?.window else { return nil }
        var rooterVC = window?.rootViewController
        if let _ = rooterVC?.isKind(of: UITabBarController.self), let tab = rooterVC as? UITabBarController {
            rooterVC = tab.selectedViewController
        } else if let _ = rooterVC?.isKind(of: UINavigationController.self), let navi = rooterVC as? UINavigationController {
            rooterVC = navi.visibleViewController
        }
        return rooterVC
    }
}
