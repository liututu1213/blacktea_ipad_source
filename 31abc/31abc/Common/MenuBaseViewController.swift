//
//  MenuBaseViewController.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/1.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

class MenuBaseViewController: BaseViewController {
    var closeClosure: (() -> Void)?
    var headerTitle: String = "" {
        didSet {
            titleLabel.text = headerTitle
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(headerView)

        headerView.addSubview(titleLabel)
        headerView.addSubview(closeButton)
        headerView.addSubview(line)

        headerView.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(50)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(headerView).offset(15)
            make.top.equalTo(view)
            make.height.equalTo(50)
        }

        closeButton.snp.makeConstraints { (make) in
            make.right.equalTo(headerView).offset(-20)
            make.centerY.equalTo(titleLabel)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }

        line.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(-1)
            make.left.right.equalTo(headerView)
            make.height.equalTo(1)
        }
    }

    @objc func closeClick() {
        dismiss(animated: true)
        closeClosure?()
    }
    
    //MARK: - SubViews
    lazy var headerView: UIView = {
        let v = UIView()
        return v
    }()
    
    lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.text = "弹框名称"
        l.textColor = UIColor("#4F4F4F")
        l.font = UIFont.boldSystemFont(ofSize: 14)
        return l
    }()
    
    lazy var closeButton: UIButton = {
        let b = UIButton()
        b.setImage(UIImage(named: "close"), for: .normal)
        b.addTarget(self, action: #selector(closeClick), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var line: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("#ECECEC")
        return v
    }()
}
