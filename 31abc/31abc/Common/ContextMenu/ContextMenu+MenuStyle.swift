//
//  ContextMenu+Style.swift
//  ThingsUI
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

extension ContextMenu {

    /// Style options for menu behavior.
    public enum MenuStyle: Int {

        /// Show a navigation bar and header in the menu. Default.
        case `default`

        /// Hide the navigation bar in the menu.
        case minimal
    }

}
