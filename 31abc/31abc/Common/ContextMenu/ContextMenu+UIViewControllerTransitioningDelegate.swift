//
//  ContextMenu+UIViewControllerTransitioningDelegate.swift
//  ThingsUI
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

extension ContextMenu: UIViewControllerTransitioningDelegate {

    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let item = self.item else { return nil }
        return ContextMenuDismissing(item: item)
    }

    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let item = self.item else { return nil }
        return ContextMenuPresenting(item: item)
    }

    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        guard let item = self.item else { return nil }
        let controller = ContextMenuPresentationController(presentedViewController: presented, presenting: presenting, item: item)
        controller.contextDelegate = self
        return controller
    }

}
