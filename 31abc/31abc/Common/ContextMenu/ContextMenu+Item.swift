//
//  File.swift
//  ThingsUI
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

extension ContextMenu {

    class Item {

        let options: Options
        let viewController: ClippedContainerViewController

        weak var sourceView: UIView?
        weak var delegate: ContextMenuDelegate?

        init(
            viewController: UIViewController,
            options: Options,
            sourceView: UIView?,
            delegate: ContextMenuDelegate?
            ) {
            self.viewController = ClippedContainerViewController(options: options, viewController: viewController)
            self.options = options
            self.sourceView = sourceView
            self.delegate = delegate
        }
    }

}
