//
//  ContextMenuDelegate.swift
//  ContextMenu
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

public protocol ContextMenuDelegate: class {
    func contextMenuWillDismiss(viewController: UIViewController, animated: Bool)
    func contextMenuDidDismiss(viewController: UIViewController, animated: Bool)
}
