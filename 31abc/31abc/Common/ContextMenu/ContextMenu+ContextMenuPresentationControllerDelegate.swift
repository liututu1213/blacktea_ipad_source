//
//  ContextMenu+ContextMenuPresentationControllerDelegate.swift
//  ThingsUI
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

extension ContextMenu: ContextMenuPresentationControllerDelegate {

    func willDismiss(presentationController: ContextMenuPresentationController) {
        guard item?.viewController === presentationController.presentedViewController else { return }
        item = nil
    }
}
