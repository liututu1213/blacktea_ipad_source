//
//  ContextMenuOptions.swift
//  ThingsUI
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

extension ContextMenu {

    /// Display and behavior options for a menu.
    public struct Options {

        /// Animation durations and properties.
        let durations: AnimationDurations

        /// Appearance properties for the menu container.
        let containerStyle: ContainerStyle

        /// Style options for menu behavior.
        let menuStyle: MenuStyle

        /// Trigger haptic feedback when the menu is shown.
        let hapticsStyle: UIImpactFeedbackGenerator.FeedbackStyle?

        /// press Background, contentView will hide, default is true
        let backgroundHideAutomatically: Bool

        public init(
            durations: AnimationDurations = AnimationDurations(),
            containerStyle: ContainerStyle = ContainerStyle(),
            menuStyle: MenuStyle = .default,
            hapticsStyle: UIImpactFeedbackGenerator.FeedbackStyle? = nil,
            backgroundHideAutomatically: Bool = false
            ) {
            self.durations = durations
            self.containerStyle = containerStyle
            self.menuStyle = menuStyle
            self.hapticsStyle = hapticsStyle
            self.backgroundHideAutomatically = backgroundHideAutomatically
        }
    }
}
