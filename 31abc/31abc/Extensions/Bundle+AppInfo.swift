//
//  Bundle+AppInfo.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/20.
//  Copyright © 2019 31abc. All rights reserved..
//

import Foundation

extension Bundle {

    /// application name
    var bundleName: String {
        return self.object(forInfoDictionaryKey: kCFBundleNameKey as String) as! String
    }

    /// build number
    var bundleVersion: String {

        return self.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }

    var shortVersion: String {

        return self.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
}
