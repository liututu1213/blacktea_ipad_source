//
//  UIFontExtension.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/27.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

extension UIFont {
    /**
     *  Core Sans D
     *  使用场景：≥20号数字
     **/
    open class func regularCoreSansDFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "CoreSansD35Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    open class func mediumCoreSansDFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "CoreSansD45Medium", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    /**
     *  San Francisco Pro
     *  使用场景：1.英文字体
     *          2.<20号数字
     **/
    open class func regularSanFranciscoProFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProText-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    open class func mediumSanFranciscoProFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-Medium", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
    }
    
    /**
     *  苹方 (PingFang)
     *  使用场景：中文字体
     **/
    open class func regularPingFangFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "PingFangSC-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    open class func mediumPingFangFont(ofSize fontSize: CGFloat) -> UIFont {
        return UIFont(name: "PingFangSC-Medium", size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize)
    }
}

