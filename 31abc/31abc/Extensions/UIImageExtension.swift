//
//  UIImageExtension.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/27.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

extension UIImage {
    
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 10, height: 10)) {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, UIScreen.main.scale)
        
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        self.init(cgImage:(UIGraphicsGetImageFromCurrentImageContext()?.cgImage!)!)
        UIGraphicsEndImageContext()
    }
    
    public func repaintImage(tintColor: UIColor) -> UIImage {
        return repaintGradientImage(tintColor: tintColor) ?? self
    }
    
    fileprivate func repaintGradientImage(tintColor: UIColor) -> UIImage? {
        return repaintImage(tintColor: tintColor, blendMode: .overlay)
    }
    
    fileprivate func repaintImage(tintColor: UIColor, blendMode: CGBlendMode) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        tintColor.setFill()
        let bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIRectFill(bounds)
        
        if blendMode != .destinationIn {
            draw(in: bounds, blendMode: .destinationIn, alpha: 1)
        }
        
        let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return tintedImage
    }
}

extension UIImage {
    class func QRCodeImage(_ text: String, scale: CGFloat = 10) -> UIImage? {
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setDefaults()
        filter?.setValue(text.data(using: String.Encoding.utf8), forKey: "inputMessage")
        filter?.setValue("M", forKey: "inputCorrectionLevel")

        var outputImage = filter?.outputImage
        let transform = CGAffineTransform(scaleX: scale, y: scale)

        outputImage = outputImage?.transformed(by: transform)
        return outputImage.flatMap{UIImage(ciImage: $0)}
    }

    class func createClearImage(sourceImage: UIImage) -> UIImage? {
        let size = sourceImage.size
        UIGraphicsBeginImageContext(size)
        sourceImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let resultImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resultImage
    }
}

