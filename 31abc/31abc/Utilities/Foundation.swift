//
//  ABCFoundation.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/27.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

public let kScreenWidth = UIScreen.main.bounds.size.width
public let kScreenHeight = UIScreen.main.bounds.size.height
public let kScreenSize = UIScreen.main.bounds.size

enum PredicateValidate {
    case phone(_: Any?)
    case emial(_: Any?)
    case password(_: Any?)
    
    var vaild: Bool {
        var predicateText: String = ""
        var evaluateObject: Any?
        
        switch self {
        case let .phone(text):
            evaluateObject = text
            predicateText = "(1\\d{10})"
            
        default:
            break
        }
        let regexMobile = NSPredicate(format: "SELF MATCHES %@",predicateText)
        return regexMobile.evaluate(with: evaluateObject)
    }
}
