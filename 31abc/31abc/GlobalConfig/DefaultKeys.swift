//
//  DefaultKeys.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/9.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

extension DefaultKeys {
    static let environment = DefaultKey<String>("kEnvironment")
    static let debug       = DefaultKey<Bool>("kDebug")

    /// appid
    static let agoraAppId        = DefaultKey<String>("1553ebfc30cc4250a6fbd665d89dfda3")
    static let umAppId           = DefaultKey<String>("5d7db25e4ca357ed8d0000b9")

    static let accessToken       = DefaultKey<String>("accessToken")
    static let tokenType         = DefaultKey<String>("tokenType")
    static let refreshToken      = DefaultKey<String>("refreshToken")
    static let rememberPassword  = DefaultKey<Bool>("rememberPassword")
    static let firstDeviceCheck  = DefaultKey<Bool>("firstDeviceCheck")
    static let phoneNumber       = DefaultKey<String>("phoneNumber")

    static let classroom        = DefaultKey<String>("classroom")
}

struct DefaultSettings {
    static let defaults: [DefaultKeys: Any] = [
        .rememberPassword: true,
        .debug: false
    ]
}




