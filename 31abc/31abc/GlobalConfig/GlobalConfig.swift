//
//  GlobalConfig.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/12.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import Kingfisher
import Toast_Swift

public class GlobalConfig: NSObject {
    class func setup() {

        /// debug
        if UserDefaults.standard[.debug] {
            let downloader = KingfisherManager.shared.downloader
            downloader.trustedHosts = Set([ABCMoya.environment.server.domain, "cdn.31abc.com"])
            
            UMCommonLogManager.setUp()
            UMConfigure.setLogEnabled(false)

            ToastManager.shared.duration = 2.0
            ToastManager.shared.position = .center
        }
        
        //UM
        UMConfigure.initWithAppkey(DefaultKeys.umAppId.rawValue, channel: "App Store")
        MobClick.setScenarioType(.E_UM_NORMAL)

        _ = DeviceManager.shared
    }
}

extension GlobalConfig {
    static var base: GlobalConfigModel {
        return GlobalConfigModel.model()
    }
}

struct GlobalConfigModel {
    var register = ""
    /// ....

    public static func model() -> GlobalConfigModel {
        switch ABCMoya.environment {
        case .release:
            return GlobalConfigModel(register: "https://i.31abc.com/#/register")

        case .test:
            return GlobalConfigModel(register: "http://mp.yum.com:3332/#/login")
        }
    }
}
