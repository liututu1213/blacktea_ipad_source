//
//  LocalFileManager.swift
//  31abc
//
//  Created by Cathy on 2020/2/9.
//  Copyright © 2020 31abc. All rights reserved.
//

import UIKit

class LocalFileManager: NSObject {
    private struct Constants {
        static let saveTime = 7.0
    }
    
    class func cleanMediaFile() {
        let manager = FileManager.default
        
        let urlForDoucument = manager.urls(for: .documentDirectory, in: .allDomainsMask)
        guard let contentsOfPath = try? manager.contentsOfDirectory(atPath: NSHomeDirectory() + "/Documents/Media/") else { return }
        if urlForDoucument.count < 1 { return }
        
        let paths = contentsOfPath.map{ return "Media/" + $0}
        paths.forEach { (path) in
            let file = urlForDoucument[0].appendingPathComponent(path)
            
            guard let attri = (try? manager.attributesOfItem(atPath: file.path)) else { return }
            if let createDate = attri[FileAttributeKey.creationDate] {
                if let date = createDate as? Date, Date().daysSince(date) > Constants.saveTime {
                    
                    try? manager.removeItem(atPath: file.path)
                }
            }
        }
    }
}
