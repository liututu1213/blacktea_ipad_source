//
//  AppDelegate.swift
//  ABC4iPad
//
//  Created by hxy on 2019/9/17.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        GlobalConfig.setup()
        window = UIWindow(frame: UIScreen.main.bounds)
        switchRooterViewController(rooterViewController())
        window?.makeKeyAndVisible()

        checkUpgrade()
        LocalFileManager.cleanMediaFile()
        return true
    }
    
    func rooterViewController() -> UIViewController {
        let accountManager = AccountManager.shared
        guard UserDefaults.standard[.rememberPassword],
            let token = accountManager.loginModel?.access_token,
            token.count > 0 else {
                return LoginViewController()
        }
        return HomeViewController()
    }

    func switchRooterViewController(_ rooter: UIViewController) {
        let nav = UINavigationController(rootViewController: rooter)
        nav.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = nav

        /// Notification
        if !rooter.isKind(of: LoginViewController.self) {
            NotificationCenter.default.post(name: .LoginNotification, object: nil)
        } else {
            NotificationCenter.default.post(name: .LogoutNotification, object: nil)
        }
    }

    fileprivate func checkUpgrade() {
        NetWorkRequest(.appSetup(3)) { (res) in
            switch res {
            case .success(let json):
                
                let dic = json as? [String :Any]
                if let setupInfo = AppSetUp.deserialize(from: dic) {
                    if let images = setupInfo.background, images.count > 0  {
                        AccountManager.shared.loadBackgroundImages(images)
                    }
                    
                    if let version = setupInfo.version, version.isShowAlert {
                        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                        guard let visibleVC = NavigationTool.shared.visibleViewController() else { return }
                        let options = ContextMenu.Options(containerStyle: ContextMenu.ContainerStyle(overlayColor: color),
                                                          menuStyle: .minimal,
                                                          backgroundHideAutomatically: !version.is_must)
                        
                        let vc = UpgradeViewController()
                        vc.version = version
                        ContextMenu.shared.show(sourceViewController: visibleVC,
                                                viewController: vc ,
                                                options: options)
                    }
                }
                
            default:()
            }
        }
    }
    
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

        let accountManager = AccountManager.shared
        guard UserDefaults.standard[.rememberPassword],
            let token = accountManager.loginModel?.access_token,
            token.count > 0 else {
                switchRooterViewController(LoginViewController())
                return true
        }

        if let host = url.host, host == DefaultKeys.classroom.rawValue {
            let id = url.path.replacingOccurrences(of: "/", with: "")
            if let session_id: Int = Int(id) {
                ClassRoomManager.shared.getClassRoomInfo(lesson_section_id: session_id)
            }
        }
        return true
    }
}



