//
//  PreviewViewController.swift
//  31abc
//
//  Created by Cathy on 2020/3/14.
//  Copyright © 2020 31abc. All rights reserved.
//

import UIKit
import WebKit

class PreviewViewController: BaseViewController {

    var url: URL

    init(url: URL) {
        self.url = url

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        layoutUI()
        layoutNavigationItem()
        
        view.backgroundColor = UIColor.main
        loadURL()
    }

    fileprivate func loadURL() {
        webView.load(URLRequest(url: url))
    }
    
    //MARK: - Actions
    @objc func refreshClick() {
        webView.reload()
    }

    fileprivate func back() {
        navigationController?.popViewController(animated: true)
    }

    // MARK: - setupUI
    func layoutNavigationItem() {
        view.addSubview(backButton)
        view.addSubview(refreshButton)

        backButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.top.equalTo(view).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(53)
        }

        refreshButton.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-20)
            make.top.equalTo(view).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(53)
        }
    }

    func layoutUI() {
        view.addSubview(webView)
        webView.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
    }

    // MARK: - SubViews
    fileprivate lazy var backButton: AnimationButton = {
        let b = AnimationButton()
        b.cornerRadius = 5.0
        b.image = UIImage(named: "arrow_left")?.repaintImage(tintColor: UIColor("#745B1A"))
        b.clickClosure = {[weak self] (button) in
            self?.back()
        }
        return b
    }()

    fileprivate lazy var refreshButton: AnimationButton = {
        let b = AnimationButton()
        b.cornerRadius = 5.0
        b.title = "刷新"
        b.setTitleColor(UIColor("#745B1A"), for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        b.clickClosure = {[weak self] (button) in
            self?.refreshClick()
        }
        return b
    }()

    fileprivate lazy var webView: WKWebView = {
        let config = WKWebViewConfiguration()
        config.allowsInlineMediaPlayback = true
        let wk = WKWebView(frame: .zero, configuration: config)
        wk.navigationDelegate = self
        wk.uiDelegate = self
        wk.scrollView.alwaysBounceVertical = false
        wk.scrollView.bouncesZoom = false
        wk.scrollView.isScrollEnabled = false
        wk.backgroundColor = .clear
        wk.scrollView.backgroundColor = .clear
        return wk
    }()
}

extension PreviewViewController: WKNavigationDelegate, WKUIDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopAnimating()
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        startAnimating()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        stopAnimating()
    }
}
