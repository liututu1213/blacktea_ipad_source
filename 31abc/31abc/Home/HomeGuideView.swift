//
//  ABCHomeGuideView.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/27.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class HomeGuideView: UIView {

    struct GuideItem {
        var type: CourseType = .live
        var image: UIImage?
        var title: String
    }

    var selectedClosure: ((CourseType) -> Void)?
    var dataSource: [GuideItem] = [] {
        didSet {
            reloadData()
        }
    }

    private var itemViews: [HomeGuideItemView] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
        layer.cornerRadius = 10.0
        layer.masksToBounds = true

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func reloadData() {
        subviews.forEach { (view) in
            view.removeFromSuperview()
        }
        let top: CGFloat = 20.0
        let height: CGFloat = 68.0
        let margin: CGFloat = 5.0
        var temp: [HomeGuideItemView] = []
        dataSource.enumerated().forEach { (index, item) in

            let itemView = HomeGuideItemView(title: item.title, image: item.image)
            itemView.tag = index
            itemView.isSelected = index == 0
            let tap = UITapGestureRecognizer(target: self, action: #selector(selectedItemClick(_:)))
            itemView.addGestureRecognizer(tap)
            temp.append(itemView)

            addSubview(itemView)
            itemView.snp.makeConstraints { (make) in
                make.top.equalTo(self).offset((height + margin) * CGFloat(index) + top)
                make.left.equalTo(self).offset(10)
                make.right.equalTo(self).offset(-10)
                make.height.equalTo(68)
            }
        }

        itemViews = temp
    }
    
    @objc func selectedItemClick(_ tapGestureRecognizer: UITapGestureRecognizer) {
        if let tag = tapGestureRecognizer.view?.tag {
            itemViews.forEach { (view) in
                view.isSelected = view.tag == tag
            }
            guard let type = CourseType(rawValue: tag) else { return }
            selectedClosure?(type)
        }
    }
}

class HomeGuideItemView: UIView {

    var image: UIImage?
    var isSelected: Bool {
        set {
            _isSelected = newValue
            if newValue == true {
                imageView.image = image?.repaintImage(tintColor: UIColor("#4F4F4F"))
                titleLabel.textColor = UIColor("#4F4F4F")
                backgroundColor = .white
            } else {
                imageView.image = image?.repaintImage(tintColor: .white)
                backgroundColor = .clear
                titleLabel.textColor = .white
            }
        }
        get {
            return _isSelected
        }
    }
    private var _isSelected: Bool = false
    
    init(title: String, image: UIImage?) {
        self.image = image
        super.init(frame: .zero)
        
        layer.cornerRadius = 5.0
        layer.masksToBounds = true
        titleLabel.text = title
        
        isSelected = false
        
        addSubview(titleLabel)
        addSubview(imageView)
        
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(10)
            make.centerX.equalTo(self)
            make.width.equalTo(24)
            make.height.equalTo(26)
        }
        
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(10)
            make.left.equalTo(self).offset(5)
            make.right.equalTo(self).offset(-5)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 14)
        l.textAlignment = .center
        return l
    }()
    
    fileprivate lazy var imageView: UIImageView = {
        let i = UIImageView()
        return i
    }()
}

