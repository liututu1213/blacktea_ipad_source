//
//  ClassRoom.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/10.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import HandyJSON

public enum RoleType: Int, HandyJSONEnum {
    case student
    case teacher
}

public enum StatusType: Int, HandyJSONEnum {
    case join = 0
    case leave = 1
}

class CoursewareMedia: HandyJSON {
    enum `Type`: Int, HandyJSONEnum {
        case video
        case audio
    }
    public required init() {}

    var type: Type = .video
    var id = ""
    var url = ""
}

class ClassRoom: HandyJSON {
    public required init() {}
    class  ClassModel: HandyJSON {
        public required init() {}
        class Student: HandyJSON {
            public required init() {}
            var id = 0
            var name = ""
            var user_id: UInt = 0
        }

        var id = 0
        var name = ""
        var student: [Student] = []
    }

    class Coach: HandyJSON {
        public required init() {}
        var name = ""
        var header = ""
        var user_id: UInt?
    }

    class Lesson: HandyJSON {
        public required init() {}
        var time_length: Double = 0.0
        var start_time: Date?

        func mapping(mapper: HelpingMapper) {
            mapper <<< start_time
                       <-- DateFormatterTransform(dateFormatter: defalutDateFormatter)

        }
    }

    var courseware_url: URL?
    var channel_id: Int = 0
    var coach: Coach?
    var `class`: ClassModel?
    var lesson_section:Lesson?
    var courseware_multi_media: [CoursewareMedia] = []

    func mapping(mapper: HelpingMapper) {

        mapper <<< courseware_url
            <-- URLTransform(shouldEncodeURLString: true)

    }
}

class ChannelInfo: HandyJSON {
    class DeviceInfo: HandyJSON {
        public required init() {}

        var operating_system = "ios"
        var version = UIDevice.current.systemVersion
        var type = "iPad"
    }

    public required init() {}

    var lesson_section_id = 0
    var channel_id = 0
    var time = 0
    var type: StatusType = .join
    var role: RoleType = .student
    var client_version = Bundle.main.shortVersion
    var device_info = DeviceInfo()
}

