//
//  HomeModel.swift
//  4iPad
//
//  Created by Cathy on 2019/9/29.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import HandyJSON

class Hierarchy: HandyModel {
    var level: Int = 0
    var name = ""
    var short_name = ""
}

class BookTopic: HandyJSON {
    public required init() {}
    var topic_lesson_id: Int = 0
    var name = ""
    var cover: URL?
    
    func mapping(mapper: HelpingMapper) {
        
        mapper <<< cover
            <-- URLTransform(shouldEncodeURLString: true)
    }
}

class Book: HandyModel {
    var name = ""
    var hierarchy: [Hierarchy] = []
    var progress = ""
    var topic: BookTopic?
    
    /// 业务逻辑
    var detail: String {
        var detail = ""
        let progressArr = progress.split(separator: "-")
        let count = max(progressArr.count, hierarchy.count)
        
        for index in 0..<count {
            var hierarchyName = ""
            var progressName = ""
            
            if index == count - 1 {
                hierarchyName = "- "
            }
            if index < hierarchy.count {
                hierarchyName = hierarchyName.appending(hierarchy[index].name)
            }
            if index < progressArr.count {
                progressName = String(progressArr[index])
            }
            
            detail.append(hierarchyName + " " + progressName + " ")
        }
        return detail
    }
}

class LessonSection: HandyJSON {
    enum RecordStatus: Int, HandyJSONEnum {
        case undone = 0
        case done
        case invalid
    }
    var start_time: Date = Date()
    var text_book: Book?
    var preview_url: String = ""
    var review_url: String = ""
    var preview_type: Int = 0
    var id: Int = 0
    var recording_status: RecordStatus = .undone
    var has_video: Bool = false
    var ios_recording_url: URL?
    var platform: Int = 0
    
    var enableToRoom: Bool {
        return false
    }

    /// yyyy-MM-dd
    var time: String {
        return start_time.string(withFormat: "yyyy-MM-dd")
    }

    var startTimeStr: String {
        if start_time.isInToday {
            return "今天 " + start_time.string(withFormat: "HH:mm")
        }
        if  start_time.isInCurrentYear {
            return start_time.string(withFormat: "MM月dd日 EEEE HH:mm")
        }
        return start_time.string(withFormat: "yyyy年MM月dd日 EEEE")
    }
    
    func mapping(mapper: HelpingMapper) {

        mapper <<< start_time
            <-- DateFormatterTransform(dateFormatter: defalutDateFormatter)

        mapper <<< ios_recording_url
                   <-- URLTransform(shouldEncodeURLString: true)
    }
    
     public required init() {}
}

class HomeModel: HandyJSON {
    var lesson_section: [LessonSection] = []
    var student: Student?
    var now_date: Date = Date()

    required init() {}

    func mapping(mapper: HelpingMapper) {
        
        mapper <<< now_date
            <-- DateFormatterTransform(dateFormatter: defalutDateFormatter)
    }
}

/// 业务数据 - 按照日期进行拆分成组
class SectionModel {
    var lesson_section: [LessonSection] = []
    var time: String = ""
}

///已完成课程
class CourseCompleteData: HandyJSON {
    required init() {}
    var total: Int = 0
    var perPage: Int = 0
    var currentPage: Int = 0
    var list: [LessonSection] = []
}
