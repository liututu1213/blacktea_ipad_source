//
//  CheckMenuView.swift
//  4iPad
//
//  Created by Cathy on 2019/9/28.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class MenuView: UIView {

    public var isShowing: Bool = false

    private var animationFinisned: Bool = true
    private var rect: CGRect = .zero
    override init(frame: CGRect) {
        super.init(frame: frame)
        alpha = 0
        
        backgroundColor = UIColor("#FCDE93")
        layer.cornerRadius = 10
        layer.masksToBounds = true
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 5
        
        addSubview(checkButton)
        addSubview(loginOutButton)
        addSubview(aboutButton)
        
        checkButton.snp.makeConstraints { (make) in
            make.top.equalTo(self).offset(15)
            make.left.equalTo(self).offset(10)
            make.right.equalTo(self).offset(-10)
            make.height.equalTo(36)
            make.width.equalTo(104)
        }
        
        loginOutButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(checkButton)
            make.top.equalTo(checkButton.snp.bottom).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(104)
        }

        aboutButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(checkButton)
            make.top.equalTo(loginOutButton.snp.bottom).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(104)
            make.bottom.equalTo(self).offset(-15)
        }
    }
    
    func showAnimation(_ rect: CGRect) {
        guard animationFinisned else { return }
        animationFinisned = false
        isShowing = true

        self.rect = rect
        transform = CGAffineTransform(
            a: 1,
            b: 0,
            c: 0,
            d: 0,
            tx: rect.midX - frame.midX,
            ty: rect.midY - frame.midY
        ).scaledBy(x: 0.01, y: 0.01)
        
        alpha = 0
        UIView.animate(withDuration: 0.25, animations: {
            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
            self.alpha = 1
            
        }) { (finished) in
            self.animationFinisned = true
        }
    }
    
    func dismissAnimation(completion: ((Bool) -> Void)?) {
        guard animationFinisned else { return }
        animationFinisned = false
        isShowing = false

        transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        UIView.animate(withDuration: 0.25, animations: {
            self.transform = CGAffineTransform(
                a: 1,
                b: 0,
                c: 0,
                d: 1,
                tx: self.rect.midX - self.frame.midX,
                ty: self.rect.midY - self.frame.midY
            ).scaledBy(x: 0.01, y: 0.01)
            self.alpha = 0
        }) { (finished) in
            self.animationFinisned = true
        }
    }

    func selectedButton(_ button: UIButton) {
        checkButton.isSelected = false
        loginOutButton.isSelected = false
        aboutButton.isSelected = false

        button.isSelected = true

        checkButton.backgroundColor = checkButton.isSelected ? UIColor("#F2C94C") : UIColor("#FCDE93")
        loginOutButton.backgroundColor = loginOutButton.isSelected ? UIColor("#F2C94C") : UIColor("#FCDE93")
        aboutButton.backgroundColor = aboutButton.isSelected ? UIColor("#F2C94C") : UIColor("#FCDE93")
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var checkButton: UIButton = {
        let b = UIButton()
        b.setTitle("设备检测", for: .normal)
        b.setTitleColor(UIColor("#4F4F4F"), for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.backgroundColor = UIColor("#F2C94C")
        b.isSelected = true
        b.layer.cornerRadius = 18;
        b.layer.masksToBounds = true
        return b
    }()
    
   lazy var loginOutButton: UIButton = {
        let b = UIButton()
        b.setTitle("退出登录", for: .normal)
        b.setTitleColor(UIColor("#4F4F4F"), for: .normal)
        b.backgroundColor = UIColor("#FCDE93")
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.isSelected = false
        b.layer.cornerRadius = 18;
        b.layer.masksToBounds = true
        return b
    }()

    lazy var aboutButton: UIButton = {
        let b = UIButton()
        b.setTitle("关于", for: .normal)
        b.setTitleColor(UIColor("#4F4F4F"), for: .normal)
        b.backgroundColor = UIColor("#FCDE93")
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.isSelected = false
        b.layer.cornerRadius = 18;
        b.layer.masksToBounds = true
        return b
    }()
}
