//
//  HomeTableViewCell.swift
//  4iPad
//
//  Created by Cathy on 2019/9/28.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

let kClassCollectionItemWidth: CGFloat = 380
let kClassCollectionItemHeight: CGFloat = 550

let kClassCollectionWidth: CGFloat = 790
let kClassCollectionMargin: CGFloat = 50
let kClassCollectionItemLineSpacing: CGFloat = 50
let kClassCollectionCornerRadius: CGFloat = 20

class HomeTableViewCell: UITableViewCell {
    
    var lesson_section: [LessonSection] = []

    private var type: CourseType = .live
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .clear
        selectionStyle = .none
        
        contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(kClassCollectionMargin)
            make.bottom.equalTo(contentView).offset(-kClassCollectionMargin)
            make.width.equalTo(kClassCollectionWidth)
            make.left.equalTo(contentView).offset(30)
        }
        collectionView.backgroundColor = .clear
    }
    
    func updateUI(lesson: [LessonSection], type: CourseType) {
        self.type = type
        lesson_section = lesson
        collectionView.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = kClassCollectionItemLineSpacing
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: kClassCollectionItemWidth, height: kClassCollectionItemHeight)
        
        let collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        collectionView.register(HomeClassCollectionCell.self, forCellWithReuseIdentifier: NSStringFromClass(HomeClassCollectionCell.self))
        return collectionView
    }()
}

extension HomeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return lesson_section.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let lesson = lesson_section[indexPath.item]
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NSStringFromClass(HomeClassCollectionCell.self), for: indexPath) as? HomeClassCollectionCell else {
            return UICollectionViewCell()
        }
        
        cell.updateUI(lesson: lesson, type: type)
        return cell
    }
}

class HomeClassCollectionCell: UICollectionViewCell {
    var lesson: LessonSection = LessonSection()

    private var type: CourseType = .live
    private let kTimeLabelMinWidth: CGFloat = 150.0
    private let kTImeLabelHeight: CGFloat = 60.0
    private let kTimeLabelMargin: CGFloat = 15.0
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .clear
        layoutUI()
    }
    
    fileprivate func updateUI(lesson: LessonSection, type: CourseType) {
        self.type = type
        let timeString = lesson.startTimeStr
        self.lesson = lesson

        let size = timeString.boundingRect(
            with: CGSize(width: kClassCollectionItemWidth, height: CGFloat(MAXFLOAT)),
            options: NSStringDrawingOptions.usesLineFragmentOrigin,
            attributes: [.font: timeLabel.font!],
            context: nil).size

        let mx = max(size.width + 2 * 15, kTimeLabelMinWidth)
        let m = min(mx, kClassCollectionItemWidth)
        timeLabel.text = timeString

        timeLabel.snp.updateConstraints { (make) in
            make.width.equalTo(m)
        }

        classNameLabel.text = lesson.text_book?.topic?.name
        detailLabel.text = lesson.text_book?.detail

        let now = AccountManager.shared.now_date
        let minutes = lesson.start_time.minutesSince(now)

        if type == .recording {
            switch lesson.recording_status {
            case .undone:
                liveButton.setTitle("回放生成中", for: .normal)
                liveButton.isEnabled = false
            case .done:
                liveButton.isEnabled = true
                liveButton.setTitle("课程回放", for: .normal)
            case .invalid:
                liveButton.setTitle("课程回放", for: .normal)
                liveButton.isEnabled = false
            }

        } else {
            liveButton.isEnabled = minutes <= 30
            liveButton.setTitle("开始上课", for: .normal)
        }


        if type == .recording, lesson.preview_type == 1 {
            prepareButton.setTitle("复习", for: .normal)
            prepareButton.isHidden = lesson.review_url.count == 0
        } else {
            prepareButton.setTitle("预习", for: .normal)
            prepareButton.isHidden = lesson.preview_url.count == 0
        }

        if let url = lesson.text_book?.topic?.cover {
            classImageView.kf.setImage(with: url)
        }
    }

    // MARK: - Actions
    @objc func previewButtonClick() {
        if lesson.preview_type == 2 {
            guard let url = URL(string: lesson.preview_url) else { return }
            let vc = PreviewViewController(url: url)
            NavigationTool.shared.pushViewController(vc)
        } else if lesson.preview_type == 1 {
            let urlString = (type == .live) ? lesson.preview_url : lesson.review_url
            guard let visibleVC = NavigationTool.shared.visibleViewController(), urlString.count > 0 else { return }

            let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
            let options = ContextMenu.Options(containerStyle: ContextMenu.ContainerStyle(overlayColor: color),
                                              menuStyle: .minimal,
                                              backgroundHideAutomatically: true)
            let vc = QRCodeViewController()
            vc.previewUrl = urlString
            ContextMenu.shared.show(sourceViewController: visibleVC,
                                    viewController: vc ,
                                    options: options)
        }
    }

    @objc func liveButtonClick() {
        switch type {
        case .recording:

            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
            ClassRoomManager.shared.getRecording_agroa(lesson_section_id: lesson.id) {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            }

        case .live:
            liveButton.isEnabled = false
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())

            ClassRoomManager.shared.getClassRoomInfo(lesson_section_id: lesson.id) {[weak self] in
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
                self?.liveButton.isEnabled = true
            }
        }
    }
    
    fileprivate func layoutUI() {
        contentView.addSubview(timeLabel)
        contentView.addSubview(itemContentView)
        
        timeLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(contentView)
            make.width.equalTo(kTimeLabelMinWidth)
            make.height.equalTo(kTImeLabelHeight)
        }
        
        itemContentView.snp.makeConstraints { (make) in
            make.top.equalTo(timeLabel.snp.bottom).offset(25)
            make.left.equalTo(contentView)
            make.centerX.equalTo(contentView)
            make.height.equalTo(466)
        }
        
        itemContentView.addSubview(classNameLabel)
        itemContentView.addSubview(detailLabel)
        itemContentView.addSubview(classImageView)
        itemContentView.addSubview(prepareButton)
        itemContentView.addSubview(liveButton)
        
        classNameLabel.snp.makeConstraints { (make) in
            make.left.top.equalTo(itemContentView).offset(15)
        }
        
        detailLabel.snp.makeConstraints { (make) in
            make.left.equalTo(classNameLabel)
            make.top.equalTo(classNameLabel.snp.bottom).offset(5)
        }
        
        classImageView.snp.makeConstraints { (make) in
            make.top.equalTo(detailLabel.snp.bottom).offset(15)
            make.left.equalTo(classNameLabel)
            make.width.equalTo(350)
            make.height.equalTo(263)
        }
        
        prepareButton.snp.makeConstraints { (make) in
            make.left.equalTo(classNameLabel)
            make.bottom.equalTo(itemContentView).offset(-30)
            make.width.equalTo(120)
            make.height.equalTo(58)
        }
        
        liveButton.snp.makeConstraints { (make) in
            make.right.equalTo(itemContentView).offset(-15)
            make.bottom.equalTo(itemContentView).offset(-30)
            make.width.equalTo(155)
            make.height.equalTo(58)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - SubViews
    fileprivate lazy var timeLabel: UILabel = {
        let l = UILabel()
        l.backgroundColor = UIColor("#F2C94C")
        l.layer.borderColor = UIColor.white.cgColor
        l.layer.borderWidth = 5
        l.layer.cornerRadius = kClassCollectionCornerRadius
        l.layer.masksToBounds = true
        l.font = UIFont.boldSystemFont(ofSize: 24)
        l.textColor = .white
        l.textAlignment = .center
        return l
    }()
    
    fileprivate lazy var itemContentView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("#FCF0C4")
        v.layer.borderColor = UIColor.white.cgColor
        v.layer.borderWidth = 5
        v.layer.cornerRadius = kClassCollectionCornerRadius
        v.layer.masksToBounds = true
        return v
    }()
    
    fileprivate lazy var classNameLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.boldSystemFont(ofSize: 24)
        l.textColor = UIColor("#4F4F4F")
        l.textAlignment = .center
        return l
    }()
    
    fileprivate lazy var detailLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.boldSystemFont(ofSize: 14)
        l.textColor = UIColor("#4F4F4F")
        l.textAlignment = .center
        return l
    }()
    
    fileprivate lazy var classImageView: UIImageView = {
        let i = UIImageView()
        i.layer.cornerRadius = 10
        i.layer.masksToBounds = true
        i.layer.borderColor = UIColor.white.cgColor
        i.layer.borderWidth = 5
        return i
    }()
    
    fileprivate lazy var prepareButton: UIButton = {
        let b = UIButton()
        b.layer.cornerRadius = 29
        b.layer.masksToBounds = true
        b.setBackgroundImage(UIImage(color: UIColor("#F2994A")), for: .normal)
        b.setBackgroundImage(UIImage(color: UIColor("#ECD29E")), for: .disabled)
        b.setImage(UIImage(named: "Preview")?.repaintImage(tintColor: .white), for: .normal)
        b.addTarget(self, action: #selector(previewButtonClick), for: .touchUpInside)
        b.setTitle("预习", for: .normal)
        b.setTitleColor(.white, for: .normal)
        return b
    }()
    
    fileprivate lazy var liveButton: UIButton = {
        let b = UIButton()
        b.layer.cornerRadius = 29
        b.layer.masksToBounds = true
        b.setBackgroundImage(UIImage(color: UIColor("#1BB982")), for: .normal)
        b.setBackgroundImage(UIImage(color: UIColor("#C4DFB1")), for: .disabled)
        b.setImage(UIImage(named: "Live")?.repaintImage(tintColor: .white), for: .normal)
        b.addTarget(self, action: #selector(liveButtonClick), for: .touchUpInside)
        b.setTitle("开始上课", for: .normal)
        b.setTitleColor(.white, for: .normal)
        return b
    }()
}
