//
//  QRCodeViewController.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/1.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class QRCodeViewController: MenuBaseViewController {

    var previewUrl: String = "" {
        didSet {
            qrCodeImageView.image = UIImage.QRCodeImage(previewUrl)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        preferredContentSize = CGSize(width: 284, height: 315)
        headerTitle = "预习"

        view.addSubview(descLabel)
        view.addSubview(qrCodeImageView)

        descLabel.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom).offset(20)
            make.centerX.equalTo(view)
        }

        qrCodeImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(descLabel.snp.bottom).offset(16)
            make.width.height.equalTo(164)
        }
    }

    fileprivate lazy var descLabel: UILabel = {
        let l = UILabel()
        l.text = "微信扫一扫 开始预习"
        l.textColor = UIColor("#4F4F4F")
        l.font = UIFont.systemFont(ofSize: 14)
        return l
    }()

    fileprivate lazy var qrCodeImageView: UIImageView = {
        let i = UIImageView()
        i.backgroundColor = .white
        return i
    }()
}
