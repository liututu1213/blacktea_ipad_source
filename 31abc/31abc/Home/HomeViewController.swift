//
//  HomeViewController.swift
//  4iPad
//
//  Created by Cathy on 2019/9/25.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import SnapKit
import Toast_Swift
import AgoraRtmKit
import Alamofire

enum CourseType: Int {
    case live = 0
    case recording
}

class HomeViewController: BaseViewController {
    private var completeData: CourseCompleteData?
    private var currentPage: Int = 1
    private var loading: Bool = false
    private var sections: [SectionModel] = []
    private var isReloadData: Bool = true
    private var networkReachability: Bool = true
    private var network: NetworkReachabilityManager?
    private var currentType: CourseType = .live {
        didSet {
            if oldValue != currentType {
                loadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor("#48D499")
        backgroundImageView.contentMode = .scaleToFill
        backgroundImageView.image = AccountManager.shared.getBackgroundImage(PageType.home.imageName)
        startListening()

        layoutUI()
        setupFooterView()
        loadBackgroundImage()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isReloadData = true
        loadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setup()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isReloadData = false
    }

    deinit {
        stopListening()
    }

    private func setupFooterView() {

        tableView.mj_footer = MJRefreshAutoNormalFooter(refreshingBlock: {[weak self] in

            print("--beginRefreshing---")
            if let data = self?.completeData, self?.currentType == .recording {
                if data.perPage * data.currentPage < data.total {
                    self?.loadRecordingData(page: data.currentPage + 1, perPage: data.perPage)
                } else {
                    self?.tableView.mj_footer?.endRefreshingWithNoMoreData()
                }
            }
        })
        tableView.mj_footer?.isHidden = true
    }

    fileprivate func setup() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            if !UserDefaults.standard[.firstDeviceCheck] {
                self.showDeviceCheckView()
                UserDefaults.standard[.firstDeviceCheck] = true
            }
        }
    }

    fileprivate func startListening() {
        network = NetworkReachabilityManager()
        network?.listener = {[weak self] (status) in
            guard let self = self else { return }
            switch status {
            case .reachable:
                if !self.networkReachability && self.currentType == .live {
                    self.loadData()
                }
                self.networkReachability = true
            default:
                self.networkReachability = false
                break
            }
        }

        network?.startListening()
    }

    fileprivate func stopListening() {
        NetworkReachabilityManager()?.stopListening()
    }

    fileprivate func showDeviceCheckView() {
        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let options = ContextMenu.Options(containerStyle: ContextMenu.ContainerStyle(overlayColor: color), menuStyle: .minimal)
        let vc = DeviceViewController()
        ContextMenu.shared.show(sourceViewController: self,
                                viewController: vc ,
                                options: options)
    }

    fileprivate func loadBackgroundImage() {
        AccountManager.shared.downloadImageCompletion = {[weak self](image, type) in
            if type == .home {
                self?.backgroundImageView.image = image
            }
        }
    }

    fileprivate func guideViewSelectedItem(_ type: CourseType) {
        currentType = type
        tableView.mj_footer?.isHidden = type == .live

    }
    
    fileprivate func loadData() {
        currentType == .live ? loadLiveCourse() : loadRecordingData()
    }

    func loadLiveCourse() {
        startAnimating()
        NetWorkRequest(.home) {[weak self] (res) in
            self?.stopAnimating()
            switch res {
            case .success(let json):

                let dic = json as? [String :Any]
                if let homeModel = HomeModel.deserialize(from: dic) {
                    self?.updateStudentInfo(homeModel)
                    self?.handleData(homeModel)
                    self?.tableView.reloadData()
                    self?.fireToRefresh(homeModel)
                }
            case .failure:()
            }
        }
    }

    func loadRecordingData(page: Int = 1, perPage: Int = 10) {
        startAnimating()
        NetWorkRequest(.history(page, perPage)) {[weak self] (res) in
            guard let `self` = self else { return }
            self.stopAnimating()
            self.loading = false
            self.tableView.mj_footer?.endRefreshing()
            switch res {
            case .success(let json):
                let dic = json as? [String :Any]
                if let model = CourseCompleteData.deserialize(from: dic) {
                    self.completeData = model
                    
                    let filter = model.list.filter({$0.platform == 1})
                    
                    if model.currentPage > self.currentPage, self.sections.count > 0 {
                        var temp = self.sections[0].lesson_section
                        temp.append(contentsOf: filter)
                        self.sections[0].lesson_section = temp
                    } else {
                        let item = SectionModel()
                        item.lesson_section = filter
                        self.sections = [item]
                    }
                    
                    self.tableView.reloadData()
                }
                
            case .failure:()
            }
        }
    }

    fileprivate func fireToRefresh(_ homeModel: HomeModel) {
        ///重新刷新UI
        let minSeconds = homeModel.lesson_section
            .map{$0.start_time.secondsSince(homeModel.now_date) - Double(30 * 60)}
            .filter{$0 > 0.0}
            .sorted(by: <)
            .first
        
        guard let seconds = minSeconds else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {[weak self] in
            guard let `self` = self else { return }
            if self.isReloadData && self.currentType == .live {
                self.loadData()
            }
        }
    }
    
    fileprivate func handleData(_ homeModel: HomeModel) {
        let set = Set(homeModel.lesson_section.map{$0.time})
        sections =  set.map{ time -> SectionModel in
            let item = SectionModel()
            item.lesson_section = homeModel.lesson_section.filter{ $0.time == time}.sorted(by: {$0.start_time < $1.start_time})
            item.time = time
            return item
        }.sorted(by: {$0.time < $1.time})
    }

    fileprivate func updateStudentInfo(_ homeModel: HomeModel) {
        AccountManager.shared.student = homeModel.student
        AccountManager.shared.now_date = homeModel.now_date

        if let name = homeModel.student?.name {
            nameLabel.text = name
        }

        if let header = homeModel.student?.header, header.count > 0 {
            profileImageView.kf.setImage(with: URL(string: header))
        }
    }

    //MARK: - Actions
    @objc func refreshClick() {
        loadData()
    }

    @objc func checkClick(_ button: UIButton) {
        menuView.selectedButton(button)
        showDeviceCheckView()
    }

    @objc func logoutClick(_ button: UIButton) {
        menuView.selectedButton(button)

        NetWorkRequest(.logout) { (res) in
            switch res {
            case .success:
                AccountManager.shared.loginModel = nil
                let a = UIApplication.shared.delegate as! AppDelegate
                a.switchRooterViewController( LoginViewController())

            default:()
            }
        }
    }

    @objc func aboutClick(_ button: UIButton) {
        menuView.selectedButton(button)

        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let options = ContextMenu.Options(containerStyle: ContextMenu.ContainerStyle(overlayColor: color),
                                          menuStyle: .minimal,
                                          backgroundHideAutomatically: true)
        let vc = AboutViewController()
        ContextMenu.shared.show(sourceViewController: self,
                                viewController: vc ,
                                options: options)
    }

    fileprivate func menuButtonClick(_ button:UIButton) {
        if !menuView.isShowing {
            menuView.showAnimation(button.frame)
        } else {
            menuView.dismissAnimation(completion: nil)
        }
    }

    //MARK: - layoutUI
    fileprivate func layoutUI() {
        view.addSubview(tableView)

        view.addSubview(nameBackgroundView)
        view.addSubview(nameLabel)
        view.addSubview(profileImageView)

        view.addSubview(guideView)
        view.addSubview(menuView)
        view.addSubview(menuButton)
        view.addSubview(refreshButton)

        guideView.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(25)
            make.top.equalTo(view).offset(150)
            make.height.equalTo(380)
            make.width.equalTo(99)
        }

        /// profile
        profileImageView.snp.makeConstraints { (make) in
            make.left.top.equalTo(view).offset(30)
            make.width.height.equalTo(70)
        }

        nameBackgroundView.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileImageView)
            make.width.equalTo(48)
            make.height.equalTo(48)
            make.left.equalTo(profileImageView.snp.centerX)
        }
        
        nameLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(profileImageView)
            make.height.equalTo(48)
            make.left.equalTo(profileImageView.snp.right).offset(-10)
            make.width.greaterThanOrEqualTo(60)
        }

        /// menu
        menuButton.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(25)
            make.right.equalTo(view).offset(-25)
            make.height.equalTo(44)
            make.width.equalTo(72)
        }
        
        menuView.snp.makeConstraints { (make) in
            make.right.equalTo(menuButton)
            make.top.equalTo(menuButton.snp.bottom).offset(10)
        }
        
        refreshButton.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-10)
            make.bottom.equalTo(view).offset(-30)
            make.width.height.equalTo(56)
        }
        
        tableView.snp.makeConstraints { (make) in
            make.left.equalTo(guideView.snp.right).offset(20)
            make.top.right.equalTo(view)
            make.bottom.equalTo(view)
        }
    }

    //MARK: - SubViews
    fileprivate lazy var guideView: HomeGuideView = {
        let v = HomeGuideView()
        v.dataSource = [
            HomeGuideView.GuideItem(type: .live, image: UIImage(named: "Preview"), title: "待上课程"),
            HomeGuideView.GuideItem(type: .recording, image: UIImage(named: "complete_course"), title: "已上课程")]
        v.selectedClosure = { [weak self] (type) in
            self?.guideViewSelectedItem(type)
        }
        return v
    }()
    
    fileprivate lazy var profileImageView: UIImageView = {
        let i = UIImageView()
        i.layer.cornerRadius = 35
        i.layer.masksToBounds = true
        i.image = UIImage(named: "profile")
        i.layer.borderColor = UIColor.white.cgColor
        i.layer.borderWidth = 5
        i.backgroundColor = UIColor.main
        return i
    }()
    
    fileprivate lazy var nameLabel: PaddingLabel = {
        let l = PaddingLabel()
        l.font = UIFont.mediumCoreSansDFont(ofSize: 24)
        l.textColor = UIColor("#4F4F4F")
        l.layer.cornerRadius = 24
        l.layer.masksToBounds = true
        l.backgroundColor = .white
        l.textAlignment = .center
        l.padding = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
        return l
    }()

    fileprivate lazy var nameBackgroundView: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        return v
    }()
    
    fileprivate lazy var menuButton: AnimationButton = {
        let b = AnimationButton()
        b.image = UIImage(named: "settings")?.repaintImage(tintColor: UIColor("#745B1A"))
        b.clickClosure = {[weak self] (button) in
            self?.menuButtonClick(button)
        }
        return b
    }()
    
    fileprivate lazy var menuView: MenuView = {
        let v = MenuView()
        v.checkButton.addTarget(self, action: #selector(checkClick(_:)), for: .touchUpInside)
        v.loginOutButton.addTarget(self, action: #selector(logoutClick(_:)), for: .touchUpInside)
        v.aboutButton.addTarget(self, action: #selector(aboutClick(_:)), for: .touchUpInside)
        return v
    }()
    
    fileprivate lazy var refreshButton: UIButton = {
        let b = UIButton()
        b.setBackgroundImage(UIImage(named: "refresh")?.repaintImage(tintColor: .white), for: .normal)
        //b.setImage(UIImage(named: "refresh")?.repaintImage(tintColor: .white), for: .normal)
        b.addTarget(self, action: #selector(refreshClick), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var tableView: UITableView = {
        let t = UITableView(frame: .zero, style: .grouped)
        t.dataSource = self
        t.delegate = self
        t.backgroundColor = .clear
        t.separatorStyle = .none
        t.indicatorStyle = .white
        t.scrollIndicatorInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        t.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 120))
        t.register(HomeTableViewCell.self, forCellReuseIdentifier: NSStringFromClass(HomeTableViewCell.self))
        return t
    }()
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = sections[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(HomeTableViewCell.self), for: indexPath) as? HomeTableViewCell else { return UITableViewCell() }

        cell.backgroundColor = .clear
        cell.updateUI(lesson: section.lesson_section, type: currentType)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let collectionCelles = sections[indexPath.row].lesson_section.count
        let collectionItemHeight = (kClassCollectionItemHeight + kClassCollectionItemLineSpacing) *  CGFloat(collectionCelles % 2 + collectionCelles / 2)
        return collectionItemHeight + kClassCollectionMargin * 2
    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let conentOffsetY = scrollView.contentOffset.y
//        let bottomOffset = scrollView.contentSize.height - conentOffsetY
//        let height = scrollView.frame.size.height
//        if bottomOffset <= height,  sections.count > 0,
//            let data = completeData,
//            data.perPage * data.currentPage < data.total, currentType == .recording {
//            if !loading {
//                loadRecordingData(page: data.currentPage + 1, perPage: data.perPage)
//                loading = true
//            }
//        }
//    }
}
