//
//  ResetPasswordView.swift
//  4iPad
//
//  Created by Cathy on 2019/9/26.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class ResetPasswordView: UIView {
    var backLoginClosure: (() -> Void)?

    private var codeTimer: Timer?
    private var codeTimeInterval = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 10.0
        layer.cornerRadius = 10.0
        
        layoutUI()
        updatePhoneTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func updatePhoneTextField() {
        phoneTextField.text = AccountManager.shared.phoneNumber
        showErrorTip()
        handleBtnEnabled()
    }
    
    @objc func resetPassword() {
        endEditing(true)
        guard let phone = phoneTextField.text else { return }
        guard let psd = passwordTextField.text else { return }
        guard let pin = codeTextField.text else { return }
        guard let oncePsd = oncePasswordTextField.text else { return }

        if psd != oncePsd {
            let e = ABCError.token(code: .resetPasswordMissing)
            showErrorTip(e)
            passwordTextField.text = ""
            oncePasswordTextField.text = ""
            return
        }
        
        let params: [String : Any] = [
            "mobile": phone,
            "password": psd,
            "pin": pin,
        ]
        
        NetWorkRequest(.resetPassword(params)) {[weak self] (res) in
            self?.showErrorTip(res.error)
            switch res {
            case .success:
                self?.backLogin()
            default:
                break
            }
        }
    }

    fileprivate func clearData() {
        phoneTextField.text = ""
        codeTextField.text = ""
        codeButton.isEnabled = false
        passwordTextField.text = ""
        oncePasswordTextField.text = ""
        resetButton.isEnabled = false
        tipLabel.text = ""
    }
    
    fileprivate func showErrorTip(_ error: ABCError? = nil) {
        tipLabel.isHidden = error == nil
        let message = error.flatMap{$0.code == .getAccessTokenFailed ? "输入的验证码有误" : $0.message}
        tipLabel.text = message
    }
    
    @objc func backLogin() {
        backLoginClosure?()
        clearData()
        endEditing(true)
    }

    @objc func textFieldDidChanged(_ textField: UITextField) {
        if textField == phoneTextField {
            AccountManager.shared.phoneNumber = textField.text ?? ""
        }
        handleBtnEnabled()
    }
    
    public func startCodeTimer() {
        codeTimeInterval = 60
        let t = Timer(timeInterval: 1,
                      target: self,
                      selector: #selector(codeTimeRefresh),
                      userInfo: nil,
                      repeats: true)
        RunLoop.current.add(t, forMode: .common)
        t.fire()
        codeTimer = t
        codeButton.isEnabled = false
    }
      
    public func stopCodeTimer() {
        if let t = codeTimer {
            t.invalidate()
            codeTimer = nil
            codeTimeInterval = 0
            let text = phoneTextField.text ?? ""
            codeButton.isEnabled = text.count == 11
            codeButton.setTitle("获取验证码", for: .disabled)
        }
    }
      
      @objc func codeTimeRefresh() {
          codeTimeInterval = codeTimeInterval - 1
          if codeTimeInterval <= 0 {
              stopCodeTimer()
          } else {
              codeButton.setTitle("\(codeTimeInterval) s", for: .disabled)
          }
      }
      
    
    @objc func sendPin(_ button: UIButton) {
        button.isEnabled = false
        
        guard let phone = phoneTextField.text else { return }
        let params: [String : Any] = ["type": 3,
                                      "mobile": phone]
        
        NetWorkRequest(.sendPin(params)) {[weak self] (res) in
            self?.showErrorTip(res.error)
            
            switch res {
            case .success:
                self?.startCodeTimer()

            default:
                break
            }
        }
    }
    
    fileprivate func handleBtnEnabled() {
        let isPhoneRegular = PredicateValidate.phone(phoneTextField.text).vaild
        codeButton.isEnabled = isPhoneRegular && (codeTimer?.isValid ?? false) == false
        resetButton.isEnabled = isPhoneRegular &&
            (codeTextField.text ?? "").count > 0 &&
            (passwordTextField.text ?? "").count > 0 &&
            (oncePasswordTextField.text ?? "").count > 0
        
        resetButton.backgroundColor = resetButton.isEnabled ? UIColor.main : UIColor("#E3E3E3")
    }
    
    fileprivate func layoutUI() {
        addSubview(titleLabel)
        addSubview(lineView)
        addSubview(tipLabel)
        addSubview(phoneTextField)
        addSubview(codeButton)
        addSubview(codeTextField)
        addSubview(passwordTextField)
        addSubview(oncePasswordTextField)
        addSubview(resetButton)
        addSubview(lineBottonView)
        addSubview(backButton)
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(self)
            make.height.equalTo(60)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(-1)
            make.height.equalTo(1)
            make.left.right.equalTo(self)
        }
        
        tipLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(phoneTextField.snp.top).offset(-4)
            make.left.equalTo(phoneTextField)
        }
        
        phoneTextField.snp.makeConstraints { (make) in
            make.top.equalTo(lineView.snp.bottom).offset(30)
            make.left.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(38.0)
        }
        
        
        codeButton.snp.makeConstraints { (make) in
            make.right.equalTo(self).offset(-30)
            make.top.equalTo(phoneTextField.snp.bottom).offset(15)
            make.width.equalTo(100)
            make.height.equalTo(38.0)
        }
        
        codeTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneTextField.snp.bottom).offset(15)
            make.left.height.equalTo(phoneTextField)
            make.right.equalTo(codeButton.snp.left).offset(-5)
        }
        
        //password
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(codeTextField.snp.bottom).offset(15)
            make.left.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(38.0)
        }
        
        oncePasswordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp.bottom).offset(15)
            make.left.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(38.0)
        }
        
        // 确认修改
        resetButton.snp.makeConstraints { (make) in
            make.top.equalTo(oncePasswordTextField.snp.bottom).offset(15)
            make.left.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(38.0)
        }
        
        
        backButton.snp.makeConstraints { (make) in
            make.top.equalTo(resetButton.snp.bottom).offset(24)
            make.centerX.equalTo(self)
            make.height.equalTo(20)
            make.width.equalTo(130)
        }
        
        lineBottonView.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(1)
            make.centerY.equalTo(backButton)
        }
        
    }
    
    // MARK: - SubViews
    fileprivate lazy var tipLabel: UILabel = {
        let l = UILabel()
        l.textColor = UIColor("#F97462")
        l.font = UIFont.systemFont(ofSize: 14)
        l.text = "手机号不存在!"
        l.isHidden = true
        return l
    }()
    
    fileprivate lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.textColor = UIColor.main
        l.text = "重置密码"
        l.font = UIFont.systemFont(ofSize: 14)
        l.textAlignment = .center
        return l
    }()
    
    fileprivate lazy var lineView: UIView = {
        let l = UIView()
        l.backgroundColor = UIColor("#E3E3E3")
        return l
    }()
    
    fileprivate lazy var phoneTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "请输入手机号"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.clearButtonMode = .whileEditing
        t.autocorrectionType = .no
        t.autocapitalizationType = .none
        t.returnKeyType = .done
        t.keyboardType = .numberPad
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var codeTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "请输入验证码"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var codeButton: UIButton = {
        let b = UIButton(type: .custom)
        b.setTitle("获取验证码", for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        
        b.setBackgroundImage(UIImage(color: UIColor.main), for: .normal)
        b.setBackgroundImage(UIImage(color: UIColor("#E3E3E3")), for: .disabled)
        b.isEnabled = false
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        b.addTarget(self, action: #selector(sendPin(_:)), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var passwordTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "请输入新密码"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.clearButtonMode = .whileEditing
        t.isSecureTextEntry = true
        t.autocorrectionType = .no
        t.autocapitalizationType = .none
        t.returnKeyType = .done
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var oncePasswordTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "再输入一次"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.clearButtonMode = .whileEditing
        t.isSecureTextEntry = true
        t.autocorrectionType = .no
        t.autocapitalizationType = .none
        t.returnKeyType = .done
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var resetButton: UIButton = {
        let b = UIButton(type: .custom)
        b.setTitle("确认修改", for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        b.isEnabled = false
        b.backgroundColor = b.isEnabled ? UIColor.main : UIColor("#E3E3E3")
        b.addTarget(self, action: #selector(resetPassword), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var backButton: UIButton = {
        let b = UIButton()
        b.setTitle("返回登录", for: .normal)
        b.setTitleColor(UIColor.main, for: .normal)
        b.backgroundColor = .white
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.addTarget(self, action: #selector(backLogin), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var lineBottonView: UIView = {
        let l = UIView()
        l.backgroundColor = UIColor("#E3E3E3")
        return l
    }()
}

extension ResetPasswordView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
