//
//  ABCBackgroundModel.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import HandyJSON

enum PageType: Int, HandyJSONEnum {
    case login  =  0
    case home = 1
    case room = 2
    
    var imageName: String {
        switch self {
        case .login:
            return "loginBackgroundImage.png"
        case .home:
            return "homeBackgroundImage.png"
        case .room:
            return "roomBackgroundImage.png"
        }
    }
}

class BackgroudImage: HandyJSON {

    required init() {
    }
    
    var id = 0
    var type: PageType = .login
    var updated_at: Date?
    var order = 0
    var url_2x: String?
    
    func mapping(mapper: HelpingMapper) {

        mapper <<< updated_at <-- DateFormatterTransform(dateFormatter: defalutDateFormatter)
        
    }
}

class ImageData: HandyJSON {
    required init() {}
    var background:[BackgroudImage]?
}

class AppSetUp: HandyJSON {
    required init() {}
    class Version: HandyJSON {
        required init() {}
        var version_id: Int = 0
        var version_number = ""
        var download_url = ""
        var source_url = ""
        var content = ""
        var is_must: Bool = false
        var created_at = ""
        var title = ""

        var isShowAlert: Bool {
            let serverVersion = version_number.split(separator: ".").joined()
            let localVersion = Bundle.main.shortVersion.split(separator: ".").joined()
            if let serverNumber = Int(serverVersion), let localNumber = Int(localVersion), serverNumber > localNumber {
                return true
            }
            return false
        }
    }

    var version: Version?
    var background:[BackgroudImage]?
}
