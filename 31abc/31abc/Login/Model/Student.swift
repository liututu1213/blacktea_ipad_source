//
//  Student.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/29.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import HandyJSON

class Student: HandyJSON {
    required init() {}
    
    var id: String = ""
    var name: String = ""
    var name_cn: String = ""
    var header: String?
    var user_id: UInt = 0
}

class LoginModel: BaseModel {
    var access_token: String?
    var expires_in: Int = 0
    var token_type: String?
    var refresh_token: String?
    
    var scope: String = ""
}

