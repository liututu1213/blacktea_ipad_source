//
//  LoginPhoneContentView.swift
//  4iPad
//
//  Created by Cathy on 2019/9/23.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class LoginPhoneContentView: UIView {
    var completeClosure: (() -> Void)?
    
    private var codeTimer: Timer?
    private var codeTimeInterval = 0
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layoutUI()
        updatePhoneTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public
    public func updatePhoneTextField() {
        phoneTextField.text = AccountManager.shared.phoneNumber
        showErrorTip()
        handleBtnEnabled()
    }
    
    public func startCodeTimer() {
        codeTimeInterval = 60
        let t = Timer(timeInterval: 1,
                      target: self,
                      selector: #selector(codeTimeRefresh),
                      userInfo: nil,
                      repeats: true)
        RunLoop.current.add(t, forMode: .common)
        t.fire()
        codeTimer = t
        codeButton.isEnabled = false
    }
    
    public func stopCodeTimer() {
        if let t = codeTimer {
            t.invalidate()
            codeTimer = nil
            codeTimeInterval = 0
            let text = phoneTextField.text ?? ""
            codeButton.isEnabled = text.count == 11
        }
    }
    
    @objc func codeTimeRefresh() {
        codeTimeInterval = codeTimeInterval - 1
        if codeTimeInterval <= 0 {
            stopCodeTimer()
        } else {
            codeButton.setTitle("\(codeTimeInterval) s", for: .disabled)
        }
    }
    
    @objc func sendPin(_ button: UIButton) {
        button.isEnabled = false
        
        guard let phone = phoneTextField.text else { return }
        let params: [String : Any] = ["type": 2,
                                      "mobile": phone]
        
        NetWorkRequest(.sendPin(params)) {[weak self] (res) in
            self?.showErrorTip(res.error)
            switch res {
            case .success:
                self?.startCodeTimer()
                
            default:
                break
            }
        }
    }
    
    fileprivate func showErrorTip(_ error: ABCError? = nil) {
        tipLabel.isHidden = error == nil

        let message = error.flatMap{$0.code == .getAccessTokenFailed ? "输入的验证码有误" : $0.message}
        tipLabel.text = message
    }
    
    @objc func login() {
        endEditing(true)
        let phone = AccountManager.shared.phoneNumber
        guard let psd = phoneTextField.text else { return }
        guard let pin = codeTextField.text else { return }
        let params: [String : Any] = ["login_type": 2,
                                      "mobile": phone,
                                      "password": psd,
                                      "pin": pin,
                                      "code": ""]
        
        NetWorkRequest(.login(params)) {[weak self] (res) in
            self?.showErrorTip(res.error)
            switch res {
            case .success(let json):
                let dic = json as? [String :Any]
                if let loginModel = LoginModel.deserialize(from: dic) {
                    AccountManager.shared.loginModel = loginModel
                    AccountManager.shared.savePhoneNumber()
                    self?.completeClosure?()
                }
                
            default:
                break
            }
        }
    }
    
    @objc func textFieldDidChanged(_ textField: UITextField) {
        if textField == phoneTextField {
            AccountManager.shared.phoneNumber = textField.text ?? ""
        }
        
        handleBtnEnabled()
    }
    
    fileprivate func handleBtnEnabled() {
        let isPhoneRegular = PredicateValidate.phone(phoneTextField.text).vaild
        let isCodeRegular = (codeTextField.text ?? "").count > 0
        
        codeButton.isEnabled = isPhoneRegular && (codeTimer?.isValid ?? false) == false
        loginButton.isEnabled = isPhoneRegular && isCodeRegular
        loginButton.backgroundColor = loginButton.isEnabled ? UIColor.main : UIColor("#E3E3E3")
    }
    
    fileprivate func layoutUI() {
        addSubview(tipLabel)
        addSubview(phoneTextField)
        addSubview(codeTextField)
        addSubview(codeButton)
        addSubview(loginButton)
        
        tipLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(phoneTextField.snp.top).offset(-4)
            make.left.equalTo(phoneTextField)
        }
        
        phoneTextField.snp.makeConstraints { (make) in
            make.left.top.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(38.0)
        }
        
        codeButton.snp.makeConstraints { (make) in
            make.right.equalTo(self).offset(-30)
            make.top.equalTo(phoneTextField.snp.bottom).offset(15)
            make.width.equalTo(100)
            make.height.equalTo(38.0)
        }
        
        codeTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneTextField.snp.bottom).offset(15)
            make.left.height.equalTo(phoneTextField)
            make.right.equalTo(codeButton.snp.left).offset(-5)
        }
        
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(codeTextField.snp.bottom).offset(15)
            make.left.right.height.equalTo(phoneTextField)
            make.bottom.equalTo(self)
        }
        
    }
    
    // MARK: - SubViews
    fileprivate lazy var tipLabel: UILabel = {
        let l = UILabel()
        l.textColor = UIColor("#F97462")
        l.font = UIFont.systemFont(ofSize: 14)
        l.text = "手机号不存在!"
        l.isHidden = true
        return l
    }()
    
    fileprivate lazy var phoneTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "请输入手机号"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.keyboardType = .numberPad
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var codeTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "请输入验证码"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var codeButton: UIButton = {
        let b = UIButton(type: .custom)
        b.setTitle("获取验证码", for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.setBackgroundImage(UIImage(color: UIColor.main), for: .normal)
        b.setBackgroundImage(UIImage(color: UIColor("#E3E3E3")), for: .disabled)
        b.isEnabled = false
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        b.setTitle("获取验证码", for: .disabled)
        b.addTarget(self, action: #selector(sendPin(_:)), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var loginButton: UIButton = {
        let b = UIButton(type: .custom)
        b.setTitle("登录", for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.backgroundColor = UIColor.main
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        b.isEnabled = false
        b.backgroundColor = b.isEnabled ? UIColor.main : UIColor("#E3E3E3")
        b.addTarget(self, action: #selector(login), for: .touchUpInside)
        return b
    }()
}

extension LoginPhoneContentView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == phoneTextField {
            codeTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
