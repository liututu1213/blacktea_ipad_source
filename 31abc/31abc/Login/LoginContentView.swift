//
//  LoginContentView.swift
//  4iPad
//
//  Created by Cathy on 2019/9/26.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class LoginContentView: UIView {
    var completeClosure: (() -> Void)?
    var forgetPasswordClosure: (() -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 10.0
        layer.cornerRadius = 10.0

        layoutUI()
        selectedPasswordButton(true)
    }

    public func updatePhoneTextField() {
        passwordContentView.updatePhoneTextField()
        iphoneCodeContentView.updatePhoneTextField()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func loginComplete() {
        let a = UIApplication.shared.delegate as! AppDelegate
        a.switchRooterViewController(HomeViewController())
    }

    @objc fileprivate func forgetPassword() {
        forgetPasswordClosure?()
    }

    // MARK: - Action
    @objc fileprivate func passwordButtonClick(_ button: UIButton) {
        selectedPasswordButton(true)
    }

    @objc fileprivate func iphoneCodeButtonClick(_ button: UIButton) {
        selectedPasswordButton(false)
    }

    @objc fileprivate func checkClick() {
        checkButton.isSelected = !checkButton.isSelected
        UserDefaults.standard[.rememberPassword] = checkButton.isSelected
    }

    @objc fileprivate func registerClick() {

        guard let url = URL(string: GlobalConfig.base.register) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    fileprivate func selectedPasswordButton(_ selected: Bool) {
        forgetButton.isHidden = !selected

        passwordButton.isSelected = selected
        iphoneCodeButton.isSelected = !passwordButton.isSelected
        passwordContentView.isHidden = !selected
        iphoneCodeContentView.isHidden = !passwordContentView.isHidden

        iphoneCodeContentView.updatePhoneTextField()
        passwordContentView.updatePhoneTextField()
    }
    
    fileprivate func layoutUI() {
        addSubview(passwordButton)
        addSubview(iphoneCodeButton)
        addSubview(lineView)
        addSubview(passwordContentView)
        addSubview(iphoneCodeContentView)
        addSubview(checkButton)
        addSubview(rememberLabel)
        addSubview(forgetButton)
//        addSubview(lineBottonView)
//        addSubview(registerButton)
        
        passwordButton.snp.makeConstraints { (make) in
            make.left.top.equalTo(self)
            make.width.equalToSuperview().dividedBy(2)
            make.height.equalTo(60)
        }
        
        iphoneCodeButton.snp.makeConstraints { (make) in
            make.right.top.equalTo(self)
            make.width.equalToSuperview().dividedBy(2)
            make.height.equalTo(60)
        }
        
        lineView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(iphoneCodeButton.snp.bottom)
            make.height.equalTo(1)
        }
        
        //输入框
        passwordContentView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(lineView.snp.bottom)
        }
        
        iphoneCodeContentView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self)
            make.top.equalTo(lineView.snp.bottom)
        }
        
        //记住密码 忘记密码
        checkButton.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(30)
            make.width.height.equalTo(12)
            make.top.equalTo(iphoneCodeContentView.snp.bottom).offset(20)
        }
        
        rememberLabel.snp.makeConstraints { (make) in
            make.left.equalTo(checkButton.snp.right).offset(5)
            make.centerY.equalTo(checkButton)
            make.height.equalTo(20)
        }
        
        forgetButton.snp.makeConstraints { (make) in
            make.right.equalTo(self).offset(-30)
            make.centerY.equalTo(checkButton)
            make.height.equalTo(20)
        }
        
        // 注册
//        registerButton.snp.makeConstraints { (make) in
//            make.top.equalTo(rememberLabel.snp.bottom).offset(24)
//            make.centerX.equalTo(self)
//            make.height.equalTo(20)
//            make.width.equalTo(130)
//        }
//
//        lineBottonView.snp.makeConstraints { (make) in
//            make.left.equalTo(self).offset(30)
//            make.right.equalTo(self).offset(-30)
//            make.height.equalTo(1)
//            make.centerY.equalTo(registerButton)
//        }
    }
    
    // MARK: - Subviews
    fileprivate lazy var passwordButton: UIButton = {
        let b = UIButton()
        b.setTitleColor(UIColor.main, for: .selected)
        b.setTitleColor(UIColor("#828282"), for: .normal)
        b.setTitle("密码登录", for: .normal)
        b.addTarget(self, action: #selector(passwordButtonClick(_:)), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var iphoneCodeButton: UIButton = {
        let b = UIButton()
        b.setTitleColor(UIColor.main, for: .selected)
        b.setTitleColor(UIColor("#828282"), for: .normal)
        b.addTarget(self, action: #selector(iphoneCodeButtonClick(_:)), for: .touchUpInside)
        b.setTitle("短信登录", for: .normal)
        return b
    }()
    
    fileprivate lazy var lineView: UIView = {
        let l = UIView()
        l.backgroundColor = UIColor("#E3E3E3")
        return l
    }()
    
    fileprivate lazy var passwordContentView: LoginUserNameContentView = {
        let view = LoginUserNameContentView()
        view.completeClosure = { [weak self] in
            self?.loginComplete()
        }
        return view
    }()
    
    fileprivate lazy var iphoneCodeContentView: LoginPhoneContentView = {
        let view = LoginPhoneContentView()
        view.completeClosure = { [weak self] in
            self?.loginComplete()
        }
        return view
    }()
    
    fileprivate lazy var checkButton: UIButton = {
        let b = UIButton()
        b.setImage(UIImage(named: "square_checked")?.repaintImage(tintColor: UIColor("#4F4F4F")), for: .selected)
        b.setImage(UIImage(named: "square_check")?.repaintImage(tintColor: UIColor("#4F4F4F")), for: .normal)
        b.isSelected = UserDefaults.standard[.rememberPassword]
        b.addTarget(self, action: #selector(checkClick), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var rememberLabel: UILabel = {
        let l = UILabel()
        l.text = "记住我"
        l.textColor = UIColor("#4F4F4F")
        l.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(checkClick)))
        l.isUserInteractionEnabled = true
        l.font = UIFont.systemFont(ofSize: 14)
        return l
    }()
    
    fileprivate lazy var forgetButton: UIButton = {
        let b = UIButton()
        b.setTitle("忘记密码", for: .normal)
        b.setTitleColor(UIColor("#4F4F4F"), for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.addTarget(self, action: #selector(forgetPassword), for: .touchUpInside)
        return b
    }()
    
//    fileprivate lazy var registerButton: UIButton = {
//        let b = UIButton()
//        b.setTitle("注册领取免费试听课", for: .normal)
//        b.setTitleColor(UIColor.main, for: .normal)
//        b.backgroundColor = .white
//        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
//        b.addTarget(self, action: #selector(registerClick), for: .touchUpInside)
//        return b
//    }()
//
//    fileprivate lazy var lineBottonView: UIView = {
//        let l = UIView()
//        l.backgroundColor = UIColor("#E3E3E3")
//        return l
//    }()
}
