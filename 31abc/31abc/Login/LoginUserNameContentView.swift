//
//  LoginUserNameContentView.swift
//  4iPad
//
//  Created by Cathy on 2019/9/23.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import SnapKit
import Moya

class LoginUserNameContentView: UIView {
    var completeClosure: (() -> Void)?
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        layoutUI()
        updatePhoneTextField()
    }
    
    public func updatePhoneTextField() {
        phoneTextField.text = AccountManager.shared.phoneNumber
        showErrorTip()
        handleBtnEnabled()
    }
    
    fileprivate func layoutUI() {
        addSubview(tipLabel)
        addSubview(phoneTextField)
        addSubview(passwordTextField)
        addSubview(loginButton)
        
        tipLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(phoneTextField.snp.top).offset(-4)
            make.left.equalTo(phoneTextField)
        }
        
        phoneTextField.snp.makeConstraints { (make) in
            make.left.top.equalTo(self).offset(30)
            make.right.equalTo(self).offset(-30)
            make.height.equalTo(38.0)
        }
        
        passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(phoneTextField.snp.bottom).offset(15)
            make.left.right.height.equalTo(phoneTextField)
        }
        
        loginButton.snp.makeConstraints { (make) in
            make.top.equalTo(passwordTextField.snp.bottom).offset(15)
            make.left.right.height.equalTo(passwordTextField)
            make.bottom.equalTo(self)
        }
    }
    
    // MARK: - Actions
    @objc func textFieldDidChanged(_ textField: UITextField) {
        handleBtnEnabled()
        
        if textField == phoneTextField {
            AccountManager.shared.phoneNumber = textField.text ?? ""
        }
    }
    
    func handleBtnEnabled() {
        let isUserNameRegular = PredicateValidate.phone(phoneTextField.text).vaild
        let isPassworddRegular = (passwordTextField.text ?? "").count > 0
        loginButton.isEnabled = isUserNameRegular && isPassworddRegular
        loginButton.backgroundColor = loginButton.isEnabled ? UIColor.main : UIColor("#E3E3E3")
    }
    
    @objc func login() {
        endEditing(true)
        let phone = AccountManager.shared.phoneNumber
        guard let psd = passwordTextField.text else { return }
        let params: [String : Any] = ["login_type": 1,
                                      "mobile": phone,
                                      "password": psd,
                                      "pin": "",
                                      "code": ""]
        
        NetWorkRequest(.login(params)) {[weak self] (res) in
            self?.showErrorTip(res.error)
            switch res {
            case .success(let json):
                let dic = json as? [String :Any]
                if let loginModel = LoginModel.deserialize(from: dic) {
                    AccountManager.shared.loginModel = loginModel
                    AccountManager.shared.savePhoneNumber()
                    self?.completeClosure?()
                }
                
            default:
                break
            }
        }
    }
    
    fileprivate func showErrorTip(_ error: ABCError? = nil) {
        tipLabel.isHidden = error == nil
        let message = error.flatMap{$0.code == .getAccessTokenFailed ? "用户名或密码有误" : $0.message}
        tipLabel.text = message
    }
    
    // MARK: - SubViews
    fileprivate lazy var tipLabel: UILabel = {
        let l = UILabel()
        l.textColor = UIColor("#F97462")
        l.font = UIFont.systemFont(ofSize: 14)
        l.text = "手机号不存在!"
        l.isHidden = true
        return l
    }()
    
    fileprivate lazy var phoneTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "请输入手机号"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.clearButtonMode = .whileEditing
        t.autocorrectionType = .no
        t.autocapitalizationType = .none
        t.returnKeyType = .done
        t.keyboardType = .numberPad
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var passwordTextField: UITextField = {
        let t = UITextField()
        t.placeholder = "请输入密码"
        t.font = UIFont.systemFont(ofSize: 14)
        t.textColor = UIColor("#4F4F4F")
        t.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 1))
        t.leftViewMode = .always
        t.clearButtonMode = .whileEditing
        t.isSecureTextEntry = true
        t.autocorrectionType = .no
        t.autocapitalizationType = .none
        t.returnKeyType = .done
        t.layer.cornerRadius = 5.0
        t.layer.masksToBounds = true
        t.layer.borderColor = UIColor("#E3E3E3").cgColor
        t.layer.borderWidth = 1.0
        t.delegate = self
        t.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
        return t
    }()
    
    fileprivate lazy var loginButton: UIButton = {
        let b = UIButton(type: .custom)
        b.setTitle("登录", for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        b.isEnabled = false
        b.backgroundColor = b.isEnabled ? UIColor.main : UIColor("#E3E3E3")
        b.addTarget(self, action: #selector(login), for: .touchUpInside)
        return b
    }()
}

extension LoginUserNameContentView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done, textField.text == "\n" {
            textField.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.returnKeyType == .done, textField.text == "\n" {
            textField.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
}
