//
//  LoginViewController.swift
//  4iPad
//
//  Created by Cathy on 2019/9/23.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import Kingfisher
import enum Result.Result

let kLoginViewHeight: CGFloat = 337.0
let kResetPasswordViewHeight: CGFloat = 405.0
let kLoginViewWidth: CGFloat = 300.0

class LoginViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor("#48D499")
        
        layoutUI()
        addNotifications()
    }
    
    fileprivate func layoutUI() {
        view.addSubview(loginContentView)
        loginContentView.snp.makeConstraints { (make) in
            make.width.equalTo(kLoginViewWidth)
            make.height.equalTo(kLoginViewHeight)
            make.center.equalTo(view)
        }
        
        view.addSubview(resetView)
        resetView.snp.makeConstraints { (make) in
            make.width.equalTo(kLoginViewWidth)
            make.height.equalTo(kResetPasswordViewHeight)
            make.center.equalTo(view)
        }

        view.addSubview(testView)
        testView.snp.makeConstraints { (make) in
            make.left.bottom.equalTo(view)
            make.height.equalTo(100)
            make.width.equalTo(100)
        }
        
        backgroundImageView.image = AccountManager.shared.getBackgroundImage(PageType.login.imageName)
        AccountManager.shared.downloadImageCompletion = {[weak self] (image, type) in
            if type == .login {
                self?.backgroundImageView.image = image
            }
        }
    }

    fileprivate func showLoginContentView(_ isShow: Bool) {
        loginContentView.isHidden = !isShow
        resetView.isHidden = !loginContentView.isHidden
        loginContentView.updatePhoneTextField()
        resetView.updatePhoneTextField()

        view.endEditing(true)
    }
    
    fileprivate func addNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleKeyboardWillShowHideNotification(_:)),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleKeyboardWillShowHideNotification(_:)),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc fileprivate func handleKeyboardWillShowHideNotification(_ notification: NSNotification) {
        guard ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.size.height) != nil else { return }
        if notification.name == UIResponder.keyboardWillShowNotification {
            let height = 0.7 * (kScreenHeight - kResetPasswordViewHeight) / 2
            loginContentView.snp.updateConstraints { (make) in
                make.centerY.equalTo(view).offset(-height)
            }
            
            resetView.snp.updateConstraints { (make) in
                make.centerY.equalTo(view).offset(-height)
            }
        } else {
            loginContentView.snp.updateConstraints { (make) in
                make.centerY.equalTo(view)
            }
            resetView.snp.updateConstraints { (make) in
                make.centerY.equalTo(view)
            }
        }
    }

    // MARK: - SubViews
    fileprivate lazy var loginContentView: LoginContentView = {
        let view = LoginContentView()
        view.forgetPasswordClosure = {[weak self] in
            self?.showLoginContentView(false)
        }
        return view
    }()
    
    fileprivate lazy var resetView: ResetPasswordView = {
        let view = ResetPasswordView()
        view.backLoginClosure = {[weak self] in
            self?.showLoginContentView(true)
        }
        view.isHidden = true
        return view
    }()

    fileprivate lazy var testView: UIView = {
        let v = UIView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(testTap))
        tap.numberOfTapsRequired = 5
        tap.numberOfTouchesRequired = 1
        v.addGestureRecognizer(tap)
        return v
    }()

    @objc func testTap() {
        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let options = ContextMenu.Options(containerStyle: ContextMenu.ContainerStyle(overlayColor: color), menuStyle: .minimal)
        let vc = DebugViewController()
        ContextMenu.shared.show(sourceViewController: self,
                                viewController: vc ,
                                options: options)
    }
}
