//
//  ABCBaseModel.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/29.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import HandyJSON

class HandyModel: HandyJSON {
    required init() {}
}

class BaseModel: HandyModel {
    var code: Int = 0
    var msg: String = ""
}
