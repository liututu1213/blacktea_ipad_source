//
//  ABCAPI.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/23.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import Moya
import Alamofire

enum ABCAPI{
    case appSetup(Int)
    case login([String: Any])
    case logout
    case backgroundImage
    case sendPin([String: Any])
    case resetPassword([String: Any])
    case refreshToken(String)
    
    case home
    case history(Int, Int)
    case lessonSection([String: Any]?)
    case checkDevice([String: Any])
    
    case upgrade
    
    /// 主页相关接口
    case classRoom(Int)
    case joinRTC([String: Any])
    case joinRTM([String: Any])
    case startRecording(Int)
    case download(url: URL, id: String)
    case recording_agroa(Int)
}

extension ABCAPI: ABCTargetType {
    var api: (baseURL: URL, path: String, method: Moya.Method, task: Task) {
        switch self {
        case .appSetup(let device_type):
            return api(path: "init/index/\(device_type)",
                method: .get,
                task: .requestPlain)

        case .login(let params):
            return api(path: "account/login",
                    method: .post,
                    task: .requestParameters(parameters: params, encoding: JSONEncoding.default))
            
        case .logout:
            return api(path: "account/login_out",
                    method: .post,
                    task: .requestPlain)
            
        case .backgroundImage:
            return api(path: "init/background",
                    method: .get,
                    task: .requestPlain)
            
        case .sendPin(let params):
            return api(path: "account/sendPin",
                    method: .post,
                    task: .requestParameters(parameters: params, encoding: JSONEncoding.default))
            
        case .resetPassword(let params):
            return api(path: "account/password",
                    method: .patch,
                    task: .requestParameters(parameters: params, encoding: JSONEncoding.default))
            
            
        case .refreshToken(let str):
            return api(path: "account/refresh_token",
                    method: .post,
                    task: .requestParameters(parameters: ["refresh_token": str], encoding: JSONEncoding.default))
            
        /// 主页相关接口
        case .home:
            return api(path: "home/3",
                    method: .get,
                    task: .requestPlain)

        case .history(let page, let perPage):
            return api(path: "lessonSection/history?page=\(page)&perPage=\(perPage)&device_type=3",
                    method: .get,
                    task: .requestPlain)
            
        case .lessonSection(let params):
            let task: Task = params == nil ? .requestPlain : .requestParameters(parameters: params!, encoding: JSONEncoding.default)
            
            return api(path: "lessonSection/index",
                    method: .get,
                    task: task)
            
        case .checkDevice(let params):
            return api(path: "system/checkDevice",
                    method: .post,
                    task: .requestParameters(parameters: params, encoding: JSONEncoding.default))
            
        case .upgrade:
            return api(path: "",
                    method: .post,
                    task: .requestPlain)
            
        /// class room
        case .classRoom(let id):
            return api(path: "classRoom/index/" + "\(id )/3",
                method: .get,
                task: .requestData(Data()))
            
        case .joinRTC(let params):
            return api(path: "classRoom/joinRTC",
                    method: .post,
                    task: .requestParameters(parameters: params, encoding: JSONEncoding.default))
            
        case .joinRTM(let params):
            return api(path: "classRoom/joinRTM",
                    method: .post,
                    task: .requestParameters(parameters: params, encoding: JSONEncoding.default))
            
        case .startRecording(let id):
            return api(path: "classRoom/startRecording/" + "\(id )",
                method: .post,
                task: .requestPlain)
        case .download(let url, let id):
            return api(baseURL: url,
                       path: "",
                       method: .get,
                       task: .downloadDestination(downloadDestination1(id: id)))
        case .recording_agroa(let id):
            return api(path: "lessonSection/recording_agroa/" + "\(id)" + "/2",
                method: .get,
                task: .requestPlain)
        }
    }
}

extension ABCAPI {

    func localUrl(id: String) -> URL {
        let path = NSHomeDirectory().appending("/Documents/Media/").appending("\(id).mp4")
        return URL(fileURLWithPath: path)
    }

    func downloadDestination1(id: String) -> DownloadDestination {
        let path = NSHomeDirectory().appending("/Documents/Media/").appending("\(id).mp4")
        let url = URL(fileURLWithPath: path)
        return { _, _ in return (url, [.removePreviousFile, .createIntermediateDirectories]) }
    }

    func api(baseURL: URL = ABCMoya.baseURL, path: String, method: Moya.Method, task: Task) -> (baseURL: URL, path: String, method: Moya.Method, task: Task){
        return (baseURL, path, method, task)
    }
}
