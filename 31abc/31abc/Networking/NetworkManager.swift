//
//  ABCNetworkManager.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/24.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import Moya
import Alamofire
import HandyJSON
import enum Result.Result

public typealias ABCCompletion = (_ result: Result<Any, ABCError>) -> Void
public typealias ABCResponseHeaderCompletion = (_ allHeaderFields: [AnyHashable : Any]?)-> Void
public typealias CommonCompletion = ((Bool) -> Void)?
private let ABCUnknownErrorCode = 10000
private var requestTimeOut:Double = 30

fileprivate func manager() -> Manager {
    let serverTrustPolicies: [String: ServerTrustPolicy] = [
        ABCMoya.environment.server.domain: .disableEvaluation
    ]

    if UserDefaults.standard[.debug] {
        return Manager(configuration: URLSessionConfiguration.default,
                       serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies))
    }
    return Manager.default
}



fileprivate let Provider = MoyaProvider<ABCAPI>(endpointClosure: myEndpointClosure,
                                                requestClosure: requestClosure,
                                                manager: manager(),
                                                plugins: [networkPlugin],
                                                trackInflights: false)

@discardableResult
func NetWorkRequest(_ target: ABCAPI,
                    _ responseHeaderCompletion: ABCResponseHeaderCompletion? = nil,
                    completion: @escaping ABCCompletion) -> Cancellable {
    
    return  Provider.request(target, callbackQueue: .none, progress: .none) {(res) in
        switch res {
        case let .success(value):
            do {
                responseHeaderCompletion?(value.response?.allHeaderFields)

                let json = try value.mapJSON() as? [String: Any] ?? [:]
                print(json)
                let res = ABCResponseFilterJSON(json)
                
                /// 是否需要重新获取Token
                if let errorCode = res.error?.code, errorCode == .authenticationFailed {
                    refreshToken { (isSuccess) in
                        if isSuccess == true {
                            completion(.failure(.token(code: .refreshTokenSuccess)))
                        }
                    }
                } else {
                    completion(res)
                }
            } catch let jsonError {
                completion(.failure(.network(jsonError)))
            }
        case let .failure(error): completion(.failure(.network(error)))
        }
    }
}

@discardableResult
func NetWorkDownload(_ target: ABCAPI,
                     _ responseHeaderCompletion: ABCResponseHeaderCompletion? = nil,
                     completion: @escaping ABCCompletion) -> Cancellable {
    return  Provider.request(target, callbackQueue: .none, progress: .none) {(res) in
        switch res {
        case let .success(value):
            let data = value.data
            completion(.success(data))
            
        case let .failure(error): completion(.failure(.network(error)))
        }
    }
}

private let myEndpointClosure = { (target: ABCAPI) -> Endpoint in

    let url = target.baseURL.absoluteString + target.path
    var task = target.task
    
    var endpoint = Endpoint(
        url: url,
        sampleResponseClosure: { .networkResponse(200, target.sampleData) },
        method: target.method,
        task: task,
        httpHeaderFields: target.headers
    )
    
    let access_token = AccountManager.shared.loginModel?.access_token
    let token_type = AccountManager.shared.loginModel?.token_type
    if let token = access_token, token.count > 0 , let type = token_type, type.count > 0 {
       endpoint = endpoint.adding(newHTTPHeaderFields: ["Authorization": "\(type) \(token)"])
    }
    
    requestTimeOut = 30
    switch target {
    case .login:
        return endpoint
    case .sendPin:
        requestTimeOut = 5
        return endpoint
        
    default:
        return endpoint
    }
}

private let requestClosure = { (endpoint: Endpoint, done: MoyaProvider.RequestResultClosure) in
    do {
        var request = try endpoint.urlRequest()
        //设置请求时长
        request.timeoutInterval = requestTimeOut
        // 打印请求参数
        if let requestData = request.httpBody {
            print("\(request.url!)"+"\n"+"\(request.httpMethod ?? "")"+"发送参数"+"\(String(data: request.httpBody!, encoding: String.Encoding.utf8) ?? "")")
        }else{
            print("\(request.url!)"+"\(String(describing: request.httpMethod))")
        }
        done(.success(request))
    } catch {
        done(.failure(MoyaError.underlying(error, nil)))
    }
}

private let networkPlugin = NetworkActivityPlugin { (changeType, targetType) in
    switch(changeType){
    case .began:
        print("开始请求网络\(Date())")
        
    case .ended:
        print("结束 \(Date())")
    }
}

var isNetworkConnect: Bool {
    get{
        let network = NetworkReachabilityManager()
        return network?.isReachable ?? true //无返回就默认网络已连接
    }
}

public func ABCResponseFilterJSON(_ json: [String: Any]) -> Result<Any, ABCError> {
    guard let code = json["code"] as? Int else { return .failure(.service(code: ABCUnknownErrorCode, msg: "未知错误")) }

    let data = json["data"]
    if code == 0 {
        
        guard let _data = data else { return .success("") }
        return .success(_data)
    } else {
        let msg = json["msg"] as? String
        return .failure(.service(code: code, msg: msg ?? ""))
    }
}

func refreshToken(completion: CommonCompletion) {
    guard let refresh_token = AccountManager.shared.loginModel?.refresh_token else {
        let a = UIApplication.shared.delegate as! AppDelegate
        a.switchRooterViewController(LoginViewController())
        completion?(false)
        return  
    }
    
    NetWorkRequest(.refreshToken(refresh_token)) { (res) in
        switch res {
        case .success(let json):
            let dic = json as? [String :Any]
            if let loginModel = LoginModel.deserialize(from: dic) {
                AccountManager.shared.loginModel = loginModel
                completion?(true)
            }
            
        case .failure:
            let a = UIApplication.shared.delegate as! AppDelegate
            a.switchRooterViewController(LoginViewController())
            completion?(false)
        }
    }
}



public enum ABCError: Error {
    case network(Error)
    case service(code: Int, msg: String)
    case token(code: ABCCode)
    
    var code: ABCCode {
        switch self {
        case .network(let _e):
            let e = _e as NSError;
            return ABCCode(rawValue: e.code) ?? .unknownErrorCode
            
        case let .service(code: code, msg: _):
            return ABCCode(rawValue: code) ?? .unknownErrorCode
        case .token(let code):
            return code
        }
    }
    
    var message: String {
        switch self {
        case let .network(e): return e.localizedDescription
        case .service(code: _, msg: _):
            return code.msg
        case .token(let code):
            return code.msg
        }
    }
}

public enum ABCCode: Int {
    case success                   = 0
    case failed                    = 101
    case permissionDenied          = 102
    case paramtersMissing          = 103
    case authenticationFailed      = 104
    case getAccessTokenFailed      = 105
    case unknownMethod             = 106
    case mobileFormatError         = 201
    case mobileExist               = 202
    case mobileNotExist            = 203
    case pinError                  = 204
    case pinTimeOut                = 205
    case registerError             = 206
    case studentNotFound           = 207
    case mobileAlreadyReceiveTrial = 401
    case smsSendCountError         = 403
    
    /// 自定义Code
    case unknownErrorCode          = 10000
    case refreshTokenSuccess       = 10001
    case resetPasswordMissing      = 10002
    
    public var msg: String {
        switch self {
        case .success:                         return "成功"
        case .failed:                          return "错误"
        case .permissionDenied:                return "权限不足"
        case .paramtersMissing:                return "参数错误"
        case .authenticationFailed:            return "登录已失效，请重新登录"
        case .getAccessTokenFailed:            return "用户名或密码错误"
        case .unknownMethod:                   return "系统连接错误"
        case .mobileFormatError:               return "手机号格式输入错误"
        case .mobileExist:                     return "注册的手机号已存在"
        case .mobileNotExist:                  return "该手机号不存在"
        case .pinError:                        return "输入的验证码有误"
        case .pinTimeOut:                      return "验证码已超时，请重新发送"
        case .registerError:                   return "注册信息有误"
        case .studentNotFound:                 return "该学员不存在"
        case .mobileAlreadyReceiveTrial:       return "该手机号已注册"
        case .smsSendCountError:               return "短信发送次数超过限制"
        case .resetPasswordMissing:            return "两次输入密码不一致"
            
        default:
            return "其他"
        }
    }
}
