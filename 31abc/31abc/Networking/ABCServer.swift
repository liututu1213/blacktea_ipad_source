//
//  ABCServer.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/12.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit

public enum ABCEnvironment: String {
    case test    = "test"
    case release = "release"
}

extension ABCEnvironment {
    var server: ABCServer {
        return ABCServer.server(self)
    }
}

struct ABCServer {
    var baseURL = ""
    var domain = ""

    public static func server(_ envronment: ABCEnvironment) -> ABCServer {
        switch envronment {
        case .test:
            return ABCServer(baseURL: "http://mp.yumabc.com:3331/latte_member_api/", domain: "mp.yumabc.com")
        case .release:
            return ABCServer(baseURL: "https://api.31abc.com/latte_member_api/", domain: "api.31abc.com")
        }
    }
}
