//
//  ABCTargetType.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/12.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import Moya

public protocol ABCTargetType: TargetType {
    var api:(baseURL: URL, path: String, method: Moya.Method, task: Task) { get }
}

extension ABCTargetType {
    public var baseURL: URL {
        return api.baseURL
    }

    var path: String {
        return api.path
    }

    var task: Task {
        return api.task
    }

    var method: Moya.Method {
        return api.method
    }

    var headers: [String : String]? {
        return ABCMoya.headers
    }

    var sampleData: Data {
        return "".data(using: String.Encoding.utf8)!
    }
}
