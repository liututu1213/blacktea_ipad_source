//
//  ABCMoya.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/12.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import Moya
import Alamofire

public protocol ABCMoyaProtocol {
    static var baseURL: URL { get }
    static var environment:  ABCEnvironment { get set }
    static var headers: [String: String] { get }
    static var manager: Manager { get }
}

struct ABCMoya: ABCMoyaProtocol {
    static var baseURL: URL {
        return URL(string: environment.server.baseURL)!
    }

    private static var _environment: ABCEnvironment = {
       return UserDefaults.standard[.environment].flatMap{ABCEnvironment(rawValue: $0)} ?? .release
    }()

    public static var environment: ABCEnvironment {
        get { return _environment }
        set {
            NotificationCenter.default.post(name: .environmentWillChangeNotification, object: nil)
            _environment = newValue

            UserDefaults.standard[.environment] = newValue.rawValue
            NotificationCenter.default.post(name: .environmentDidChangedNotification, object: nil)
        }
    }

    static var headers: [String : String] {
        return ["Content-Type":"application/json"]
    }

    static var manager: Manager {
        /// can set ServerTrustPolicy
        return Manager(configuration: URLSessionConfiguration.default)
    }
}

extension Notification.Name {
    static let environmentWillChangeNotification = Notification.Name("EnvironmentWillChangeNotification")
    static let environmentDidChangedNotification = Notification.Name("EnvironmentDidChangedNotification")
}
