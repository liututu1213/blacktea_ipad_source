//
//  AboutViewController.swift
//  31abc
//
//  Created by Cathy on 2019/12/15.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class AboutViewController: MenuBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = "31abc App"

        view.backgroundColor = .white
        preferredContentSize = CGSize(width: 366, height: 240)
        navigationController?.navigationBar.isHidden = true


        view.addSubview(contentLabel)
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.top.equalTo(headerView.snp.bottom).offset(15)
        }

        view.addSubview(confirmButton)
        confirmButton.snp.makeConstraints { (make) in
            make.right.bottom.equalTo(view).offset(-20)
            make.width.equalTo(100)
            make.height.equalTo(36)
        }

    }

    @objc func confirmClick() {
        closeClick()
    }

    fileprivate lazy var contentLabel: UILabel = {
        let l = UILabel()
        l.text = "Version: \(Bundle.main.shortVersion) \nOS: iOS \(UIDevice.current.systemVersion) \n\n Copyright © 2019 31abc"
        l.textColor = UIColor("#4F4F4F")
        l.font = UIFont.systemFont(ofSize: 14)
        l.numberOfLines = 0
        return l
    }()

    fileprivate lazy var confirmButton: UIButton = {
        let b = UIButton()
        b.backgroundColor = UIColor.main
        b.layer.cornerRadius = 5
        b.layer.masksToBounds = true
        b.setTitle("OK", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.addTarget(self, action: #selector(confirmClick), for: .touchUpInside)
        return b
    }()
}
