//
//  RecordModel.swift
//  31abc
//
//  Created by Cathy on 2020/2/20.
//  Copyright © 2020 31abc. All rights reserved.
//

import UIKit
import HandyJSON

struct Coach: HandyJSON {
    var name = ""
    var header = ""
    var user_id: UInt?
}

struct RecordModel: HandyJSON {
    struct Video: HandyJSON {
        var user_id: UInt = 0
        var id: Int = 0
        var timestamp: TimeInterval = 0.0
        var url: URL?
        var duration: Double?

        mutating func mapping(mapper: HelpingMapper) {
            mapper <<<
                url <-- URLTransform(shouldEncodeURLString: false)
        }
    }

    var recording: [Video] = []
    var ios_recording_url: URL?
    var coach = Coach()

    mutating func mapping(mapper: HelpingMapper) {
        mapper <<<
            ios_recording_url <-- URLTransform(shouldEncodeURLString: false)
    }

        //URL(string: "http://mp.yumabc.com:3332/playback-test-v0.2.0/?token=32a309d30b65e5f75f0ea249ad6fe0b227cb783e&classId=197854&uid=10030&role=0&platform=ios#/")!
}

enum BridgeType: String, HandyJSONEnum {
    case loaded = "PBW_WEBPAGE_LOADED"
    case setup = "PBW_INITINFO_POST_TO_JS"
    case play = "PBW_VIDEO_PLAY"
    case pause = "PBW_VIDEO_PAUSE"
    case star = "PBW_STAR_ADD"
    case resetStar = "PBW_STAR_RESET"
    case none = "NONE"
}

struct BridgeModel: HandyJSON {

    struct Data: HandyJSON {
        struct Video: HandyJSON {
            var id: Int = 0
            var duration: TimeInterval = 0.0
        }
        var recordingVideos: [Video] = []
    }
    var type: BridgeType = .setup
    var data: Data = Data()
}

struct WebInfo: HandyJSON {
    struct Data: HandyJSON {
        var curTime: TimeInterval = 0.0
    }

    var type: BridgeType = .none
    var data: Data = Data()
}


struct WebStar: HandyJSON {
    struct Data: HandyJSON {
        var curTime: TimeInterval = 0.0
        var userId: UInt = 0
        var star: Int = 0
        var isSeeking: Bool = false
    }

    var type: BridgeType = .none
    var data: Data = Data()
}
