//
//  RecordViewController.swift
//  31abc
//
//  Created by Cathy on 2019/12/19.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import WebKit
import AVKit
import NVActivityIndicatorView

class RecordViewController: BaseViewController {
    enum VideoType {
        case student
        case teacher
    }

    class RecordVideo {
        var url: URL
        var player: AVPlayer?
        var type: VideoType
        var id: Int = 0
        var timestamp: TimeInterval = 0.0

        var durtion: TimeInterval = 0.0
        weak var layer: AVPlayerLayer?


        init(id: Int, url: URL, player: AVPlayer, type: VideoType, timestamp: TimeInterval) {
            self.id = id
            self.url = url
            self.player = player
            self.type = type
            self.timestamp = timestamp
        }

        deinit {
            print("RecordVideo deinit.....")
        }
    }

    var recordModel: RecordModel
    var videoList: [RecordVideo] = []
    var readyToPlay: Bool = false
    var isPlaying: Bool = false
    public var animationViews: [UIView] = []


    init(record: RecordModel) {
        recordModel = record
        super.init(nibName: nil, bundle: nil)
        
        setupVideo()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.main

        layoutUI()
        layoutNavigationItem()
        loadURL()
        addObserver()
        
        webView.configuration.userContentController.add(self, name: "playbackRoomToOC")
    }

    deinit {
        videoList.forEach { (video) in
            video.player?.currentItem?.removeObserver(self, forKeyPath: "status")
            video.player?.currentItem?.removeObserver(self, forKeyPath: "loadedTimeRanges")
            video.player?.currentItem?.removeObserver(self, forKeyPath: "playbackBufferEmpty")
            video.player?.currentItem?.removeObserver(self, forKeyPath: "playbackLikelyToKeepUp")
            video.player = nil
        }

        videoList = []
        NotificationCenter.default.removeObserver(self)
         print("RecordViewController deinit.....")
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        webView.configuration.userContentController.removeScriptMessageHandler(forName: "playbackRoomToOC")
        stopPlay()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
    }

    fileprivate func addObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(routeChangeNotification(_:)),
            name: AVAudioSession.routeChangeNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationBecomeActive),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)

        NotificationCenter.default.addObserver(
                  self,
                  selector: #selector(didEnterBackground),
                  name: UIApplication.didEnterBackgroundNotification,
                  object: nil)
    }

    func reloadPage() {
        webView.reload()

    }

    func setupVideo() {
        let coachId = recordModel.coach.user_id
        let studentId = AccountManager.shared.student?.user_id

        var temp: [RecordVideo] = []
        recordModel.recording.forEach { (video) in
            guard let url = video.url else { return }
            if video.user_id == studentId || video.user_id == coachId {
                let playerItem = AVPlayerItem(asset: AVAsset(url: url))
                let player = AVPlayer(playerItem: playerItem)
                let layer = AVPlayerLayer(player: player)
                layer.isHidden = true

                playerItem.addObserver(self, forKeyPath: "status", options: .new, context: nil)
                playerItem.addObserver(self, forKeyPath: "loadedTimeRanges", options: .new, context: nil)
                playerItem.addObserver(self, forKeyPath: "playbackBufferEmpty", options: .new, context: nil)
                playerItem.addObserver(self, forKeyPath: "playbackLikelyToKeepUp", options: .new, context: nil)

                if video.user_id == coachId {
                    let re = RecordVideo(id: video.id, url: url, player: player, type: .teacher, timestamp: video.timestamp)
                    re.layer = layer
                    layer.frame = teacherView.bounds
                    teacherView.layer.addSublayer(layer)
                    teacherView.addSubview(teacherActivityView)
                    temp.append(re)

                } else if video.user_id == studentId {
                    let re = RecordVideo(id: video.id, url: url, player: player, type: .student, timestamp: video.timestamp)
                    re.layer = layer
                    layer.frame = myselfView.bounds
                    myselfView.layer.addSublayer(layer)
                    myselfView.addSubview(myselfActivityView)
                    temp.append(re)
                }
            }
        }

        videoList = temp
        log(recordModel.recording.toJSONString())
    }

    func checkCurrentPlayItem() {
        videoList.forEach { (video) in

            if let currentItem = video.player?.currentItem {
                guard let range = currentItem.loadedTimeRanges.first?.timeRangeValue else { return }
                let startTime = CMTimeGetSeconds(range.start)
                let duration = CMTimeGetSeconds(range.duration)
                let result = startTime + duration

                let currentTime = CMTimeGetSeconds(currentItem.currentTime())

                if result < currentTime {
                    if video.type == .teacher {
                        teacherActivityView.startAnimating()
                    } else {
                        myselfActivityView.startAnimating()
                    }
                } else {
                    if video.type == .teacher {
                        teacherActivityView.stopAnimating()
                    } else {
                        myselfActivityView.stopAnimating()
                    }
                }

                // print("当前播放进度:\(currentTime), 当前缓存进度:\(result)")
            }
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status" {
            if videoList.count >= 2,
                let currentItem1 = videoList[0].player?.currentItem,
                let currentItem2 = videoList[1].player?.currentItem {
                readyToPlay = (currentItem1.status == .readyToPlay && currentItem2.status == .readyToPlay)
            }
        } else if keyPath == "loadedTimeRanges" {
            checkCurrentPlayItem()

        } else if keyPath == "playbackBufferEmpty" {
            if videoList.count >= 2,
                let currentItem1 = videoList[0].player?.currentItem,
                let currentItem2 = videoList[1].player?.currentItem {

                //缓存不足
                guard let item = object as? AVPlayerItem, isPlaying == true else { return }
                if item == currentItem1 {

                    teacherActivityView.startAnimating()

                } else if item == currentItem2 {
                    myselfActivityView.startAnimating()

                }
            }
        } else if keyPath == "playbackLikelyToKeepUp" {

            if videoList.count >= 2,
                let currentItem1 = videoList[0].player?.currentItem,
                let currentItem2 = videoList[1].player?.currentItem {

                guard let item = object as? AVPlayerItem else { return }
                if item == currentItem1 {
                    teacherActivityView.stopAnimating()

                } else if item == currentItem2 {
                    myselfActivityView.stopAnimating()
                }
            }

        }
    }

    func messageHand(webInfo: WebInfo) {
        log(webInfo.toJSONString())

        switch webInfo.type {
        case .loaded :
            sendRecordInfo()
        case .play:
            play(video: webInfo.data)
        case .pause:
            pause(video: webInfo.data)
        case .resetStar:
            starNumberLabel.text = ""
        default:()
        }
    }

    func sendRecordInfo() {
        if !readyToPlay { return }
        stopPlay()

        let videos = videoList.map { (video) -> BridgeModel.Data.Video in
            return .init(id: video.id, duration: (video.player?.currentItem?.asset.duration.seconds ?? 0.0) * 1000.0)
        }

        let info = BridgeModel(type: .setup, data: BridgeModel.Data(recordingVideos: videos))
        guard let str = info.toJSONString() else { return }
//        bridge.callHandler("playbackRoomToJs", data: str) { (data) in
//            print("data")
//        }

        let jsStr = "playbackRoomToJs(\(str))"

        webView.evaluateJavaScript(jsStr) { (data, error) in
            print(data)
            print(error)
        }

        log("send message to js:\(str)")
    }

    func play(video data: WebInfo.Data) {
        isPlaying = true
        videoList.forEach { (video) in
            video.layer?.isHidden = false
            video.layer?.frame =  video.type == .student ? myselfView.bounds: teacherView.bounds

            let time = (video.player?.currentItem?.asset.duration.seconds ?? 0.0) * 1000.0
            if data.curTime < video.timestamp {
                DispatchQueue.main.asyncAfter(deadline: .now() + (video.timestamp - data.curTime) / 1000) {
                    video.player?.play()
                }
            } else if data.curTime > (video.timestamp + time) {
                //video.layer?.removeFromSuperlayer()
                 video.layer?.isHidden = true
                isPlaying = false
            } else {
                video.player?.seek(to: CMTime(seconds: (data.curTime - video.timestamp) / 1000, preferredTimescale: CMTimeScale(1000)))
                video.player?.play()
            }
        }
    }

    func pause(video data: WebInfo.Data) {
        isPlaying = false
        videoList.forEach { (video) in
            let time = (video.player?.currentItem?.asset.duration.seconds ?? 0.0) * 1000.0
            if data.curTime > (video.timestamp + time) {
                stopPlay()
            } else {
                video.player?.pause()
            }
        }
    }

    func stopPlay() {
        isPlaying = false
        videoList.forEach { (video) in
            //video.layer?.removeFromSuperlayer()
            video.layer?.isHidden = true
            video.player?.seek(to: .zero)
            video.player?.pause()
        }
    }

    @objc func routeChangeNotification(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSession.RouteChangeReason(rawValue: reasonValue),
            isPlaying else { return }

        switch reason {
        case .oldDeviceUnavailable:
            if let previousRoute = userInfo[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription {
                for output in previousRoute.outputs where output.portType == AVAudioSession.Port.headphones {
                    DispatchQueue.main.async {

                        if !self.isPlaying { return }
                        self.videoList.forEach({$0.player?.play()})
                    }
                }
            }
        default:
            ()
        }
    }

    @objc func applicationBecomeActive() {
        webView.evaluateJavaScript("willEnterForeground()", completionHandler: nil)
    }

    @objc func didEnterBackground() {
        webView.evaluateJavaScript("didEnterBackground()", completionHandler: nil)
    }

    fileprivate func loadURL() {
        if let url = recordModel.ios_recording_url {
            webView.load(URLRequest(url: url))
        }
//
//        guard let url = Bundle.main.url(forResource: "testWebview3.html", withExtension: nil) else { return }
//        webView.loadFileURL(url, allowingReadAccessTo: url)
    }

    fileprivate func back() {
        navigationController?.popViewController(animated: true)
    }

    // MARK: - setupUI
    func layoutNavigationItem() {
        view.addSubview(backButton)
        view.addSubview(refreshButton)

        backButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.top.equalTo(view).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(53)
        }

        refreshButton.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-20)
            make.top.equalTo(view).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(53)
        }
    }

    fileprivate func layoutUI() {

        view.addSubview(classView)
        view.addSubview(teacherView)
        view.addSubview(myselfView)
        view.addSubview(teacherTitleView)
        view.addSubview(myselfTitleView)
        view.addSubview(starContentView)
        view.addSubview(starImageView)
        view.addSubview(starNumberLabel)

        classView.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.centerY.equalTo(view)
            make.width.equalTo(kClassViewWidth)
            make.height.equalTo(kClassViewHeight)
        }

        teacherView.snp.makeConstraints { (make) in
            make.left.equalTo(classView.snp.right).offset(25)
            make.top.equalTo(classView)
            make.width.equalTo(kTeacherViewWidth)
            make.height.equalTo(kTeacherViewWidth * kAspectRatio)
        }

        teacherTitleView.snp.makeConstraints { (make) in
            make.left.top.equalTo(teacherView).offset(10)
            make.height.equalTo(30)
            make.right.lessThanOrEqualTo(teacherView).offset(-50)
        }

        myselfView.snp.makeConstraints { (make) in
            make.left.equalTo(teacherView)
            make.top.equalTo(teacherView.snp.bottom).offset(25)
            make.width.equalTo(kTeacherViewWidth)
            make.height.equalTo(kTeacherViewWidth * kAspectRatio)
        }

        myselfTitleView.snp.makeConstraints { (make) in
            make.left.top.equalTo(myselfView).offset(10)
            make.height.equalTo(30)
            make.right.lessThanOrEqualTo(myselfView).offset(-50)
        }

        classView.addSubview(webView)
        webView.snp.makeConstraints { (make) in
            make.edges.equalTo(classView)
        }

        starContentView.snp.makeConstraints { (make) in
            make.left.equalTo(myselfView)
            make.top.equalTo(myselfView.snp.bottom).offset(24)
            make.width.equalTo(147)
            make.height.equalTo(56)
        }

        starImageView.snp.makeConstraints { (make) in
            make.left.equalTo(starContentView).offset(10)
            make.centerY.equalTo(starContentView)
        }

        starNumberLabel.snp.makeConstraints { (make) in
            make.left.equalTo(starImageView.snp.right).offset(5)
            make.centerY.equalTo(starContentView)
            make.right.equalTo(starContentView).offset(-15)
        }

        myselfTitleView.text = AccountManager.shared.student?.name
        teacherTitleView.text = recordModel.coach.name

    }

    // MARK: - SubViews
    fileprivate lazy var backButton: AnimationButton = {
        let b = AnimationButton()
        b.cornerRadius = 5.0
        b.image = UIImage(named: "arrow_left")?.repaintImage(tintColor: UIColor("#745B1A"))
        b.clickClosure = {[weak self] (button) in
            self?.back()
        }
        return b
    }()

    fileprivate lazy var refreshButton: AnimationButton = {
        let b = AnimationButton()
        b.cornerRadius = 5.0
        b.title = "刷新"
        b.setTitleColor(UIColor("#745B1A"), for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        b.clickClosure = {[weak self] (button) in
            self?.reloadPage()
        }
        return b
    }()

    lazy var classView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("#FCF0C4")
        v.layer.borderColor = UIColor.white.cgColor
        v.layer.borderWidth = 5
        v.layer.cornerRadius = 10.0
        v.layer.masksToBounds = true

        let backgroundView = UIImageView(image: UIImage(named: "course_placeholder"))
        v.addSubview(backgroundView)
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }

        return v
    }()

    lazy var teacherView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.main
        v.layer.borderColor = UIColor("#F2C94C").cgColor
        v.layer.borderWidth = 5
        v.layer.cornerRadius = 10.0
        v.layer.masksToBounds = true
        
        let backgroundView = UIImageView(image: UIImage(named: "avatar_placeholder_teacher"))
        v.addSubview(backgroundView)
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }
        return v
    }()

    fileprivate lazy var teacherTitleView: ABCLabel = {
        let l = ABCLabel()
        l.backgroundColor = UIColor(white: 0, alpha: 0.4)
        l.layer.cornerRadius = 15.0
        l.layer.masksToBounds = true
        l.textColor = .white
        l.font = UIFont.systemFont(ofSize: 18)
        l.textAlignment = .center
        l.textInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        return l
    }()

    fileprivate lazy var myselfView: UIView = {
         let v = UIView()
         v.backgroundColor = UIColor("#FCF0C4")
         v.layer.borderColor = UIColor.white.cgColor
        v.layer.borderWidth = 5
        v.layer.cornerRadius = 10.0
        v.layer.masksToBounds = true

        let backgroundView = UIImageView(image: UIImage(named: "avatar_placeholder_student"))
        v.addSubview(backgroundView)
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }
        return v
     }()

    fileprivate lazy var myselfTitleView: ABCLabel = {
        let l = ABCLabel()
        l.backgroundColor = UIColor(white: 0, alpha: 0.4)
        l.layer.cornerRadius = 15.0
        l.layer.masksToBounds = true
        l.textColor = .white
        l.font = UIFont.systemFont(ofSize: 18)
        l.textAlignment = .center
        l.textInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        return l
    }()

    fileprivate lazy var webView: WKWebView = {
        let config = WKWebViewConfiguration()
        config.allowsInlineMediaPlayback = true
        config.userContentController = WKUserContentController()

        let wk = WKWebView(frame: .zero, configuration: config)
        wk.navigationDelegate = self
        wk.uiDelegate = self
        wk.scrollView.alwaysBounceVertical = false
        wk.scrollView.bouncesZoom = false
        wk.scrollView.isScrollEnabled = false
        wk.backgroundColor = .clear
        wk.scrollView.backgroundColor = .clear
        return wk
    }()

    fileprivate lazy var starContentView: UIView = {
         let v = UIView()
         v.backgroundColor = UIColor(white: 0, alpha: 0.4)
         v.layer.cornerRadius = 28.0
         v.layer.masksToBounds = true
         return v
     }()

     lazy var starImageView: UIImageView = {
         let i = UIImageView()
         i.image = UIImage(named: "star")
         return i
     }()

     lazy var starNumberLabel: UILabel = {
         let l = UILabel()
         l.textColor = .white
         l.font = UIFont.systemFont(ofSize: 36)
         l.textAlignment = .center
         return l
     }()

    fileprivate lazy var teacherActivityView: NVActivityIndicatorView = {
        let v = NVActivityIndicatorView(
            frame: CGRect(x: kTeacherViewWidth / 2 - 30,
                          y: (kTeacherViewWidth * kAspectRatio) / 2 - 30,
                          width: 60,
                          height: 60))
        return v
    }()

    fileprivate lazy var myselfActivityView: NVActivityIndicatorView = {
        let v = NVActivityIndicatorView(
            frame: CGRect(x: kTeacherViewWidth / 2 - 30,
                          y: (kTeacherViewWidth * kAspectRatio) / 2 - 30,
                          width: 60,
                          height: 60))
        return v
    }()

}

extension RecordViewController: WKNavigationDelegate, WKUIDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopAnimating()
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        startAnimating()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        stopAnimating()
    }
}

//MARK: - Star
extension RecordViewController {

    fileprivate func updateStartView(_ data: WebStar.Data) {
        if data.userId == AccountManager.shared.student?.user_id {
            starNumberLabel.text = "\(data.star)"
            if data.star > 0 && !data.isSeeking {
                startAnimation()
            }
        }
    }

    func startAnimation() {
        AudioPlayer.shared.playAudio(.star)
        let endPoint = view.convert(starImageView.center, to: nil)
        let startPoint = classView.center

        let animationImageView = UIImageView(image: UIImage(named: "star"))
        animationImageView.center = startPoint
        view.addSubview(animationImageView)
        animationViews.append(animationImageView)

        let transform = CGAffineTransform(rotationAngle:  CGFloat(-Double.pi / 2))
        animationImageView.transform = transform
        animationImageView.alpha = 0

        UIView.animate(withDuration: kAnimationDuration,
                       delay: 0,
                       options: [.curveEaseOut],
                       animations: {

                        let transform = CGAffineTransform(rotationAngle: 0)
                        animationImageView.transform = transform.scaledBy(x: 2, y: 2)
                        animationImageView.alpha = 1

        }) { (finished) in

            let pathAnimation = CAKeyframeAnimation(keyPath: "position")
            let value1: NSValue = NSValue(cgPoint: animationImageView.center)
            let value2: NSValue = NSValue(cgPoint: endPoint)
            pathAnimation.values = [value1, value2]

            //旋转
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnimation.toValue = Double.pi / 3

            // 缩放
            let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.toValue = 0.5

            let group = CAAnimationGroup()
            group.animations = [pathAnimation, scaleAnimation, rotationAnimation]
            group.isRemovedOnCompletion = false
            group.fillMode = .forwards
            group.duration = kAnimationDuration
            group.delegate = self
            group.timingFunction = CAMediaTimingFunction(name: .easeIn)

            animationImageView.layer.add(group, forKey: nil)
        }
    }
}

extension RecordViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if let view = animationViews.first {
            view.removeFromSuperview()
            self.animationViews.removeFirst()
        }
    }
}

extension RecordViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        switch message.name {
        case "playbackRoomToOC":

            let dic = message.body as? [String :Any]
            if let info: WebInfo = WebInfo.deserialize(from: dic){
                messageHand(webInfo: info)

            }
            if let star = WebStar.deserialize(from: dic) {
                log(star.toJSONString())
                updateStartView(star.data)
            }

        default:
            break
        }
    }
}
