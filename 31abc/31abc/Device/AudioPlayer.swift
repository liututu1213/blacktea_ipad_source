//
//  AudioPlayer.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/11.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import AVFoundation

let minPower: Double = -160.0

class AudioPlayer: NSObject, AVAudioPlayerDelegate {
    enum PathType: String {
        case `default` = "recorder.caf"
        case recorder  = "recorder"
        case star = "addstar.caf"

        var filePath: String? {
            switch self {
            case .recorder:
                return NSHomeDirectory().appending("/Documents/").appending(PathType.recorder.rawValue)
            default:
                return Bundle.main.path(forResource: rawValue, ofType: nil)
            }
        }
    }

    static let shared = AudioPlayer()

    var playAudioProgress: ((Double) -> Void)?
    var recorderProgress: ((Double) -> Void)?

    private var player: AVAudioPlayer?
    private var recorder:AVAudioRecorder?
    private var timer: Timer?
    private var recorderParams: [String: Any] = [:]

    func playAudio(_ type: PathType ) {
        stopAudio()

        guard let path = type.filePath else { return }
        let url = URL(fileURLWithPath: path)
        player = try? AVAudioPlayer.init(contentsOf: url)
        player?.prepareToPlay()
        player?.play()
        player?.delegate = self
        player?.isMeteringEnabled = true

        if type != .star {
            timer = Timer(timeInterval: 1, target: self, selector: #selector(updateAudio), userInfo: nil, repeats: true)
            RunLoop.current.add(timer!, forMode: .common)
        }

    }

    func stopAudio() {
        player?.stop()
        stopTimer()
    }

    func startRecorder() {
        let session = AVAudioSession.sharedInstance()
        try? session.setCategory(.playAndRecord)
        let file = NSHomeDirectory().appending("/Documents/").appending(PathType.recorder.rawValue)
        let url = URL(fileURLWithPath: file)

        recorderParams = [
            AVFormatIDKey: NSNumber(value: kAudioFormatMPEG4AAC),
            AVNumberOfChannelsKey: 2,
            AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
            AVEncoderBitRateKey : 320000,
            AVSampleRateKey : 44100.0]

        recorder = try? AVAudioRecorder(url: url, settings: recorderParams)
        recorder?.isMeteringEnabled = true
        recorder?.prepareToRecord()
        recorder?.record()

        timer = Timer(timeInterval: 1, target: self, selector: #selector(updateRecorder), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: .common)
    }

    func stopRecorder() {
        recorder?.stop()
        recorder = nil
        stopTimer()
    }

    func reset() {
        stopAudio()
        stopRecorder()
    }

    func stopTimer() {
        timer?.invalidate()
        timer = nil
    }

    @objc fileprivate func updateRecorder() {
        recorder?.updateMeters()
        let average = Double(recorder?.averagePower(forChannel: 0) ?? 0.0)
        let progress = (average - minPower) / abs(minPower)
        recorderProgress?(progress)
    }

    @objc fileprivate func updateAudio() {
        player?.updateMeters()
        let average = Double(player?.averagePower(forChannel: 0) ?? 0.0)
        let progress = (average - minPower) / abs(minPower)
        playAudioProgress?(progress)
    }
}
