//
//  DeviceCheckView.swift
//  4iPad
//
//  Created by Cathy on 2019/9/28.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit

class DeviceCheckView: UIView {
    enum CheckType: Int {
        case camera = 0
        case loudspeaker
        case microphone
        case network

        var title: String {
            switch self {
            case .camera:
                return "摄像头"
            case .loudspeaker:
                return "扬声器"
            case .microphone:
                return "麦克风"

            case .network:
                return "网络状况"
            }
        }

        var failed: String? {
            switch self {
            case .camera:
                return "看不到"
            case .loudspeaker:
                return "听不到"
            case .microphone:
                return "听不到"
            case .network:
                return nil
            }
        }

        var succuess: String? {
            switch self {
            case .camera:
                return "可以看到"
            case .loudspeaker:
                return "可以听到"
            case .microphone:
                return "可以听到"

            case .network:
                return "完成"
            }
        }
    }

    var closeClosure: (() -> Void)?

    private var currentType: CheckType = .camera
    private let dataSource = [
        MenuListView.Item(title: CheckType.camera.title),
        MenuListView.Item(title: CheckType.loudspeaker.title),
        MenuListView.Item(title: CheckType.microphone.title),
        MenuListView.Item(title: CheckType.network.title)]

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = .white
        layer.cornerRadius = 10.0
        layoutUI()
        startCheck()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func layoutUI() {
        addSubview(listiew)
        listiew.snp.makeConstraints { (make) in
            make.top.equalTo(self)
            make.left.equalTo(self)
            make.width.equalTo(126)
            make.bottom.equalTo(self)
        }

        addSubview(localVideoContenView)
        localVideoContenView.snp.makeConstraints { (make) in
            make.left.equalTo(listiew.snp.right)
            make.top.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self)
        }

        addSubview(speakerContenView)
        speakerContenView.snp.makeConstraints { (make) in
            make.left.equalTo(listiew.snp.right)
            make.top.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self)
        }

        addSubview(microContenView)
        microContenView.snp.makeConstraints { (make) in
            make.left.equalTo(listiew.snp.right)
            make.top.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self)
        }

        addSubview(networkContentView)
        networkContentView.snp.makeConstraints { (make) in
            make.left.equalTo(listiew.snp.right)
            make.top.equalTo(self)
            make.right.equalTo(self)
            make.bottom.equalTo(self)
        }

        //底部button
        addSubview(failedButton)
        addSubview(successButton)

        successButton.snp.makeConstraints { (make) in
            make.right.equalTo(self).offset(-16)
            make.bottom.equalTo(self).offset(-16)
            make.height.equalTo(34)
            make.width.equalTo(100)
        }

        failedButton.snp.makeConstraints { (make) in
            make.right.equalTo(successButton.snp.left).offset(-10)
            make.bottom.equalTo(self).offset(-16)
            make.height.equalTo(34)
            make.width.equalTo(100)
        }
    }

    public func startCheck() {
        DeviceDebugManager.shared.enableVideo(localVideoContenView.videoView)
    }

    public func stopCheck() {
        AudioPlayer.shared.reset()
        DeviceDebugManager.shared.leaveChannel()
    }

    public func retestCheck() {
        stopCheck()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            DeviceDebugManager.shared.enableVideo(self.localVideoContenView.videoView)
        }
    }

    //MARK: - Action
    fileprivate func selectedItemClick(_ type: CheckType) {
        currentType = type
        AudioPlayer.shared.stopAudio()
        resetContentView()

        failedButton.isHidden = currentType.failed == nil
        successButton.isHidden = currentType.succuess == nil
        failedButton.setTitle(currentType.failed, for: .normal)
        successButton.setTitle(currentType.succuess, for: .normal)

        switch type {
        case .camera:
            localVideoContenView.isHidden = false

        case .loudspeaker:
            speakerContenView.isHidden = false
            speakerContenView.playAudio()

        case .microphone:
            microContenView.isHidden = false

        case .network:
            networkContentView.isHidden = false
        }
    }

    func resetContentView() {
        localVideoContenView.isHidden = true
        speakerContenView.isHidden = true
        microContenView.isHidden = true
        networkContentView.isHidden = true
    }

    @objc fileprivate func failedClick() {
        checkItem(success: false)
    }

    @objc fileprivate func successClick() {
        checkItem(success: true)
    }

    func checkItem(success: Bool) {
        let errorImage = UIImage(named: "warning_alt")?.repaintImage(tintColor: UIColor("#EB5757"))
        let successImage = UIImage(named: "o_checked")?.repaintImage(tintColor: UIColor.main)
        let image = success ? successImage : errorImage

        guard currentType.rawValue < dataSource.count else { return }
        let item = dataSource[currentType.rawValue]
        item.image = image
        listiew.reloadItem(item: item, at: currentType.rawValue)

        let status: DeviceCheckStatus = success ? .normal : .unnormal
        let deviceInfo = DeviceDebugManager.shared.deviceInfo

        switch currentType {
        case .camera:
            deviceInfo.camera_arbitrary = DeviceManager.videoStatus
            deviceInfo.camera_status = status

        case .loudspeaker:
            deviceInfo.headphone_arbitrary = success ? .normal: .disable

        case .microphone:
            deviceInfo.microphone_status = status
            deviceInfo.microphone_arbitrary = DeviceManager.audioStatus
        case .network:
            deviceInfo.network_status = DeviceDebugManager.shared.txQuality.rawValue
        }

        if let type = CheckType(rawValue: currentType.rawValue + 1) {
            selectedItemClick(type)
            listiew.selectItem(at: type.rawValue)
        } else {
            closeClosure?()
        }
    }

    //MARK: - SubViews
    fileprivate lazy var listiew: MenuListView = {
        let l = MenuListView()
        l.dataSource = dataSource
        l.selectedClosure = {[weak self] (index) in
            if let type = CheckType(rawValue: index) {
                self?.selectedItemClick(type)
            }
        }

        /// 默认选中0
        l.selectItem(at: 0)
        selectedItemClick(.camera)
        return l
    }()

    lazy var localVideoContenView: VideoContentView = {
        let v = VideoContentView()
        return v
    }()

    fileprivate lazy var speakerContenView: SpeakerContenView = {
        let v = SpeakerContenView()
        return v
    }()

    fileprivate lazy var microContenView: MicroContenView = {
        let v = MicroContenView()
        return v
    }()

    fileprivate lazy var networkContentView: NetworkConentView = {
        let v = NetworkConentView()
        v.retestClosure = {[weak self] in
            self?.retestCheck()
        }
        return v
    }()

    fileprivate lazy var failedButton: UIButton = {
        let b = UIButton()
        b.layer.borderWidth = 1
        b.layer.borderColor = UIColor("#E3E3E3").cgColor
        b.layer.cornerRadius = 5
        b.layer.masksToBounds = true
        b.setTitle("听不到", for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.backgroundColor = .white
        b.setTitleColor(UIColor("#4F4F4F"), for: .normal)
        b.addTarget(self, action: #selector(failedClick), for: .touchUpInside)
        return b
    }()
    
    fileprivate lazy var successButton: UIButton = {
        let b = UIButton()
        b.backgroundColor = UIColor.main
        b.layer.cornerRadius = 5
        b.layer.masksToBounds = true
        b.setTitle("可以听到", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.addTarget(self, action: #selector(successClick), for: .touchUpInside)
        return b
    }()
}
