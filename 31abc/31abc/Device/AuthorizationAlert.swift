//
//  AuthorizationAlert.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/25.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class AuthorizationAlert: MenuBaseViewController {
    var confirmClosure: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = "提示"

        view.backgroundColor = .white
        preferredContentSize = CGSize(width: 360, height: 210)
        navigationController?.navigationBar.isHidden = true

        view.addSubview(contentLabel)
        contentLabel.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.top.equalTo(headerView.snp.bottom).offset(15)
        }

        view.addSubview(confirmButton)
        confirmButton.snp.makeConstraints { (make) in
            make.right.bottom.equalTo(view).offset(-15)
            make.height.equalTo(34)
            make.width.equalTo(100)
        }
    }

    @objc func confirm() {
        confirmClosure?()
    }

    fileprivate lazy var contentLabel: UILabel = {
        let l = UILabel()
        l.text = "请在iPad的“设置 - 31abc”选项中， \n允许31abc访问你的设备相机和麦克风。"
        l.textColor = UIColor("#4F4F4F")
        l.font = UIFont.systemFont(ofSize: 14)
        l.numberOfLines = 0
        return l
    }()

    fileprivate lazy var confirmButton: UIButton = {
        let b = UIButton()
        b.setTitle("确定", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.backgroundColor = UIColor.main
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        b.addTarget(self, action: #selector(confirm), for: .touchUpInside)
        return b
    }()
}

