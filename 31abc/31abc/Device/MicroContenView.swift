//
//  MicroContenView.swift
//  31abc
//
//  Created by Cathy on 2019/12/15.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class MicroContenView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate lazy var microProgressView: ProgressView = {
        let v = ProgressView()
        return v
    }()

    func layoutUI() {
        addSubview(microProgressView)
        addSubview(microTipLabel)
        addSubview(microButton)

        microTipLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(self).offset(50)
            make.right.equalTo(self).offset(-90)
        }

        microButton.snp.makeConstraints { (make) in
            make.left.equalTo(microTipLabel)
            make.top.equalTo(microTipLabel.snp.bottom).offset(20)
            make.width.equalTo(100)
            make.height.equalTo(34)
        }

        microProgressView.snp.makeConstraints { (make) in
            make.centerY.equalTo(microButton)
            make.left.equalTo(microButton.snp.right).offset(37)
            make.width.equalTo(164)
            make.height.equalTo(26)
        }
    }

    @objc func startRecorder() {
        microButton.isSelected = !microButton.isSelected

        if microButton.isSelected {
            AudioPlayer.shared.startRecorder()
            AudioPlayer.shared.recorderProgress = {[weak self] (progress) in
                self?.microProgressView.progress = progress
            }
            startTimer()

        } else {
            startPlayRecorder()
        }
    }

    fileprivate func startTimer() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
            if !self.microButton.isSelected { return }
            self.startPlayRecorder()
        }
    }

    fileprivate func startPlayRecorder() {
        AudioPlayer.shared.stopRecorder()
        AudioPlayer.shared.playAudio(.recorder)
        AudioPlayer.shared.playAudioProgress = {[weak self] (progress) in
            self?.microProgressView.progress = progress
        }
    }

    fileprivate lazy var microButton: UIButton = {
        let b = UIButton()
        b.backgroundColor = UIColor.main
        b.layer.cornerRadius = 5
        b.layer.masksToBounds = true
        b.setBackgroundImage(UIImage(color: UIColor.main), for: .normal)
        b.setBackgroundImage(UIImage(color: UIColor("#EB5757")), for: .selected)
        b.setTitle("录音", for: .normal)
        b.setTitle("停止录音", for: .selected)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.addTarget(self, action: #selector(startRecorder), for: .touchUpInside)
        return b
    }()

    fileprivate lazy var microTipLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = UIColor("#4F4F4F")
        l.text = "点击“录音”按钮，对着麦克风从1数到10，并查看绿色音量条是否波动。"
        l.numberOfLines = 0
        return l
    }()

}
