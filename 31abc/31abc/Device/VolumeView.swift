//
//  VolumeView.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/16.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit


class VolumeView: UIView {

    var maxCount = 5
    var maxWidth: CGFloat = 20
    var minWidth: CGFloat = 10.0
    private var items: [UIView] = []

    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func layoutUI() {

        var y: CGFloat = 0.0
        let height: CGFloat = 4.0
        let margin: CGFloat = 3.0

        for index in 0..<5 {
            let rate = (maxWidth - minWidth) / CGFloat((maxCount - 1))
            let currentWidth = rate * CGFloat(maxCount - index - 1) + minWidth
            y = CGFloat(index) * (height + margin)

            let view = UIView()
            self.addSubview(view)
            view.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
            view.layer.cornerRadius = height / 2.0
            view.layer.masksToBounds = true
            view.snp.makeConstraints { (make) in
                make.top.equalTo(self).offset(y)
                make.right.equalTo(self)
                make.height.equalTo(height)
                make.width.equalTo(currentWidth)

                if maxCount == index + 1 {
                    make.bottom.equalTo(self)
                }
            }
            items.append(view)
        }
    }

    var progress: Double = 0.0 {
        didSet {
            updateProgess()
        }
    }

    fileprivate func updateProgess() {
        var current = Int(progress * Double(maxCount))
        if current >= maxCount { current = maxCount }
        if current <= 0 { current = 0 }

        items.reversed().enumerated().forEach { (i, item) in
            if i < current {
                item.backgroundColor = .white
            } else {
                item.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
            }
        }
    }
}

