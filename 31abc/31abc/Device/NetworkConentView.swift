//
//  NetworkConentView.swift
//  31abc
//
//  Created by Cathy on 2019/12/15.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class NetworkConentView: UIView {

    var retestClosure: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)

        layoutUI()
        addNotifications()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func layoutUI() {
        addSubview(checkButton)
        addSubview(tipLabel)

        checkButton.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(50)
            make.top.equalTo(self).offset(40)
            make.width.equalTo(100)
            make.height.equalTo(34)
        }

        tipLabel.snp.makeConstraints { (make) in
            make.left.equalTo(checkButton)
            make.top.equalTo(checkButton.snp.bottom).offset(14)
            make.height.equalTo(40)
        }
    }

    func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(networkQualityChange), name: .NetworkQualityNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(checkStatusChange), name: .DeviceCheckNotification, object: nil)
    }

    @objc func networkQualityChange() {
        tipLabel.isHidden = false
        tipLabel.text = DeviceDebugManager.shared.txQualityDesc
        checkStatusChange()
    }

    @objc func checkStatusChange() {
        if DeviceDebugManager.shared.enableCheck {
            checkButton.isEnabled = false
        } else {
            checkButton.isEnabled = true
            checkButton.setTitle("重新检测", for: .normal)
        }
    }

    @objc func checkNetwork() {
        retestClosure?()
    }

    fileprivate lazy var checkButton: UIButton = {
        let b = UIButton()
        b.backgroundColor = UIColor.main
        b.layer.cornerRadius = 5
        b.layer.masksToBounds = true
        b.setBackgroundImage(UIImage(color: UIColor.main), for: .normal)
        b.setBackgroundImage(UIImage(color: UIColor("#BDBDBD")), for: .disabled)
        b.setTitle("检测", for: .normal)
        b.setTitle("检测中", for: .disabled)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.addTarget(self, action: #selector(checkNetwork), for: .touchUpInside)
        return b
    }()

    fileprivate lazy var tipLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = UIColor("#4F4F4F")
        l.isHidden = true
        l.numberOfLines = 0
        return l
    }()

}
