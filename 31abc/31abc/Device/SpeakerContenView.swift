//
//  SpeakerContenView.swift
//  31abc
//
//  Created by Cathy on 2019/12/15.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class SpeakerContenView: UIView {

     override init(frame: CGRect) {
        super.init(frame: frame)
        layoutUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func layoutUI() {
        addSubview(speakerProgressView)
        addSubview(speakerTipLabel)
        addSubview(speakerButton)

        speakerTipLabel.snp.makeConstraints { (make) in
            make.top.left.equalTo(self).offset(50)
            make.right.equalTo(self).offset(-90)
        }

        speakerButton.snp.makeConstraints { (make) in
            make.left.equalTo(speakerTipLabel)
            make.top.equalTo(speakerTipLabel.snp.bottom).offset(20)
            make.width.equalTo(100)
            make.height.equalTo(34)
        }

        speakerProgressView.snp.makeConstraints { (make) in
            make.centerY.equalTo(speakerButton)
            make.left.equalTo(speakerButton.snp.right).offset(37)
            make.width.equalTo(164)
            make.height.equalTo(26)
        }
    }

    func playAudio() {
        AudioPlayer.shared.playAudio(.default)
        AudioPlayer.shared.playAudioProgress = {[weak self] (progress) in
            self?.speakerProgressView.progress = progress
        }
        speakerButton.isSelected = true
    }

    func stopAudio() {
        AudioPlayer.shared.stopAudio()
        speakerButton.isSelected = false
    }

    @objc func speakerButtonClick() {
        speakerButton.isSelected ? stopAudio() : playAudio()
    }

    fileprivate lazy var speakerProgressView: ProgressView = {
        let v = ProgressView()
        return v
    }()
    
    fileprivate lazy var speakerTipLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = UIColor("#4F4F4F")
        l.text = "能否听见声音？"
        return l
    }()
    
    fileprivate lazy var speakerButton: UIButton = {
        let b = UIButton()
        b.setBackgroundImage(UIImage(color: UIColor.main), for: .normal)
        b.setBackgroundImage(UIImage(color: UIColor("#EB5757")), for: .selected)
        b.layer.cornerRadius = 5
        b.layer.masksToBounds = true
        b.setTitle("播放", for: .normal)
        b.setTitle("停止播放", for: .selected)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.addTarget(self, action: #selector(speakerButtonClick), for: .touchUpInside)
        return b
    }()
}
