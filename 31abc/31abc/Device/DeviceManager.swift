//
//  DeviceManager.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/1.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import AVKit

class DeviceManager: NSObject {
    static let shared = DeviceManager()

    override init() {
        super.init()

        NotificationCenter.default.addObserver(self, selector: #selector(login), name: .LoginNotification, object: nil)
    }

    @objc func login() {
        requestAuthorization()
    }

    ///授权状态
    class var videoStatus: DeviceStatus {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        return  status == .authorized ? .normal : .disable
    }

    class var audioStatus: DeviceStatus {
        let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        return  status == .authorized ? .normal : .disable
    }

    ///请求授权
    func requestAuthorization() {
        let videoStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        let audioStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.audio)
        if videoStatus == .notDetermined  {
            AVCaptureDevice.requestAccess(for: .video) { (status) in

            }
        }

        if audioStatus == .notDetermined {
            AVCaptureDevice.requestAccess(for: .audio) { (status) in

            }
        }

        if videoStatus == .denied || audioStatus == .denied {

            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let color = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                guard let visibleVC = NavigationTool.shared.visibleViewController() else { return }
                let options = ContextMenu.Options(containerStyle: ContextMenu.ContainerStyle(overlayColor: color),
                                                  menuStyle: .minimal,
                                                  backgroundHideAutomatically: true)

                let vc = AuthorizationAlert()
                vc.confirmClosure = {[weak self] in
                    self?.openSettings()
                }
                ContextMenu.shared.show(sourceViewController: visibleVC,
                                        viewController: vc ,
                                        options: options)

            }
        }
    }

    func openSettings() {
        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
