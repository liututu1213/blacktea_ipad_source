//
//  DeviceModel.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/3.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import HandyJSON

/// 设备状态，1为已安装，2为disable或不存在
enum DeviceStatus: Int, HandyJSONEnum {
    case normal  =  1
    case disable = 2
}

/// 程序检测设备状态，1为已安装，2为disable或不存在
enum DeviceCheckStatus: Int, HandyJSONEnum {
    case unnormal = 0
    case normal  =  1
}

class DeviceModel: HandyJSON {
    required init() {}

    var role: Int = 0
    var operating_system = UIDevice.current.systemName + UIDevice.current.systemVersion
    var cpu = SystemInfo.deviceCpuType()
    var network_delay: TimeInterval = 0
    var frame_loss = 0
    var microphone_arbitrary: DeviceStatus = .disable //麦克风
    var headphone_arbitrary: DeviceStatus = .disable //耳机
    var camera_arbitrary: DeviceStatus = .disable //摄像头
    var select_video_device_name = ""
    var select_audio_device_name = ""
    var select_audio_output_device_name = ""
    var client_version = Bundle.main.shortVersion
    var mac = ""
    var camera_status: DeviceCheckStatus = .unnormal
    var microphone_status: DeviceCheckStatus = .unnormal
    var time: Int = 0

    /// 1：设备初始化，2：设备选择，3：自检完成，4：设备插拔，5：设置完成，7：自检中断
    var upload_status: Int = 1

    /// 0：未知；1：Windows，2：Mac，3：iPad，4：Android Pad；5：Android phone，6：iPhone
    var device_type: Int = 3
    var ip = ""
    var network_status: UInt = 0
}
