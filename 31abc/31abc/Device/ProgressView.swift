//
//  ProgressView.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/1.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

private let defalutTintColors = [UIColor("#C2EE83"),
                                 UIColor("#C2EE83"),
                                 UIColor("#C2EE83"),
                                 UIColor("#C2EE83"),
                                 UIColor("#C2EE83"),
                                 UIColor("#C2EE83"),
                                 UIColor("#EED683"),
                                 UIColor("#EECA83"),
                                 UIColor("#EEB683"),
UIColor("#EEA383")]

class ProgressView: UIView {
    var itemSize:CGSize
    var itemBackgroundColor: UIColor
    var itemMargin: CGFloat = 4.0
    var itemRadius: CGFloat = 5.0
    var progress: Double = 0.0 {
        didSet {
            updateProgess()
        }
    }

    /// getter
    var contenSize: CGSize {
        return CGSize(width: CGFloat(items.count) * ( itemMargin + itemSize.width) - itemMargin,
                      height: itemSize.height)
    }

    private var items: [UIView] = []
    private var tintColors: [UIColor] = []
    public init(tintColors: [UIColor] = defalutTintColors,
                itemBackgroundColor: UIColor = UIColor("#E5E5E5"),
                itemSize: CGSize = CGSize(width: 16, height: 26)) {
        self.itemBackgroundColor = itemBackgroundColor
        self.itemSize = itemSize

        super.init(frame: .zero)

        self.tintColors = tintColors
        self.itemBackgroundColor = itemBackgroundColor
        layoutUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func layoutUI() {
        var x: CGFloat = 0
        var subviews: [UIView] = []

        tintColors.enumerated().forEach { (index,_) in
            let item = UIView()
            item.backgroundColor = itemBackgroundColor
            item.layer.cornerRadius = itemRadius
            item.layer.masksToBounds = true
            x = (itemSize.width + itemMargin) * CGFloat(index)
            item.frame = CGRect(x: x, y: 0, width: itemSize.width, height: itemSize.height)
            addSubview(item)
            subviews.append(item)
        }

        items = subviews
        frame = CGRect(x: 0, y: 0, width: x + itemSize.width, height: itemSize.height)
        backgroundColor = .white
    }

    fileprivate func updateProgess() {
        guard progress <= 1.0 else { return }
        let currentIndex = Int(progress * Double(items.count))

        items.enumerated().forEach { (index, item) in
            if index < currentIndex {
                item.backgroundColor = tintColors[index]
            } else {
                item.backgroundColor = itemBackgroundColor
            }
        }
    }
}
