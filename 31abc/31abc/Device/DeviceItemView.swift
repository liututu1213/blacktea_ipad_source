//
//  ABCDeviceItemView.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/28.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class DeviceItemView: UIView {
    var tapClosure: (() -> Void)?
    var isSelected: Bool{
        set {
            _isSelected = newValue
            if newValue == true {
                layer.borderColor = UIColor.clear.cgColor
                backgroundColor = .white
            } else {
                layer.borderColor = UIColor("#ECECEC").cgColor
                backgroundColor = UIColor("#F6F9FE")
            }
        }
        get {
            return _isSelected
        }
    }
    private var _isSelected: Bool = false
    
    func updateUI(image: UIImage?) {
        imageView.image = image
        imageView.isHidden = false
    }
    
    func ishiddenImageView(_ hidden: Bool) {
        imageView.isHidden = hidden
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.borderWidth = 1
        layer.borderColor = UIColor("#ECECEC").cgColor
        
        addSubview(titleLabel)
        addSubview(imageView)
        
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(self).offset(15)
            make.centerY.equalTo(self)
        }
        
        imageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLabel)
            make.left.equalTo(titleLabel.snp.right).offset(2)
            make.width.height.equalTo(12)
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapClick))
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
    @objc func tapClick() {
        tapClosure?()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
     lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = UIColor("#4F4F4F")
        return l
    }()
    
    fileprivate lazy var imageView: UIImageView = {
        let i = UIImageView()
        i.isHidden = true
        return i
    }()
}

class MenuListView: UIView {
    class Item: NSObject {
        var title: String?
        var image: UIImage?
        init(title: String?, image: UIImage? = nil) {
            self.title = title
            self.image = image
        }
    }

    public var selectedClosure: ((Int) -> Void)?
    public var dataSource: [Item] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func selectItem(at index: Int) {
        guard index < dataSource.count else { return }
        tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .none)
    }

    func reloadItem(item: Item, at index: Int) {
        guard index < dataSource.count else { return }
        dataSource[index] = item
        tableView.reloadData()
    }
    
    lazy var tableView: UITableView = {
        let t = UITableView(frame: .zero, style: .plain)
        t.backgroundColor = UIColor("#F6F9FE")
        t.register(MenuListCell.self, forCellReuseIdentifier: NSStringFromClass(MenuListCell.self))
        t.dataSource = self
        t.delegate = self
        t.separatorStyle = .none
        t.isScrollEnabled = false
        return t
    }()
}

extension MenuListView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = dataSource[indexPath.row]
         guard let cell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(MenuListCell.self), for: indexPath) as? MenuListCell else { return UITableViewCell() }
        cell.update(title: item.title, image: item.image)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedClosure?(indexPath.row)
    }
}

class MenuListCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        backgroundColor = UIColor("#F6F9FE")
        selectedBackgroundView = UIView(frame: frame)
        selectedBackgroundView?.backgroundColor = .white

        layoutUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        rightLineView.backgroundColor = selected ? UIColor.clear : UIColor("#ECECEC")
    }

    func layoutUI() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(infoImageView)
        contentView.addSubview(lineView)
        contentView.addSubview(rightLineView)

        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(15)
            make.centerY.equalTo(contentView)
        }

        infoImageView.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLabel)
            make.left.equalTo(titleLabel.snp.right).offset(2)
            make.width.height.equalTo(12)
        }

        lineView.snp.makeConstraints { (make) in
            make.bottom.equalTo(contentView)
            make.height.equalTo(1)
            make.left.right.equalTo(contentView)
        }

        rightLineView.snp.makeConstraints { (make) in
            make.right.equalTo(contentView)
            make.top.bottom.equalTo(contentView)
            make.width.equalTo(1)
        }
    }

    func update(title: String?, image: UIImage?) {
        titleLabel.text = title
        infoImageView.image = image
    }

    fileprivate lazy var titleLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = UIColor("#4F4F4F")
        return l
    }()

    fileprivate lazy var infoImageView: UIImageView = {
        let i = UIImageView()
        return i
    }()

    fileprivate lazy var lineView: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor("#ECECEC")
        return line
    }()

    fileprivate lazy var rightLineView: UIView = {
        let line = UIView()
        line.backgroundColor = UIColor("#ECECEC")
        return line
    }()
}



