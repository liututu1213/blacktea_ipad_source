//
//  VideoContentView.swift
//  31abc
//
//  Created by Cathy on 2019/12/15.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class VideoContentView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(videoView)
        addSubview(videoTipLabel)
        videoView.snp.makeConstraints { (make) in
            make.top.left.equalTo(self).offset(50)
            make.width.equalTo(254)
            make.height.equalTo(190)
        }

        videoTipLabel.snp.makeConstraints { (make) in
            make.left.equalTo(videoView)
            make.top.equalTo(videoView.snp.bottom).offset(20)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    lazy var videoView: UIView = {
        let v = UIView()
        v.layer.cornerRadius = 5
        v.backgroundColor = UIColor("#FCF0C4")
        return v
    }()

    fileprivate lazy var videoTipLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont.systemFont(ofSize: 14)
        l.textColor = UIColor("#4F4F4F")
        l.text = "请对准摄像头，能否看得见自己?"
        return l
    }()
}
