//
//  DeviceDebugManager.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/12.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit

class DeviceDebugManager: NSObject {
    static let shared = DeviceDebugManager()

    var deviceInfo = DeviceModel()
    var txQualityDesc: String {
        switch txQuality {

        case .excellent, .good:
            return "网络质量极好"
        case .poor:
            return "网络连接正常"
        case .bad, .vBad:
            return "网络连接不稳定"
        case .down:
            return "网络连接已断开"
        default:
            return "网络状况未知"
        }
    }

    var txQuality: AgoraNetworkQuality = .unknown {
        didSet {
            if oldValue != txQuality {
                NotificationCenter.default.post(name: .NetworkQualityNotification, object: nil)
            }
        }
    }

    private(set) var enableCheck: Bool = false {
        didSet {
            if oldValue != enableCheck {
                NotificationCenter.default.post(name: .DeviceCheckNotification, object: nil)
            }
        }
    }

    private weak var contentView: UIView?
    override init() {
        super.init()
    }

    func enableVideo(_ contentView: UIView) {
        if enableCheck == true {
            leaveChannel()
        }

        loadAgoraKit(contentView)
    }

    func leaveChannel() {
        let code = rtcEngine?.leaveChannel()
        rtcEngine?.setupLocalVideo(nil)
        UIApplication.shared.isIdleTimerDisabled = false
        removeLocalSession()
        rtcEngine = nil
        if code == 0 {
            enableCheck = false
        }
    }

    var rtcEngine: AgoraRtcEngineKit?
    fileprivate func loadAgoraKit(_ contentView: UIView) {
        self.contentView = contentView

        rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: DefaultKeys.agoraAppId.rawValue, delegate: self)
        rtcEngine?.setChannelProfile(.liveBroadcasting)
        rtcEngine?.enableDualStreamMode(true)
        rtcEngine?.enableVideo()

        let config = AgoraVideoEncoderConfiguration(
            size: AgoraVideoDimension640x480,
            frameRate: .fps24,
            bitrate: AgoraVideoBitrateStandard,
            orientationMode: .adaptative)

        rtcEngine?.setVideoEncoderConfiguration(config)
        rtcEngine?.setClientRole(.broadcaster)
        rtcEngine?.startPreview()
        addLocalSession()

        let uid = AccountManager.shared.student?.user_id ?? 0
        let channelId = "\(uid)\(Int(Date().unixTimestamp))"
        let code = rtcEngine?.joinChannel(
            byToken: nil,
            channelId: channelId,
            info: nil,
            uid: 0,
            joinSuccess: nil)
        if code == 0 {
            setIdleTimerActive(false)
            rtcEngine?.setEnableSpeakerphone(true)
            enableCheck = true
        }
    }

    fileprivate func setIdleTimerActive(_ active: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !active
    }

    fileprivate func addLocalSession() {
        guard let content = contentView else { return }
        let localSession = VideoSession.localSession()
        rtcEngine?.setupLocalVideo(localSession.canvas)

        content.addSubview(localSession.hostingView)
        localSession.hostingView.snp.makeConstraints { (make) in
            make.edges.equalTo(content)
        }
    }

    fileprivate func removeLocalSession() {
        contentView?.subviews.forEach({ (view) in
            view.removeFromSuperview()
        })

    }
}

extension DeviceDebugManager: AgoraRtcEngineDelegate {

    func rtcEngine(_ engine: AgoraRtcEngineKit,
                   networkQuality uid: UInt,
                   txQuality: AgoraNetworkQuality,
                   rxQuality: AgoraNetworkQuality) {
        DeviceDebugManager.shared.txQuality = txQuality

        print("----debug--- \(txQuality.rawValue)")
    }
}

extension Notification.Name {
    static let NetworkQualityNotification = Notification.Name(rawValue: "kNetworkQualityNotification")
    static let DeviceCheckNotification = Notification.Name(rawValue: "kDeviceCheckNotification")
}

extension Notification {
    public struct Key {
        public static let QualityDesc = "kNetworkQualityDescNotification"
    }
}


