//
//  ABCDeviceViewController.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/9/28.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class DeviceViewController: MenuBaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = "设备检测"

        view.backgroundColor = .white
        preferredContentSize = CGSize(width: 608, height: 461)
        navigationController?.navigationBar.isHidden = true

        closeClosure = {[weak self] in
            self?.checkDevice()
            self?.uploadDeviceInfo()
        }

        view.addSubview(listView)
        listView.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom).offset(-1)
            make.left.right.bottom.equalTo(view)
        }

        listView.closeClosure = {[weak self] in
            self?.dismiss(animated: true)
            self?.uploadDeviceInfo()
        }

        view.bringSubviewToFront(headerView)
    }

    func uploadDeviceInfo() {
        let info = DeviceDebugManager.shared.deviceInfo
        info.ip = IP.localIP ?? ""
        info.time = Int(Date().unixTimestamp)
        let dic = info.toJSON()
        NetWorkRequest(.checkDevice(dic ?? [:])) {_ in }
    }

    deinit {
        
    }

    fileprivate func checkDevice() {
        listView.stopCheck()
    }

    fileprivate lazy var listView: DeviceCheckView = {
        let l = DeviceCheckView()
        return l
    }()
}
