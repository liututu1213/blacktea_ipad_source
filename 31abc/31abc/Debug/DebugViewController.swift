//
//  DebugViewController.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/28.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class DebugViewController: MenuBaseViewController, UITableViewDataSource, UITableViewDelegate {
    struct Model {
        var title = ""
        var value = false
    }

    var dataSource: [Model] = [Model(title: "线上环境", value: ABCMoya.environment == .release),
                               Model(title: "Debug", value: UserDefaults.standard[.debug])]

    override func viewDidLoad() {
        super.viewDidLoad()
        headerTitle = "Debug"

        view.backgroundColor = .white
        preferredContentSize = CGSize(width: 608, height: 461)
        navigationController?.navigationBar.isHidden = true

        view.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(view)
            make.top.equalTo(headerView.snp.bottom)
        }
    }

    @objc func valueChanged(_ s: UISwitch) {
        if s.tag >= dataSource.count { return }

        var model = dataSource[s.tag]
        model.value = !model.value
        if s.tag == 0 {
            ABCMoya.environment = model.value ? .release : .test
        } else if s.tag == 1 {
            UserDefaults.standard[.debug] = model.value ? true : false
        }
        dataSource[s.tag] = model
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = dataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
        cell.textLabel?.text = model.title

        let s = UISwitch()
        s.isOn = model.value
        s.tag = indexPath.row
        s.addTarget(self, action: #selector(valueChanged(_:)), for: .valueChanged)

        cell.contentView.addSubview(s)
        s.snp.makeConstraints { (make) in
            make.right.equalTo(cell.contentView).offset(-20)
            make.centerY.equalTo(cell.contentView)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }

    fileprivate lazy var tableView: UITableView = {
        let t = UITableView.init(frame: .zero, style: .plain)
        t.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
        t.dataSource = self
        t.delegate = self
        return t
    }()
}
