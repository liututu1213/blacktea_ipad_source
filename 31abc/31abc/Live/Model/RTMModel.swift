//
//  TeacherBrush.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/3.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import HandyJSON

public enum EventType: String, HandyJSONEnum {
    case none           = "NONE"
    case mouse          = "WB_MOUSE_TRAILING"
    case textCreate     = "WB_TEXT_CREATE"
    case textDelete     = "WB_TEXT_DELETE"
    case textUpdate     = "WB_TEXT_UPDATE"
    case star           = "CR_STAR_ADD"

    case videoInit      = "CW_VIDEO_INIT"
    case videoPlay      = "CW_VIDEO_PLAY"
    case videoPause     = "CW_VIDEO_PAUSE"
    case videoSeek      = "CW_VIDEO_SEEK"
    case videoDestroy   = "CW_VIDEO_DESTROY"

    case audioPlay      = "CW_AUDIO_PLAY"

    case audioOpen      = "CR_STUDENT_AUDIO_OPEN"
    case audioClose     = "CR_STUDENT_AUDIO_CLOSE"

    case helloToTeacher = "CR_HELLO_TEATURE"
    case helloToStudent = "CR_HELLO_STUDENT"
}

public enum BrushLineCap: Int, HandyJSONEnum {
    case butt
    case round
    case square

    var lineCap: CGLineCap {
        switch self {
        case .butt:
            return .butt
        case .round:
            return .round
        case .square:
            return .square
        }
    }
}

class BaseMessage: HandyJSON {
    required init() {}
    var eventType: EventType = .none
    var id = ""
    var time = 0
    var data: [String: Any]?
}

/// WB_MOUSE_TRAILING
class MouseData: HandyJSON {
    class Point: HandyJSON {
        required init() {}
        var createTime = 0
        var dtX: CGFloat = 0.0
        var dtY: CGFloat = 0.0
    }

    required init() {}
    var color = ""
    var dtWidth: CGFloat = 0.0
    var lineCap: BrushLineCap = .round
    var points: [Point] = []
    var lifeTime = 0
}

/// WB_TEXT
class TextData: HandyJSON {
    required init() {}
    var parentWidth: CGFloat = 0.0
    var parentHeight: CGFloat = 0.0
    var rateX: CGFloat = 0.0
    var rateY: CGFloat = 0.0
    var id: Int?
    var content = ""
    var fontSize: CGFloat = 24.0
    var textStrokeWidth: CGFloat = 1.0
    var textStrokeColor = ""
    var paddingHB: CGFloat = 0.0
    var paddingLR: CGFloat = 0.0
    static func == (lhs: TextData, rhs: TextData) -> Bool {
        return lhs.id == rhs.id
    }
}

/// STAR
class StarData: HandyJSON {
    required init() {}
    var star = 0
    var id: UInt = 0
}

/// VIDEO
class VideoData: HandyJSON {
    enum PlayType: String, HandyJSONEnum  {
        case pause
        case play
    }

    required init() {}
    var videoId = ""
    var videoSrc = ""
    var curTime: Double = 0.0
    var toId: String?
    var playType: PlayType = .pause
}


class AudioData: HandyJSON {
    required init() {}
    var audioId = ""
    var audioSrc = ""
}

class HelloTeacher: HandyJSON {
    required init() {}
    var eventType: EventType = .helloToTeacher
    var id = ""
    var time: String = ""
    var data: Data = Data()

    class Data: HandyJSON {
        required init() {}
        var fromId: UInt = 0
    }
}

class HelloStudent: HandyJSON {
    required init() {}
    var toId: UInt = 0
    var texts: [TextData] = []
    var userStars: [StarData] = []
    var videoPageInitInfo: VideoData?
}
