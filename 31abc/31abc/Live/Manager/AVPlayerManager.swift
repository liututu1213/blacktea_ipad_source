//
//  AVPlayerManager.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/27.
//  Copyright © 2019 31abc. All rights reserved.
//

import Foundation
import AVKit

class AVPlayerManager: NSObject {
    static let shared = AVPlayerManager()

    weak var contentView: UIView?

    private var player: AVPlayer?
    private var playerItem: AVPlayerItem?
    private var playerLayer: AVPlayerLayer?
    private var timeObserver: Any?
    private var time: CMTime = .zero
    private var isPlaying = false
    override init() {
        super.init()
        addObserver()
    }

    fileprivate func addObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(routeChangeNotification(_:)),
            name: AVAudioSession.routeChangeNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationBecomeActive),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didEnterBackground),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil)
    }

    @objc func routeChangeNotification(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSession.RouteChangeReason(rawValue: reasonValue),
            isPlaying else { return }

        switch reason {
        case .oldDeviceUnavailable:
            if let previousRoute = userInfo[AVAudioSessionRouteChangePreviousRouteKey] as? AVAudioSessionRouteDescription {
                for output in previousRoute.outputs where output.portType == AVAudioSession.Port.headphones {
                    DispatchQueue.main.async {
                        self.player?.seek(to: self.time)
                        self.player?.play()
                    }
                }
            }
        default:
            ()
        }
    }

    @objc func applicationBecomeActive() {
        if !isPlaying { return }
        try? AVAudioSession.sharedInstance().setActive(false, options: .notifyOthersOnDeactivation)
        try? AVAudioSession.sharedInstance().setCategory(.playback, options: .mixWithOthers)
        self.player?.play()
    }

    @objc func didEnterBackground() {
        if !isPlaying { return }
        self.time = self.player?.currentTime() ?? CMTime.zero
        self.player?.pause()
    }

    @objc func playerDidFinishPlaying(_ notification: Notification) {
        if let item = notification.object as? AVPlayerItem, item == playerItem {
            player?.pause()
            isPlaying = false
        }
    }

    func videoMessage(data: VideoData?, eventType: EventType) {
        if eventType == .videoInit {
            if let audio = data {
                setupPlayer(data: audio)
            }
        } else if eventType == .videoPlay {
            playMedia(data?.curTime ?? 0.0)
        } else if eventType == .videoPause {
            videoPause(data?.curTime ?? 0.0)
        } else if eventType == .videoDestroy {
            videoDestroy()
        }
    }

    fileprivate func setupPlayer(data: VideoData) {
        let localId = AccountManager.shared.student?.user_id ?? 0

        // toId为null 或者 toId是当前学生则都需要初始化视频
        let toId = data.toId ?? "\(localId)"
        if !toId.contains("\(localId)") { return }

        if player != nil { videoDestroy() }
        time = .zero

        guard let url = URL(string: data.videoSrc) else { return }
        playerItem = AVPlayerItem(asset: AVAsset(url: url))

        player = AVPlayer(playerItem: playerItem)
        player?.volume = 0
        player?.seek(to: CMTime(seconds: data.curTime, preferredTimescale: CMTimeScale(1000)), toleranceBefore: .zero, toleranceAfter: .zero)

        if let view = contentView {
            let layer = AVPlayerLayer(player: player)
            layer.frame = view.bounds
            view.layer.addSublayer(layer)
            playerLayer = layer
        }

        /// addObserver
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(playerDidFinishPlaying(_:)),
            name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
            object: playerItem)

        self.timeObserver = player?.addPeriodicTimeObserver(
            forInterval: CMTime(value: CMTimeValue(1.0), timescale: CMTimeScale(1.0)),
            queue: DispatchQueue.main,
            using: {[weak self] (time) in

                self?.time = time
                //let currentTime = CMTimeGetSeconds(time)
                //guard let t = self?.playerItem?.duration else { return }
        })

        if data.playType == .play {
            playMedia(data.curTime)
        }
    }

    fileprivate func playMedia(_ seconds: Double) {
        NotificationCenter.default.post(name: .LivePlayVideoNotification, object: nil, userInfo: [:])
        player?.seek(to: CMTime(seconds: seconds, preferredTimescale: CMTimeScale(1000)), toleranceBefore: .zero, toleranceAfter: .zero)
        player?.play()
        isPlaying = true
    }

    fileprivate func videoPause(_ seconds: Double) {
        player?.seek(to: CMTime(seconds: seconds, preferredTimescale: CMTimeScale(1000)), toleranceBefore: .zero, toleranceAfter: .zero)
        player?.pause()
        isPlaying = false
        NotificationCenter.default.post(name: .LiveStopVideoNotification, object: nil, userInfo: [:])
    }

    fileprivate func videoDestroy() {
        player?.pause()
        isPlaying = false
        NotificationCenter.default.post(name: .LiveStopVideoNotification, object: nil, userInfo: [:])
        if let timerObser = self.timeObserver  {
            player?.removeTimeObserver(timerObser)
        }
        playerLayer?.removeFromSuperlayer()

        player = nil
        playerItem = nil
        playerLayer = nil
    }

    func removePlayer() {
        videoDestroy()
    }
}

extension Notification.Name {
    static let LivePlayVideoNotification = Notification.Name("kLivePlayVideo")
    static let LiveStopVideoNotification = Notification.Name("kLiveStopVideo")
}
