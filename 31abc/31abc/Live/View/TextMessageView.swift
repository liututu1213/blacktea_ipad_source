//
//  TextMessageView.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/19.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

private let kMargin: CGFloat = 10.0

class TextMessageView: UIView {

    private var contentLabels: [Int : UILabel] = [:]
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        if view == self {
            return nil
        }
        return view
    }

    func updateTextMessage(_ data: TextData, eventType: EventType) {
        guard let textId = data.id else {
            /// id 为nil, 则清除所有Text
            contentLabels.forEach { (index, label) in
                label.removeFromSuperview()
            }
            contentLabels.removeAll()
            return
        }
        
        if eventType == .textDelete {
            let label = contentLabels[textId]
            label?.removeFromSuperview()
            contentLabels.removeValue(forKey: textId)
            return
        }

        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5
        let attributedText = NSAttributedString(
            string: data.content,
            attributes: [.font: UIFont.boldSystemFont(ofSize: data.fontSize),
                         .strokeColor: UIColor.white,
                         .foregroundColor: UIColor("#EB5757"),
                         .strokeWidth: -1,
                         .paragraphStyle: style,
                         .kern: 5])

        let maxWidth = kClassViewWidth - data.rateX * kClassViewWidth
        let size = attributedText.boundingRect(
            with: CGSize(width: maxWidth, height: kClassViewHeight),
            options: .usesLineFragmentOrigin, context: .none).size

        let x = data.rateX * kClassViewWidth
        let y = data.rateY * kClassViewHeight

        let messageLabel = updateMessageLabel(forKey: textId)
        messageLabel.attributedText = attributedText
        messageLabel.frame = CGRect(x: x,
                                    y: y,
                                    width: size.width + kMargin * 2,
                                    height: size.height + kMargin * 2)

    }

    fileprivate func updateMessageLabel(forKey key: Int) -> UILabel {
        if let messageLabel = contentLabels[key] {
            return messageLabel
        } else {
            let label = UILabel()
            label.numberOfLines = 0
            addSubview(label)
            contentLabels.updateValue(label, forKey: key)
            return label
        }
    }
}

