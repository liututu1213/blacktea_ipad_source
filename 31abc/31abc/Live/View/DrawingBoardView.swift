//
//  DrawingBoardView.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/2.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class DrawingBoardView: UIView {
    
    private var mouseData = MouseData()
    private var shapeLayers: [CAShapeLayer] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        if view == self {
            return nil
        }
        return view
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func updateMouseMessage(_ mouseData: MouseData, currentTime: Int) {
        self.mouseData  = mouseData
        shapeLayers.forEach { (l) in
            l.removeFromSuperlayer()
        }
        lastSendMessage(mouseData.lifeTime)

        let width: CGFloat = kClassViewWidth
        let height: CGFloat = kClassViewHeight
        let colorString = mouseData.color
        let lineWidth = mouseData.dtWidth * kClassViewWidth
        
        var points = mouseData.points
        points = points.filter{return (currentTime - $0.createTime) < mouseData.lifeTime }

        points.enumerated().forEach { (index, mousePoint) in
            if index < points.count - 1 {
                let startPoint = CGPoint(x: mousePoint.dtX * width, y: mousePoint.dtY * height)
                let endPoint = CGPoint(x: points[index + 1].dtX * width, y: points[index + 1].dtY * height)
                
                let startOpacity = calculatePointOpacity(
                    mousePoint: mousePoint,
                    currentTime: currentTime,
                    lifeTime: mouseData.lifeTime)
                let shap = getShapLayer(index, lineWidth: lineWidth)
                let startColor = UIColor(hexString: colorString, alpha: startOpacity)?.cgColor ?? UIColor("#9B51E0").cgColor
                
                shap.strokeColor = startColor
                drawLine(startPoint: startPoint,
                         endPoint: endPoint,
                         shapLayer: shap,
                         stokeColor: startColor)
            }
        }
    }
    
    private func calculatePointOpacity(
        mousePoint: MouseData.Point,
        currentTime: Int,
        lifeTime: Int) -> CGFloat {

        let opacity = 1.0 - CGFloat((currentTime - Int(mousePoint.createTime))) / CGFloat(lifeTime)
        if opacity >= 1.0 { return 1.0 }
        if opacity <= 0.0 { return 0.0 }
        return opacity
    }
    
    private func getShapLayer(_ index: Int, lineWidth: CGFloat) -> CAShapeLayer {
        if index >= shapeLayers.count {
            let l = CAShapeLayer()
            l.lineWidth = lineWidth
            l.lineCap = .butt
            l.lineJoin = .round
            l.miterLimit = 3
            shapeLayers.append(l)
        }
        return shapeLayers[index]
    }
    
    private func drawLine(startPoint: CGPoint, endPoint: CGPoint, shapLayer: CAShapeLayer, stokeColor: CGColor) {
        let bezierPath = UIBezierPath()
        
        bezierPath.move(to: startPoint)
        bezierPath.addLine(to: endPoint)
        bezierPath.lineWidth = 10
        bezierPath.flatness = 0.1
        bezierPath.lineCapStyle = .round
        bezierPath.lineJoinStyle = .miter
        bezierPath.miterLimit = 3
        
        shapLayer.path = bezierPath.cgPath
        self.layer.addSublayer(shapLayer)
    }

    private func lastSendMessage(_ time: Int) {
        let delayTime = TimeInterval(time / 1000)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        self.perform(#selector(clearMouseMessage), with: self, afterDelay: delayTime)
    }

    @objc private func clearMouseMessage() {
        shapeLayers.forEach { (l) in
            l.removeFromSuperlayer()
        }
    }
}
