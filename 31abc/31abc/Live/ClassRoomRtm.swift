//
//  AgoraRtm.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/13.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

import Foundation
import AgoraRtmKit

enum LoginStatus {
    case online, offline
}

public class ClassRoomRtm: NSObject {
    static let kit = AgoraRtmKit(appId: DefaultKeys.agoraAppId.rawValue, delegate: nil)
    static var current: String?
    static var status: LoginStatus = .offline

    static func updateKit(delegate: AgoraRtmDelegate) {
        guard let kit = kit else { return }
        kit.agoraRtmDelegate = delegate
    }
}
