//
//  LiveViewController.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/1.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit
import AgoraRtmKit
import WebKit
import HandyJSON
import Toast_Swift

let kClassWidthRatio: CGFloat = 0.7
let kClassTeacherWidthRatio: CGFloat = 0.4
let kAspectRatio: CGFloat = 3/4  //  h/w

let kAvailableWidth: CGFloat = kScreenWidth - 2 * 15 - 25
let kClassViewWidth: CGFloat = kAvailableWidth * (5/7)
let kClassViewHeight: CGFloat = kClassViewWidth * kAspectRatio
let kTeacherViewWidth: CGFloat = kAvailableWidth * (2/7)
let kAnimationDuration: TimeInterval = 0.7

class LiveViewController: BaseViewController {

    public var rtcEngine: AgoraRtcEngineKit?
    public var videoProfile: CGSize = AgoraVideoDimension640x480
    public var courseware_url: URL?
    public var starNumber = 0
    public var currentStarNumber: Int = 0
    public var animationViews: [UIView] = []
    public var starTimer: Timer?
    public var rtmChannel: AgoraRtmChannel?
    public var isStopLive: Bool = false
    public var teacherLeave: Bool = true
    public var audioOpen: Bool = false { ///是否开启peer Mode
        didSet {
            guard oldValue != audioOpen else { return }
            muteRemoteStudentAudioStream(audioOpen)
        }
    }

    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.main
        backgroundImageView.image = AccountManager.shared.getBackgroundImage(PageType.room.imageName)

        layoutUI()
        addNotifications()
        AVPlayerManager.shared.contentView = webView
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        //webView
        if let url = courseware_url {
            webView.navigationDelegate = self
            webView.uiDelegate = self
            webView.scrollView.delegate = self
            webView.load(URLRequest(url: url))
        }

        updateClassRoom()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {[weak self] in
            self?.loadSDK()
        }

//        if let lesson = ClassRoomManager.shared.classRoom.lesson_section, let start_time = lesson.start_time {
//            let diff = lesson.time_length - Date().secondsSince(start_time) + 5.0
//            DispatchQueue.main.asyncAfter(deadline: .now() + diff) {
//                self.isStopLive = true
//                self.cheackStopLive()
//            }
//        }
    }

    func cheackStopLive() {
//        if teacherLeave && isStopLive {
//            view.makeToast("教室已关闭", position: ToastPosition.top) { (_) in
//                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
//                    self.back()
//                }
//            }
//        }
    }

    fileprivate func loadSDK() {
        loadAgoraKit()
        loginRTM()
        ClassRoomManager.shared.joinClassRoom(.join)
    }

    deinit {
        AVPlayerManager.shared.removePlayer()
        removeNotifications()
    }

    // MARK: - notifications
    fileprivate func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(playAudio), name: .LivePlayVideoNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(stopAudio), name: .LiveStopVideoNotification, object: nil)
    }

    fileprivate func removeNotifications() {
        NotificationCenter.default.removeObserver(self, name: .LivePlayVideoNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .LiveStopVideoNotification, object: nil)
    }

    // MARK: - layoutUI
    fileprivate func back() {
        navigationController?.popViewController(animated: true)
        leaveChannel()
        logoutRTM()
        ClassRoomManager.shared.joinClassRoom(.leave)
        setIdleTimerActive(true)
    }

    fileprivate func updateClassRoom() {
        if let student = AccountManager.shared.student {
            myselfTitleView.text = student.name
            myselfTitleView.isHidden = false
        }

        if let teacher = ClassRoomManager.shared.classRoom.coach {
            teacherTitleView.text = teacher.name
            teacherTitleView.isHidden = false
        }
        starNumberLabel.text = "\(starNumber)"
    }

    fileprivate func refreshView() {
        webView.reload()
        refreshChannel()
        AVPlayerManager.shared.removePlayer()
    }

    fileprivate func layoutUI() {
        view.addSubview(backButton)
        view.addSubview(refreshButton)
        view.addSubview(classView)
        view.addSubview(teacherView)
        view.addSubview(teacherTitleView)
        view.addSubview(teacherVolume)
        view.addSubview(myselfView)
        view.addSubview(myselfTitleView)
        view.addSubview(myselfVolume)

        backButton.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(20)
            make.top.equalTo(view).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(53)
        }

        refreshButton.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-20)
            make.top.equalTo(view).offset(10)
            make.height.equalTo(36)
            make.width.equalTo(53)
        }

        classView.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(15)
            make.centerY.equalTo(view)
            make.width.equalTo(kClassViewWidth)
            make.height.equalTo(kClassViewHeight)
        }

        teacherView.snp.makeConstraints { (make) in
            make.left.equalTo(classView.snp.right).offset(25)
            make.top.equalTo(classView)
            make.width.equalTo(kTeacherViewWidth)
            make.height.equalTo(kTeacherViewWidth * kAspectRatio)
        }

        teacherTitleView.snp.makeConstraints { (make) in
            make.left.top.equalTo(teacherView).offset(10)
            make.height.equalTo(30)
            make.right.lessThanOrEqualTo(teacherView).offset(-50)
        }

        teacherVolume.snp.makeConstraints { (make) in
            make.top.equalTo(teacherView).offset(10)
            make.width.equalTo(50)
            make.right.equalTo(teacherView.snp.right).offset(-10)
        }

        myselfView.snp.makeConstraints { (make) in
            make.left.equalTo(teacherView)
            make.top.equalTo(teacherView.snp.bottom).offset(25)
            make.width.equalTo(kTeacherViewWidth)
            make.height.equalTo(kTeacherViewWidth * kAspectRatio)
        }

        myselfTitleView.snp.makeConstraints { (make) in
            make.left.top.equalTo(myselfView).offset(10)
            make.height.equalTo(30)
            make.right.lessThanOrEqualTo(myselfView).offset(-50)
        }

        myselfVolume.snp.makeConstraints { (make) in
            make.top.equalTo(myselfView).offset(10)
            make.width.equalTo(50)
            make.right.equalTo(myselfView.snp.right).offset(-10)
        }

        view.addSubview(roomStatusView)
        view.addSubview(starContentView)
        view.addSubview(starImageView)
        view.addSubview(starNumberLabel)

        roomStatusView.snp.makeConstraints { (make) in
            make.left.equalTo(view).offset(10)
            make.bottom.equalTo(view).offset(-10)
            make.width.equalTo(214)
            make.height.equalTo(56)
        }

        starContentView.snp.makeConstraints { (make) in
            make.left.equalTo(myselfView)
            make.top.equalTo(myselfView.snp.bottom).offset(24)
            make.width.equalTo(147)
            make.height.equalTo(56)
        }

        starImageView.snp.makeConstraints { (make) in
            make.left.equalTo(starContentView).offset(10)
            make.centerY.equalTo(starContentView)
        }

        starNumberLabel.snp.makeConstraints { (make) in
            make.left.equalTo(starImageView.snp.right).offset(5)
            make.centerY.equalTo(starContentView)
            make.right.equalTo(starContentView).offset(-15)
        }

        classView.addSubview(webView)
        webView.snp.makeConstraints { (make) in
            make.edges.equalTo(classView)
        }

        classView.addSubview(textMessageView)
        textMessageView.snp.makeConstraints { (make) in
            make.edges.equalTo(classView)
        }

        classView.addSubview(boardView)
        boardView.snp.makeConstraints { (make) in
            make.edges.equalTo(classView)
        }
    }

    // MARK: - SubViews
    fileprivate lazy var backButton: AnimationButton = {
        let b = AnimationButton()
        b.cornerRadius = 5.0
        b.image = UIImage(named: "arrow_left")?.repaintImage(tintColor: UIColor("#745B1A"))
        b.clickClosure = {[weak self] (button) in
            self?.back()
        }
        return b
    }()

    fileprivate lazy var refreshButton: AnimationButton = {
        let b = AnimationButton()
        b.cornerRadius = 5.0
        b.title = "刷新"
        b.setTitleColor(UIColor("#745B1A"), for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        b.clickClosure = {[weak self] (button) in
            self?.refreshView()
        }
        return b
    }()

    lazy var classView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("#FCF0C4")
        v.layer.borderColor = UIColor.white.cgColor
        v.layer.borderWidth = 5
        v.layer.cornerRadius = 10.0
        v.layer.masksToBounds = true

        let backgroundView = UIImageView(image: UIImage(named: "course_placeholder"))
        v.addSubview(backgroundView)
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }

        return v
    }()

    fileprivate var webView: WKWebView = {
        let config = WKWebViewConfiguration()
        config.allowsInlineMediaPlayback = true
        let wk = WKWebView(frame: .zero, configuration: config)
        wk.scrollView.alwaysBounceVertical = false
        wk.scrollView.bouncesZoom = false
        wk.scrollView.isScrollEnabled = false
        return wk
    }()

    lazy var textMessageView: TextMessageView = {
        let v = TextMessageView()
        v.backgroundColor = .clear
        return v
    }()

    lazy var teacherView: DrawingBoardView = {
        let v = DrawingBoardView()
        v.backgroundColor = UIColor.main
        v.layer.borderColor = UIColor("#F2C94C").cgColor
        v.layer.borderWidth = 5
        v.layer.cornerRadius = 10.0
        v.layer.masksToBounds = true

        let backgroundView = UIImageView(image: UIImage(named: "avatar_placeholder_teacher"))
        v.addSubview(backgroundView)
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }
        return v
    }()

    fileprivate lazy var teacherVolume: VolumeView = {
        let v = VolumeView()
        v.isHidden = true
        return v
    }()

    fileprivate lazy var teacherTitleView: ABCLabel = {
        let l = ABCLabel()
        l.backgroundColor = UIColor(white: 0, alpha: 0.4)
        l.layer.cornerRadius = 15.0
        l.layer.masksToBounds = true
        l.isHidden = true
        l.textColor = .white
        l.font = UIFont.systemFont(ofSize: 18)
        l.textAlignment = .center
        l.textInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        return l
    }()

    fileprivate lazy var myselfView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor("#FCF0C4")
        v.layer.borderColor = UIColor.white.cgColor
        v.layer.borderWidth = 5
        v.layer.cornerRadius = 10.0
        v.layer.masksToBounds = true

        let backgroundView = UIImageView(image: UIImage(named: "avatar_placeholder_student"))
        v.addSubview(backgroundView)
        backgroundView.snp.makeConstraints { (make) in
            make.edges.equalTo(v)
        }
        return v
    }()

    fileprivate lazy var myselfVolume: VolumeView = {
        let v = VolumeView()
        return v
    }()

    lazy var boardView: DrawingBoardView = {
        let v = DrawingBoardView()
        v.backgroundColor = .clear
        return v
    }()

    fileprivate lazy var myselfTitleView: ABCLabel = {
        let l = ABCLabel()
        l.backgroundColor = UIColor(white: 0, alpha: 0.4)
        l.layer.cornerRadius = 15.0
        l.layer.masksToBounds = true
        l.isHidden = true
        l.textColor = .white
        l.font = UIFont.systemFont(ofSize: 18)
        l.textAlignment = .center
        l.textInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        return l
    }()

     lazy var roomStatusView: UIButton = {
        let b = UIButton()
        b.backgroundColor = UIColor(white: 0, alpha: 0.4)
        b.layer.cornerRadius = 28.0
        b.layer.masksToBounds = true
        b.setTitle("已离线,正在连接..", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        b.isHidden = true
        return b
    }()

    fileprivate lazy var starContentView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(white: 0, alpha: 0.4)
        v.layer.cornerRadius = 28.0
        v.layer.masksToBounds = true
        return v
    }()

    lazy var starImageView: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "star")
        return i
    }()

    lazy var starNumberLabel: UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.font = UIFont.systemFont(ofSize: 36)
        l.textAlignment = .center
        return l
    }()

    fileprivate var videoSessions = [VideoSession]() {
        didSet {
            layoutVideoView()
        }
    }
}

extension LiveViewController: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if let view = animationViews.first {
            view.removeFromSuperview()
            self.animationViews.removeFirst()
        }
    }
}

//MARK: - AgoraKit
extension LiveViewController {
    fileprivate func refreshChannel() {

        leaveChannel()
        logoutRTM()

        for session in videoSessions {
            session.hostingView.removeFromSuperview()
        }

        videoSessions = []
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {[weak self] in
            self?.loadSDK()
        }
    }

    fileprivate func loadAgoraKit() {
        rtcEngine = AgoraRtcEngineKit.sharedEngine(
        withAppId: DefaultKeys.agoraAppId.rawValue,
        delegate: self)
        setupVideo()
        setupLocalVideo()
        setupRemoteAudio()

        rtcEngine?.setClientRole(.broadcaster)
        rtcEngine?.setChannelProfile(.liveBroadcasting)
        rtcEngine?.enableLastmileTest()

        joinChannel()
    }

    fileprivate func setupVideo() {
        rtcEngine?.enableVideo()
        rtcEngine?.enableAudioVolumeIndication(200, smooth: 3)
        let config = AgoraVideoEncoderConfiguration(
            size: videoProfile,
            frameRate: .fps24,
            bitrate: AgoraVideoBitrateStandard,
            orientationMode: .adaptative)
        rtcEngine?.setVideoEncoderConfiguration(config)
    }

    fileprivate func setupLocalVideo() {
        let localSession = VideoSession.localSession()
        localSession.uid = Int64(AccountManager.shared.student?.user_id ?? 0)
        videoSessions.append(localSession)
        rtcEngine?.setupLocalVideo(localSession.canvas)
    }

    fileprivate func setupRemoteAudio() {
        /// 只接收远端指定uid的音频
        rtcEngine?.setDefaultMuteAllRemoteAudioStreams(true)
        if let teacherId = ClassRoomManager.shared.classRoom.coach?.user_id {
            rtcEngine?.muteRemoteAudioStream(teacherId, mute: false)
        }
    }

    fileprivate func joinChannel() {
        let uid = AccountManager.shared.student?.user_id ?? 0
        let id =  ClassRoomManager.shared.channel.channel_id
        rtcEngine?.setParameters("{\"che.audio.keep.audiosession\": true}")
        let code = rtcEngine?.joinChannel(
            byToken: nil,
            channelId:"\(id)",
            info: nil,
            uid: uid,
            joinSuccess: nil)

        if code == 0 {
            setIdleTimerActive(false)
            view.hideToast()
            view.makeToast("加入班级成功!", position: ToastPosition.top)
        }
    }

    fileprivate func leaveChannel() {
        rtcEngine?.leaveChannel()
        rtcEngine?.setupLocalVideo(nil)

    }

    fileprivate func setIdleTimerActive(_ active: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !active
    }

    fileprivate func layoutVideoView() {
        for session in videoSessions {
            session.hostingView.removeFromSuperview()
        }


        let localId = AccountManager.shared.student?.user_id ?? 0
        let localSessions = videoSessions.filter{$0.uid == localId}
        if let local = localSessions.first {
            myselfView.addSubview(local.hostingView)
            local.hostingView.snp.makeConstraints { (make) in
                make.edges.equalTo(myselfView)
            }
        }

        guard let teacherId = ClassRoomManager.shared.classRoom.coach?.user_id else { return }
        let teacherSessions = videoSessions.filter{$0.uid == teacherId}
        if let teacher = teacherSessions.first {
            teacherVolume.isHidden = false
            teacherView.addSubview(teacher.hostingView)
            teacher.hostingView.snp.makeConstraints { (make) in
                make.edges.equalTo(teacherView)
            }
        } else {
            teacherVolume.isHidden = true
        }
    }
    
    fileprivate func getVideoSession(ofUid uid: Int64) -> VideoSession {
        if let fetchedSession = fetchSession(ofUid: uid) {
            return fetchedSession
        } else {
            let newSession = VideoSession(uid: uid)
            videoSessions.append(newSession)
            return newSession
        }
    }
    
    fileprivate func fetchSession(ofUid uid: Int64) -> VideoSession? {
        for session in videoSessions {
            if session.uid == uid {
                return session
            }
        }
        return nil
    }

    fileprivate func muteRemoteStudentAudioStream(_ open: Bool) {
        guard let students = ClassRoomManager.shared.classRoom.class?.student else { return }
        let localId = AccountManager.shared.student?.user_id
        students.forEach {[weak self] (student) in
            if localId != student.user_id {
                self?.rtcEngine?.muteRemoteAudioStream(student.user_id, mute: !open)
            }
        }
    }

    @objc func playAudio() {
        rtcEngine?.muteAllRemoteAudioStreams(false)
        rtcEngine?.muteAllRemoteAudioStreams(true)
    }

    @objc func stopAudio() {
        rtcEngine?.muteAllRemoteAudioStreams(false)
        ///恢复到播放音频前的peer mode
        muteRemoteStudentAudioStream(audioOpen)
        if let teacherId = ClassRoomManager.shared.classRoom.coach?.user_id {
            rtcEngine?.muteRemoteAudioStream(teacherId, mute: false)
        }
    }
}

extension LiveViewController: AgoraRtcEngineDelegate {
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        log("--didOccurError \(errorCode.rawValue)")

        switch errorCode {
        case .startCall:
            leaveChannel()
        default:
            break
        }
    }

    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        print("---didOccurWarning\(warningCode.rawValue)")
    }

    func rtcEngine(_ engine: AgoraRtcEngineKit,
                   didRejoinChannel channel: String,
                   withUid uid: UInt,
                   elapsed: Int) {
        log("自动重连")
    }

    /// 表示该客户端成功加入了指定的频道
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        log("didJoinedOfUid: uid为\(uid) 加入频道")

        let userSession = getVideoSession(ofUid: Int64(uid))
        rtcEngine?.setupRemoteVideo(userSession.canvas)

        if let teacherId = ClassRoomManager.shared.classRoom.coach?.user_id, teacherId == uid {
            teacherLeave = false
        }
    }

    func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
        if let _ = videoSessions.first {
            layoutVideoView()
        }
    }

    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        log("didOfflineOfUid: uid为\(uid) 离开频道..")

        var indexToDelete: Int?
        for (index, session) in videoSessions.enumerated() {
            if session.uid == Int64(uid) {
                indexToDelete = index
            }
        }

        if let indexToDelete = indexToDelete {
            let deletedSession = videoSessions.remove(at: indexToDelete)
            deletedSession.hostingView.removeFromSuperview()
        }

//        if let teacherId = ClassRoomManager.shared.classRoom.coach?.user_id, teacherId == uid {
//            teacherLeave = true
//            cheackStopLive()
//        }
    }

    func rtcEngine(_ engine: AgoraRtcEngineKit,
                   reportAudioVolumeIndicationOfSpeakers speakers: [AgoraRtcAudioVolumeInfo],
                   totalVolume: Int) {
        speakers.forEach { (info) in
            if info.uid == 0 {
                myselfVolume.progress = Double(totalVolume) / 255.0

            } else if let teacherId = ClassRoomManager.shared.classRoom.coach?.user_id,
                info.uid == teacherId {
                teacherVolume.progress = Double(totalVolume) / 255.0
            }
        }
    }

    func rtcEngine(_ engine: AgoraRtcEngineKit,
                   networkQuality uid: UInt,
                   txQuality: AgoraNetworkQuality,
                   rxQuality: AgoraNetworkQuality) {
        if uid == AccountManager.shared.student?.user_id ||
            uid == ClassRoomManager.shared.classRoom.coach?.user_id ||
            uid == 0 {
            
            switch rxQuality {
            case .down:
                log("网络连接已断开")
                roomStatusView.setTitle("已离线,正在连接..", for: .normal)
                roomStatusView.isHidden = false
            case .bad, .vBad:
                log("网络连接不稳定")
                roomStatusView.setTitle("网络连接不稳定", for: .normal)
                roomStatusView.isHidden = false
            default:
                //log("网络状况很佳")
                roomStatusView.isHidden = true
                break
            }
        }
    }

    func rtcEngineLocalAudioMixingDidFinish(_ engine: AgoraRtcEngineKit) {
    }
}

extension LiveViewController: WKNavigationDelegate, WKUIDelegate, UIScrollViewDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        stopAnimating()
    }
}

//MARK: - Debug
extension UIViewController {
    func log(_ message: String?) {
        if UserDefaults.standard[.debug] {
            view.makeToast(message)
            print(message ?? "")
        }
    }
}
