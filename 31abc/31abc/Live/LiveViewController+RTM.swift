//
//  LiveViewController+RTM.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/19.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import AgoraRtmKit
import Toast_Swift
import AVKit

extension LiveViewController {
    func loginRTM() {
        ClassRoomRtm.updateKit(delegate: self)
        ClassRoomRtm.kit?.login(byToken: nil, user: "\(String(describing: AccountManager.shared.student?.user_id))") {[weak self] (errorCode) in
            guard errorCode == .ok else { return }
            ClassRoomRtm.status = .online

            DispatchQueue.main.async {
                self?.createRTMChannel()
            }
        }
    }

    func createRTMChannel() {
        let id =  ClassRoomManager.shared.channel.channel_id
        guard let rtmChannel = ClassRoomRtm.kit?.createChannel(withId: "\(id)", delegate: self) else {
            log("join rtmChannel fail")
            return
        }

        rtmChannel.join {[weak self] (error) in
            if error != .channelErrorOk {
                self?.log("join channel error: \(error.rawValue)")
            } else {
                self?.sendMesage()
            }
        }
        self.rtmChannel = rtmChannel
    }

    func logoutRTM() {
        guard ClassRoomRtm.status == .online else { return }
        ClassRoomRtm.kit?.logout(completion: { (error) in
            guard error == .ok else { return }
            ClassRoomRtm.status = .offline
        })
    }

    func leaveRTMChannel() {
        rtmChannel?.leave { (error) in
        }
    }

    func sendMesage() {
        guard let id = AccountManager.shared.student?.user_id else { return }
        let hello = HelloTeacher()
        hello.data.fromId = id
        hello.time = defalutDateFormatter.string(from: Date())
        hello.id = "\(hello.time)-\(id)"

        guard let text = hello.toJSONString() else { return }
        let message = AgoraRtmMessage(text: text)
        rtmChannel?.send(message)
    }
}

extension LiveViewController: AgoraRtmDelegate {
    func rtmKit(_ kit: AgoraRtmKit,
                connectionStateChanged state: AgoraRtmConnectionState,
                reason: AgoraRtmConnectionChangeReason) {
        if state == .connected {
            log("RTM connectionStateChanged: connected")
        } else if state == .connecting {
            log("RTM connectionStateChanged: connecting")
        } else if state == .disconnected {
            log("RTM connectionStateChanged: connecting")
        }
    }

    func rtmKit(_ kit: AgoraRtmKit, messageReceived message: AgoraRtmMessage, fromPeer peerId: String) {
    }
}

extension LiveViewController: AgoraRtmChannelDelegate {
    func channel(_ channel: AgoraRtmChannel, memberJoined member: AgoraRtmMember) {
        log("   \(member.userId) join   ")
    }

    func channel(_ channel: AgoraRtmChannel, memberLeft member: AgoraRtmMember) {
        log("   \(member.userId) left   ")
    }

    func channel(_ channel: AgoraRtmChannel,
                 messageReceived message: AgoraRtmMessage,
                 from member: AgoraRtmMember) {
        if let msges = [BaseMessage].deserialize(from: message.text), msges.count > 0 {
            msges.forEach({ (msg) in
                transformMessage(msg)
            })
        } else if let msg = BaseMessage.deserialize(from: message.text) {
            //debug
            if msg.eventType != .mouse {
                log("\(message.text)")
            }
            transformMessage(msg)
        }
    }

    fileprivate func transformMessage(_ message: BaseMessage?) {
        guard let msg = message else { return }

        switch msg.eventType {
        case .mouse:
            if let mouseMessage = MouseData.deserialize(from: msg.data) {
                boardView.updateMouseMessage(mouseMessage, currentTime: msg.time)
            }
        case .star:
            if let starMessage = StarData.deserialize(from: msg.data) {
                updateStartView(starMessage)
            }

        case .textCreate, .textUpdate, .textDelete:
            if let textData = TextData.deserialize(from: msg.data) {
                textMessageView.updateTextMessage(textData, eventType: msg.eventType)
            }

        case .videoInit, .videoPlay, .videoPause, .videoDestroy, .videoSeek:
            let video = VideoData.deserialize(from: msg.data)

            setupVideo(video, eventType: msg.eventType)

        case .audioPlay:
            if let audio = AudioData.deserialize(from: msg.data) {
                rtcEngine?.startAudioMixing(audio.audioSrc, loopback: true, replace: false, cycle: 1)
            }

        case .audioOpen, .audioClose:
            audioOpen = msg.eventType == .audioOpen

        case .helloToStudent:
            if let data = HelloStudent.deserialize(from: msg.data),
                let userId = AccountManager.shared.student?.user_id,
                userId == data.toId {

                data.texts.forEach { (textData) in
                    textMessageView.updateTextMessage(textData, eventType: .textCreate)
                }

                data.userStars.forEach { (starData) in
                    updateStartView(starData)
                }

                if let videoInfo = data.videoPageInitInfo {
                    setupVideo(videoInfo, eventType: .videoInit)
                }
            }
        default:()
        }
    }

    func setupVideo(_ videoData: VideoData?, eventType: EventType) {
        AVPlayerManager.shared.videoMessage(data: videoData, eventType: eventType)

        guard let data = videoData else { return }
        let path = NSHomeDirectory().appending("/Documents/Media/").appending("\(data.videoId).mp4")
        if eventType == .videoInit {
            rtcEngine?.startAudioMixing(path, loopback: true, replace: false, cycle: 1)
            rtcEngine?.setAudioMixingPosition(Int(data.curTime * 1000))

            if data.playType != .play {
                rtcEngine?.pauseAudioMixing()
            }

        } else if eventType == .videoPlay {

            rtcEngine?.startAudioMixing(path, loopback: true, replace: false, cycle: 1)
            rtcEngine?.setAudioMixingPosition(Int(data.curTime * 1000))
            rtcEngine?.resumeAudioMixing()

        } else if eventType == .videoDestroy {
            rtcEngine?.stopAudioMixing()
        } else if eventType == .videoPause {
            rtcEngine?.setAudioMixingPosition(Int(data.curTime * 1000))
            rtcEngine?.pauseAudioMixing()
        }
    }
}




//MARK: - Star
extension LiveViewController {

    fileprivate func updateStartView(_ data: StarData) {
        if data.id == AccountManager.shared.student?.user_id {

            currentStarNumber = data.star
            starNumberLabel.text = "\(data.star)"
            if data.star > 0 {
                startAnimation()
            }
        }
    }

    func startAnimation() {
        AudioPlayer.shared.playAudio(.star)
        let endPoint = view.convert(starImageView.center, to: nil)
        let startPoint = classView.center

        let animationImageView = UIImageView(image: UIImage(named: "star"))
        animationImageView.center = startPoint
        view.addSubview(animationImageView)
        animationViews.append(animationImageView)

        let transform = CGAffineTransform(rotationAngle:  CGFloat(-Double.pi / 2))
        animationImageView.transform = transform
        animationImageView.alpha = 0

        UIView.animate(withDuration: kAnimationDuration,
                       delay: 0,
                       options: [.curveEaseOut],
                       animations: {

                        let transform = CGAffineTransform(rotationAngle: 0)
                        animationImageView.transform = transform.scaledBy(x: 2, y: 2)
                        animationImageView.alpha = 1

        }) { (finished) in
            
            let pathAnimation = CAKeyframeAnimation(keyPath: "position")
            let value1: NSValue = NSValue(cgPoint: animationImageView.center)
            let value2: NSValue = NSValue(cgPoint: endPoint)
            pathAnimation.values = [value1, value2]

            //旋转
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            rotationAnimation.toValue = Double.pi / 3

            // 缩放
            let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.toValue = 0.5

            let group = CAAnimationGroup()
            group.animations = [pathAnimation, scaleAnimation, rotationAnimation]
            group.isRemovedOnCompletion = false
            group.fillMode = .forwards
            group.duration = kAnimationDuration
            group.delegate = self
            group.timingFunction = CAMediaTimingFunction(name: .easeIn)

            animationImageView.layer.add(group, forKey: nil)
        }
    }
}
