//
//  ClassRoomManager.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/11.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import Toast_Swift

class ClassRoomManager: NSObject {
    static let shared = ClassRoomManager()

    var channel = ChannelInfo()
    var classRoom = ClassRoom()
    var servicerTime: Date?
    var diffTime: TimeInterval = 0.0

    func getClassRoomInfo(lesson_section_id: Int, completion: (() -> Void)? = nil) {
        let live = LiveViewController()

        var style = ToastStyle()
        style.backgroundColor = .clear
        style.messageColor = .white
        live.view.backgroundColor = .white
        live.view.makeToast("正在连接...", duration: 10, position: .center, style: style)

        NetWorkRequest(.classRoom(lesson_section_id), { (headers) in

            guard let str = headers?["Date"] as? String else { return }
            let formatter = DateFormatter()
            formatter.dateFormat = "EEE, dd-MMM-yyyy HH:mm:ss zzz"
            self.servicerTime = formatter.date(from: str)

        }) { [weak self] (res) in
            guard let `self` = self else { return }
            switch res {
            case .success(let json):
                let dic = json as? [String :Any]
                guard let classRoom = ClassRoom.deserialize(from: dic), let _ = classRoom.courseware_url else {
                    return
                }

                if let sTime = self.servicerTime {
                    self.diffTime = Date().secondsSince(sTime)
                }

                self.downloadMedia(classRoom.courseware_multi_media)
                self.channel.channel_id = classRoom.channel_id
                self.channel.lesson_section_id = lesson_section_id
                self.classRoom = classRoom
                completion?()

                live.courseware_url = classRoom.courseware_url
                if let nav = NavigationTool.shared.visibleNavigationController {
                    let lives = nav.viewControllers.filter({$0.isKind(of: LiveViewController.self)})
                    if lives.count == 0 {
                        nav.pushViewController(live, animated: false)
                    }
                    
                } else {
                    let a = UIApplication.shared.delegate as! AppDelegate
                    a.switchRooterViewController(live)

                    guard var viewControllers = live.navigationController?.viewControllers else { return }
                    viewControllers.insert(HomeViewController(), at: 0)
                    live.navigationController?.viewControllers = viewControllers
                }
            default:
                completion?()
                break
            }
        }
    }

    func downloadMedia(_ courseware_multi_media: [CoursewareMedia]) {
        let group = DispatchGroup()
        courseware_multi_media.forEach { (media) in
            if media.type == .audio { return }
            guard let url = URL(string: media.url) else { return }
            group.enter()

            NetWorkDownload(.download(url: url, id: media.id)) { _ in
                group.leave()
            }
        }

        group.notify(queue: .main) {
            NotificationCenter.default.post(name: .NetworkDownloadCompletionNotification, object: nil)
        }
    }

    func joinClassRoom(_ type: StatusType) {
        channel.type = type
        channel.time = Int(Date().unixTimestamp)

        NetWorkRequest(.joinRTC(channel.toJSON() ?? [:])) { res in
            switch res {
            case .success(let s):
                print("\(s)")
            case .failure(let err):
                print("\(err)")
            }

        }

        NetWorkRequest(.joinRTM(channel.toJSON() ?? [:])) { _ in
        }

        NetWorkRequest(.startRecording(channel.channel_id)) { _ in
        }

    }
}

extension ClassRoomManager {
    func getRecording_agroa(lesson_section_id: Int, completion: (() -> Void)? = nil) {
        NetWorkRequest(.recording_agroa(lesson_section_id)) { (res) in
            switch res {
            case .success(let json):
                let dic = json as? [String :Any]
                guard let record = RecordModel.deserialize(from: dic) else { return }

                let vc = RecordViewController(record: record)
                NavigationTool.shared.pushViewController(vc)
                print(record)

            case .failure(let error):
                print(error)
            }

            completion?()
        }
    }
}

extension Notification.Name {
    static let NetworkDownloadCompletionNotification = Notification.Name(rawValue: "kNetworkDownloadCompletionNotification")
}
