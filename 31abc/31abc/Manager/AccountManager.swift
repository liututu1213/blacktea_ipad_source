//
//  AccountManager.swift
//  4iPad
//
//  Created by Cathy on 2019/9/25.
//  Copyright © 2019 31abc. All rights reserved..
//

import UIKit
import HandyJSON
import enum Result.Result
import Kingfisher

class AccountManager: NSObject {
    static let shared = AccountManager()
    
    var phoneNumber: String = ""
    var student: Student?
    var now_date: Date = Date()
    var downloadImageCompletion: ((UIImage?, PageType) -> Void)?
    var loginModel: LoginModel? {
        set {
            _loginModel = newValue
            if UserDefaults.standard[.rememberPassword] {
                UserDefaults.standard[.accessToken] = newValue?.access_token
                UserDefaults.standard[.tokenType] = newValue?.token_type
                UserDefaults.standard[.refreshToken] = newValue?.refresh_token
            }
        }
        
        get {
            return _loginModel
        }
    }
    
    private var _loginModel: LoginModel?

    override init() {
        super.init()

        //default
        let defaults = DefaultSettings.defaults.mapKeys{ $0.rawValue }
        UserDefaults.standard.register(defaults: defaults)
        
        /// setup
        let model = LoginModel()
        if UserDefaults.standard[.rememberPassword] {
            model.access_token = UserDefaults.standard[.accessToken]
            model.token_type = UserDefaults.standard[.tokenType]
            model.refresh_token = UserDefaults.standard[.refreshToken]
        }
        _loginModel = model

        phoneNumber = UserDefaults.standard[.phoneNumber] ?? ""
    }

    func savePhoneNumber() {
        UserDefaults.standard[.phoneNumber] = phoneNumber
    }
}

//MARK: - Download Images
extension AccountManager {
    public func getBackgroundImage(_ imageName: String) -> UIImage? {
        let fullPath = NSHomeDirectory().appending("/Documents/").appending(imageName)
        let image = UIImage(contentsOfFile: fullPath)
        return image
    }
    
    public func loadBackgroundImages() {
        NetWorkRequest(.backgroundImage) {[weak self] (res: Result<Any, ABCError>)  in
            switch res {
            case .success(let json):
                let dic = json as? [String :Any]
                if let data = ImageData.deserialize(from: dic), let images = data.background {
                    images.enumerated().forEach { (index, imageModel) in
                        if let urlString = imageModel.url_2x, let url = URL(string: urlString) {
                            self?.downLoadImage(url, type: imageModel.type)
                        }
                    }
                }
                
            default:
                break
            }
        }
    }

    public func loadBackgroundImages(_ images: [BackgroudImage]) {
        images.enumerated().forEach { (index, imageModel) in
            if let urlString = imageModel.url_2x, let url = URL(string: urlString) {
                downLoadImage(url, type: imageModel.type)
            }
        }
    }
    
    fileprivate func downLoadImage(_ url: URL, type: PageType) {
        KingfisherManager.shared.retrieveImage(with: ImageResource(downloadURL: url)) {[weak self] (res) in
            switch res {
            case .success(let r):
                self?.saveImage(r.image, imageName: type.imageName)
                self?.downloadImageCompletion?(r.image, type)

            case .failure:()
            }
        }
    }
    
    fileprivate func saveImage(_ image: UIImage, imageName: String) {
        if let imageData = image.jpegData(compressionQuality: 1.0) as NSData? {
            let fullPath = NSHomeDirectory().appending("/Documents/").appending(imageName)
            imageData.write(toFile: fullPath, atomically: true)
        }
    }
}

extension Notification.Name {
    static let LoginNotification = Notification.Name(rawValue: "LoginNotification")
    static let LogoutNotification = Notification.Name(rawValue: "LogoutNotification")
}
