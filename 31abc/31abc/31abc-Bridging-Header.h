//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#ifndef ABCiPad_Bridging_Header_h
#define ABCPad_Bridging_Header_h

#import <UMCommon/UMCommon.h>

//导入UMCommonLog的OC的头文件
#import <UMCommonLog/UMCommonLogManager.h>

//导入UMAnalytics的OC的头文件
#import <UMAnalytics/MobClick.h>
#import <MJRefresh/MJRefresh.h>

#endif /* UMAnalyticsSwiftDemo_Bridging_Header_h */
