//
//  UpgradeInfo.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/2.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit
import HandyJSON

class UpgradeInfo: HandyJSON {
    required init() {}
    var releaseNoteList: [String] = []
    var url: URL?
    var version: String?
    var releaseDate: Date?
    var isForceUpgrade: Bool = false

    func mapping(mapper: HelpingMapper) {

        mapper <<< releaseDate
            <-- DateFormatterTransform(dateFormatter: defalutDateFormatter)

        mapper <<< url
            <-- URLTransform(shouldEncodeURLString: true)
    }
    
}
