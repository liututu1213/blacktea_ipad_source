//
//  UpgradeViewController.swift
//  ABC4iPad
//
//  Created by Cathy on 2019/10/2.
//  Copyright © 2019 31abc. All rights reserved.
//

import UIKit

class UpgradeViewController: MenuBaseViewController {

    var version = AppSetUp.Version() {
        didSet {
            updateUI()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        preferredContentSize = CGSize(width: 366, height: 337)
        headerTitle = "新版本已经发布"
        closeButton.isHidden = true

        view.addSubview(upgradeTitle)
        view.addSubview(upgradeListLabel)
        view.addSubview(upgradeButton)


        upgradeTitle.snp.makeConstraints { (make) in
            make.top.equalTo(headerView.snp.bottom).offset(15)
            make.left.equalTo(view).offset(15)
        }

        upgradeListLabel.snp.makeConstraints { (make) in
            make.top.equalTo(upgradeTitle.snp.bottom).offset(10)
            make.left.equalTo(view).offset(15)
        }

        upgradeButton.snp.makeConstraints { (make) in
            make.bottom.right.equalTo(view).offset(-15)
            make.width.equalTo(90)
            make.height.equalTo(34)
        }
    }

    fileprivate func updateUI() {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 5.0
        let attributes = [.font: UIFont.systemFont(ofSize: 14),
                          NSAttributedString.Key.paragraphStyle: style]
        upgradeListLabel.attributedText = NSAttributedString(string: version.content, attributes: attributes)
        upgradeTitle.text = version.title
    }

    @objc func upgradeButtonClick() {
        guard let url = URL(string: version.download_url) else { return }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    fileprivate lazy var upgradeButton: UIButton = {
        let b = UIButton()
        b.layer.cornerRadius = 5.0
        b.layer.masksToBounds = true
        b.addTarget(self, action: #selector(upgradeButtonClick), for: .touchUpInside)
        b.setTitle("立即更新", for: .normal)
        b.backgroundColor = UIColor.main
        b.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        b.setTitleColor(.white, for: .normal)
        return b
    }()

    fileprivate lazy var upgradeTitle: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = UIColor("#4F4F4F")
        l.font = UIFont.boldSystemFont(ofSize: 14)
        return l
    }()

    fileprivate lazy var upgradeListLabel: UILabel = {
        let l = UILabel()
        l.numberOfLines = 0
        l.textAlignment = .center
        l.textColor = UIColor("#4F4F4F")
        l.font = UIFont.systemFont(ofSize: 14)
        return l
    }()
}
